<?php include 'header.php' ?>

<?php include 'inside_menu.php' ?>
<!-- This is required to display the menu -->
<section class="hero-area">
</section>

<section class="about-area ptb-90">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="about-us">
<h2>Journals</h2>
<div class="container">
      <div class="row">
        <?php
        $html='';
        foreach($journals as $journal){
            $query_param = str_replace(" ","-",trim($journal['journal_title']));
          // echo $journal['journal_title'];
          $html .= "<div class='col-xs-18 col-sm-6 col-md-3'>
            <div class='thumbnail'>
                <div class='caption'>
                  <h4>".$journal['journal_title']."</h4>
                  <p class='about_journal'>".strip_tags($journal['about_journal'])."</p>
                  <p class='downButton'><a href='".base_url().$query_param."' class='appao-btn appao-btn2'>View More</a><p/>
              </div>
            </div>
          </div>";
            }
            echo $html;
        ?>

        <!-- <div class="col-xs-18 col-sm-6 col-md-3">
          <div class="thumbnail">
            <img src='http://placehold.it/500x250/EEE'>
              <div class="caption">
                <h4>Thumbnail label</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, soluta, eligendi doloribus sunt minus amet sit debitis repellat. Consectetur, culpa itaque odio similique suscipit</p>
                <a href="#" class="btn btn-default btn-xs pull-right" role="button"><i class="glyphicon glyphicon-edit"></i></a> <a href="#" class="btn btn-info btn-xs" role="button">Button</a> <a href="#" class="btn btn-default btn-xs" role="button">Button</a>
            </div>
          </div>
        </div> -->



      </div>
    </div>



</div>
</div>
</div>
<!-- <div class="row">
<div class="col-lg-4">
<div class="single-about-box">
<i class="icofont icofont-ruler-pencil"></i>
<h4>Responsive Design</h4>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text</p>
</div>
</div>
<div class="col-lg-4">
<div class="single-about-box active">
<i class="icofont icofont-computer"></i>
<h4>Fast Performance</h4>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text</p>
</div>
</div>
<div class="col-lg-4">
<div class="single-about-box">
<i class="icofont icofont-headphone-alt"></i>
<h4>Cross Platfrom</h4>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text</p>
</div>
</div>
</div> -->
</div>
</section>

<?php include 'footer.php' ?>
