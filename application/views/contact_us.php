<?php include 'header.php' ?>

<?php include 'inside_menu.php' ?>
<!-- This is required to display the menu -->
<section class="hero-area">
</section>

<section class="about-area ptb-90">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="about-us">
<h2>Contact us<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
  <form id="contact-form" method="POST" class="form-horizontal" action="<?php echo base_url();?>home/submit_query" accept-charset="UTF-8" enctype="multipart/form-data">
  <div class="form-group">
    <label for="email" class="col-sm-3 control-label">Your Name:</label>
    <div class="col-sm-9 ">
    <input type="text" class="form-control " id="name" name="name">
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="col-sm-3 control-label">Your Email:</label>
    <div class="col-sm-9 ">
    <input type="email" class="form-control" id="email" name="email">
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="col-sm-3 control-label">Subject:</label>
    <div class="col-sm-9 ">
    <input type="text" class="form-control" id="subject" name="subject">
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="col-sm-3 control-label">Message:</label>
    <div class="col-sm-9 ">
      <textarea name="message" id="message" rows="5" cols="50"></textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <div class="checkbox">
          <!-- <input type="submit" name="submit" value="Add" class="btn btn-info pull-right" id="submit"> -->
          <button type="submit" class="btn btn-default submit_frontend">Submit</button>
      </div>
    </div>
  </div>
 </form>

</div>
</div>
</div>
<!-- <div class="row">
<div class="col-lg-4">
<div class="single-about-box">
<i class="icofont icofont-ruler-pencil"></i>
<h4>Responsive Design</h4>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text</p>
</div>
</div>
<div class="col-lg-4">
<div class="single-about-box active">
<i class="icofont icofont-computer"></i>
<h4>Fast Performance</h4>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text</p>
</div>
</div>
<div class="col-lg-4">
<div class="single-about-box">
<i class="icofont icofont-headphone-alt"></i>
<h4>Cross Platfrom</h4>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text</p>
</div>
</div>
</div> -->
</div>
</section>

<?php include 'footer.php' ?>
