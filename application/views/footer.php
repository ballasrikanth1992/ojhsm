
<footer class="footer" id="contact">
<div class="container">
<div class="row">
<div class="col-lg-6">
<div class="contact-form">
<h4>Get in Touch</h4>
<p class="form-message"></p>
<form id="contact-form" method="POST" class="form-horizontal" action="<?php echo base_url();?>home/submit_query" accept-charset="UTF-8" enctype="multipart/form-data">
<input type="text" name="name" placeholder="Enter Your Name">
<input type="email" name="email" placeholder="Enter Your Email">
<input type="text" name="subject" placeholder="Your Subject">
<textarea placeholder="Messege" name="message"></textarea>
<button type="submit" name="submit">Send Message</button>
</form>
</div>
</div>
<div class="col-lg-6">
<div class="contact-address">
<h4>Our Address</h4>
<ul><li>
<div class="contact-address-info">
<a href="#">e Science Research</a>
<a href="#">#50, Dash Street,</a>
<a href="#">St. Augustine,</a>
<a href="#">Trinidad & Tobago.</a>
</div>
</ul></li>
<ul>
<li>
<div class="contact-address-icon">
<i class="icofont icofont-headphone-alt"></i>
</div>
<div class="contact-address-info">
<a href="callto:#">+8801712435941</a>

</div>
</li>
<li>
<div class="contact-address-icon">
<i class="icofont icofont-envelope"></i>
</div>
<div class="contact-address-info">
<a href="/cdn-cgi/l/email-protection#3516">contact@escires.com</a>
</div>
</li>
<li>
<div class="contact-address-icon">
<i class="icofont icofont-web"></i>
</div>
<div class="contact-address-info">
<a href="https://www.escires.com/" target="_blank">www.escires.com</a>
</div>
</li>
</ul>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-12">
<div class="subscribe-form">
<form action="#">
<input type="text" placeholder="Your email address here">
<button type="submit">Subscribe</button>
</form>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-12">
<div class="copyright-area">
<ul>
<li><a href="#"><i class="icofont icofont-social-facebook"></i></a></li>
<li><a href="#"><i class="icofont icofont-social-twitter"></i></a></li>
<li><a href="#"><i class="icofont icofont-brand-linkedin"></i></a></li>
<li><a href="#"><i class="icofont icofont-social-pinterest"></i></a></li>
<li><a href="#"><i class="icofont icofont-social-google-plus"></i></a></li>
</ul>
<p>&copy;
Copyright &copy;<script>document.write(new Date().getFullYear());</script> | eSciRes Publications. All Rights Reserved.
</p>
</br>
<p><a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png"></a><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Creative Commons License Open Access </span><br> by <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">e Science Research</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International <br>License</a>. Permissions beyond the scope of this license may be available<br> at <a xmlns:cc="http://creativecommons.org/ns#" href="<?php echo base_url();?>home/terms_and_cond" rel="cc:morePermissions">Terms-of-use</a></p>
</div>
</div>
</div>
</div>
</footer>

<a href="#" class="scrollToTop">
<i class="icofont icofont-arrow-up"></i>
</a>
<div class="switcher-area" id="switch-style">
<div class="display-table">
<div class="display-tablecell">
<a class="switch-button" id="toggle-switcher"><i class="icofont icofont-wheel"></i></a>
<div class="switched-options">
<div class="config-title">Home variation:</div>
<ul>
<li><a href="<?php echo base_url();?>home/join_as_editor">Join as editor</a></li>
<li><a href="<?php echo base_url();?>home/join_as_reviewer">Join as reviewer</a></li>
<li><a href="<?php echo base_url();?>home/submit_manuscript">Submit Manuscript</a></li>
</ul>
<!-- <div class="config-title">Other Pages</div> -->
<!-- <ul>
<li><a href="blog.html">Blog</a></li>
<li><a href="blog-detail.html">Blog Details</a></li>
</ul> -->
</div>
</div>
</div>
</div>

<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>

<!--<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script src="<?php echo base_url();?>assets/js/jquery.slicknav.min.js"></script>

<script src="<?php echo base_url();?>assets/js/slick.min.js"></script>

<script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>

<script src="<?php echo base_url();?>assets/js/jquery.magnific-popup.min.js"></script>

<script src="<?php echo base_url();?>assets/js/jquery.counterup.min.js"></script>

<script src="<?php echo base_url();?>assets/js/waypoints.min.js"></script>

<script src="<?php echo base_url();?>assets/js/jquery.mb.YTPlayer.min.js"></script>

<!-- <script src="<?php echo base_url();?>assets/js/jquery.easing.1.3.js"></script> -->

<!-- <script src="<?php echo base_url();?>assets/js/gmap3.min.js"></script> -->

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBnKyOpsNq-vWYtrwayN3BkF3b4k3O9A_A"></script>

<script src="<?php echo base_url();?>assets/js/custom-map.js"></script>

<script src="<?php echo base_url();?>assets/js/wow-1.3.0.min.js"></script>

<script src="<?php echo base_url();?>assets/js/switcher.js"></script>

<script src="<?php echo base_url();?>assets/js/main.js"></script>

</body>
</html>
