<?php include 'header.php' ?>

<?php include 'inside_menu.php' ?>
<!-- This is required to display the menu -->
<section class="hero-area">
</section>

<section class="about-area ptb-90">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="about-us">
<h2>About eSciRes<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
<p>We are an independent, open-access scientific publishing organization. We are not just a structure but a family of highly skilled, multitalented experienced minds who are dedicated to make a scientific research more proficient and cater to serve the generation with the latest researches worldwide. We run a strict peer – review process that is well structured and flawless. Reputed scientists and professors in our editorial board assist us in each step associated right from the scratch.</p>
</br>
<p>We are glad to invite scholars, researchers to share their hard work with the scientific society through our platform. So, join us to be a part of pure scientific research and establishing a knowledge pool for budding researchers.</p>
</br>
<p>Interested authors, researchers may send their papers/articles to our scientific editors at submissions@escires.com.</p>
</br>
<ul><li>
<p></p>
</li>
<li>
<p> We do not compromise with the quality of the research and so any kind of pirated paper will not be entertained in any case.</p></br>
</li>
<li><p>All our journals can be accessed freely. We are really looking forward to more readers and citations to spread our wings farther in the dissemination of knowledge without any discrimination. For any queries, please contact us at Contact@escires.com</p></li>
</ul>

</div>
</div>
</div>
<!-- <div class="row">
<div class="col-lg-4">
<div class="single-about-box">
<i class="icofont icofont-ruler-pencil"></i>
<h4>Responsive Design</h4>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text</p>
</div>
</div>
<div class="col-lg-4">
<div class="single-about-box active">
<i class="icofont icofont-computer"></i>
<h4>Fast Performance</h4>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text</p>
</div>
</div>
<div class="col-lg-4">
<div class="single-about-box">
<i class="icofont icofont-headphone-alt"></i>
<h4>Cross Platfrom</h4>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text</p>
</div>
</div>
</div> -->
</div>
</section>

<?php include 'footer.php' ?>
