<?php include 'header.php' ?>

<?php include 'inside_menu.php' ?>
<!-- This is required to display the menu -->
<style>
header {
  width: 100%;
  display: inline-block;
  background-color: #404040;
}

.hamburger {
  height: 20px;
  width: 20px;
  padding: 20px;
  float: right;
  cursor: pointer;
}

.hamburger:before {
  content: "";
  display: block;
  background-color: #f3f3f3;
  width: 100%;
  height: 4px;
}

.hamburger:after {
  content: "";
  display: block;
  background-color: #f3f3f3;
  width: 100%;
  height: 4px;
  margin-top: 4px;
  box-shadow: 0px 8px 0 #f3f3f3;
}

nav {
  background-color: #2b2b2b;
  margin: 0;
  max-height: 0;
  overflow: hidden;
  clear:both;
  transition: max-height .3s cubic-bezier(0.63, 0, 0.25, 1);;
}

nav ul {
  margin: 0;
  padding: 0;
  list-style: none;
  display: block;
}

nav li {
  display: block;
  margin: 0;
  text-align: center;
}

nav a {
  color: white;
  display: block;
  padding: .4em;
}

header input[type="checkbox"]:checked ~ nav {
  max-height: 150px;
  border-bottom: #404040 5px solid;
}

header a:hover,
header a:focus,
header label:hover,
header label:focus {
  background-color: #191919;
}

@media (min-width: 700px) {
  .hamburger {
    display: none;
  }
  nav {
    background: transparent;
    float: right;
    border: 0 !important;
    max-height: none;
  }
  nav ul, nav li, nav li a {
    display: inline-block;
  }
  nav a {
    display: inline-block;
    padding: 15px 1em;
  }
}

.hidden {
  position: fixed;
  top: -100%;
  left: -100%;
}

</style>
<section class="hero-area">
</section>

<section class="about-area ptb-90">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="about-us">
  <?php
  if($board_details['status']){ ?>
    <h2><?php echo strtoupper($board_details['name']); ?><span class="sec-title-border"><span></span><span></span><span></span></span></h2>

    <div class='col-lg-12 col-sm-12'>
        <div class='single-team-member'>
        <div class='team-member-im'>
          <img src='<?php echo base_url().'uploads/board/'.$board_details['image'] ?>' alt='team'>
          <div class='team-member-icon'>
          </div>
        </div>
        <div class='team-member-info'>
          <h4><?php echo ($board_details['title'].' '.$board_details['name']); ?></h4>
          <p>University : <?php echo $board_details['university']; ?></p>
          <p>Country : <?php echo $board_details['country']; ?></p>
          <?php if(!empty($board_details['research_intrest'])){ ?>
          <p>Research Interest : <?php echo $board_details['research_intrest'] ?></p>
        <?php
      }if(!empty($board_details['bio'])){ ?>
            <p>Bio : <?php echo $board_details['bio'] ?></p>
          <?php } ?>
        </div>
      </div>
    </div>
  <?php }else{ ?>
    <h2><?php echo "Profile Not Found"; ?><span class="sec-title-border"><span></span><span></span><span></span></span></h2>
  <?php } ?>

<div class="container">
      <div class="row">

        <div class="col-lg-12" id="editorial_member" >
        <div class="journal_details">
          <?php
          /*this is commented as it displays blue color transition*/
          // <div class='team-member-img'>

          /*this is commented as it social networking links*/
          // <div class='display-table'>
          //   <div class='display-tablecell'>
          //     <a href='#'><i class='icofont icofont-social-facebook'></i></a>
          //     <a href='#'><i class='icofont icofont-social-twitter'></i></a>
          //     <a href='#'><i class='icofont icofont-brand-linkedin'></i></a>
          //     <a href='#'><i class='icofont icofont-social-pinterest'></i></a>
          //   </div>
          // </div>
          ?>


        </div>
        </div>

      </div>
    </div>
<?php //var_dump($this->_ci_cached_vars); die();?>


</div>
</div>
</div>
</div>
</section>

<?php include 'footer.php' ?>
