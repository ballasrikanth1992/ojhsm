<?php include 'header.php' ?>

<?php include 'inside_menu.php' ?>

<style>
header {
  width: 100%;
  display: inline-block;
  background-color: #404040;
}

.hamburger {
  height: 20px;
  width: 20px;
  padding: 20px;
  float: right;
  cursor: pointer;
}

.hamburger:before {
  content: "";
  display: block;
  background-color: #f3f3f3;
  width: 100%;
  height: 4px;
}

.hamburger:after {
  content: "";
  display: block;
  background-color: #f3f3f3;
  width: 100%;
  height: 4px;
  margin-top: 4px;
  box-shadow: 0px 8px 0 #f3f3f3;
}

nav {
  background-color: #2b2b2b;
  margin: 0;
  /* max-height: 0; */
  overflow: hidden;
  clear:both;
  transition: max-height .3s cubic-bezier(0.63, 0, 0.25, 1);
  /*float: left;  this is for seven tabs */
  /*width: 100%;  this is for seven tabs */
}

nav ul {
  margin: 0;
  padding: 0;
  list-style: none;
  display: block;
  /*float: left;  this is for seven tabs */
  /*width: 100%;  this is for seven tabs */
}

nav li {
  display: block;
  margin: 0;
  text-align: center;
}

nav a {
  color: white;
  display: block;
  padding: .4em;
}

header input[type="checkbox"]:checked ~ nav {
  max-height: 150px;
  border-bottom: #404040 5px solid;
}

header a:hover,
header a:focus,
header label:hover,
header label:focus {
  background-color: #191919;
}

@media (min-width: 700px) {
  .hamburger {
    display: none;
  }
  nav {
    background: transparent;
    float: right;
    border: 0 !important;
    max-height: none;
  }
  nav ul, nav li, nav li a {
    display: inline-block;
  }
  nav a {
    display: inline-block;
    padding: 15px 1em;
  }
}

.hidden {
  position: fixed;
  top: -100%;
  left: -100%;
}

header a:active{
    background-color: #191919;
}

.heading_active{
  background-color: #191919;
  color: #fff;
}
</style>

<!-- This is required to display the menu -->
<section class="hero-area">
</section>

<section class="about-area ptb-60">
<div class="container">
<div class="row">
<div class="col-lg-12">
<h2 style="margin-bottom: 15px;"><?php echo strtoupper($journal_details['journal_title']); ?><span class="sec-title-border"><span></span><span></span><span></span></span></h2>

<header>

  <nav>
  <ul id="myTab">
    <li class="col-xs-6 nav_mob active" id="about_journal"><a href="#about_journal_body" data-toggle="tab">About Journal</a></li>
    <li class="col-xs-6 nav_mob" id="aim_scope"><a href="#aim_scope_body" data-toggle="tab">Aims & Scope</a></li>
    <li class="col-xs-6 nav_mob" id="editorial_board"><a href="#editorial_board_body" data-toggle="tab">Editorial Board</a></li>
    <li class="col-xs-6 nav_mob" id="articles"><a href="#articles_body" data-toggle="tab">Articles</a></li>
    <li class="col-xs-6 nav_mob" id="articles_in_press"><a href="#articles_in_press_body" data-toggle="tab">Articles in Press</a></li>
    <li class="col-xs-6 nav_mob" id="for_author"><a href="#for_author_body" data-toggle="tab">For Author</a></li>
    <li class="col-xs-6 nav_mob" id="publication_charge"><a href="#publication_charge_body" data-toggle="tab">Publication Charges</a></li>
    <li class="col-xs-6 nav_mob" id="indexing"><a href="#indexing_body" data-toggle="tab">Indexing</a></li>
    <li class="col-xs-6 nav_mob" id="special_issues"><a href="#special_issues_body" data-toggle="tab">Special Issues</a></li>
  </ul>
</nav>
</header>
<div class="about-us">
<div class="container">
      <div class="row tab-content">
        <div class="col-sm-12 col-md-12 col-xs-12 tab-pane active" id="about_journal_body">
          <div class="media">
            <div class="media-left image_padding" >
              <img class="size-medium wp-image-60 alignleft" src="<?php echo base_url();?>uploads/journals/<?php echo $journal_details['journal_image']; ?>" alt="" width="220" height="300">
            </div>
            <div class="media-body journal_details" style="width: 100%;">
              <?php
              echo $journal_details['about_journal'];
              ?>
            </div>
          </div>

        </div>

        <div class="col-lg-12 tab-pane" id="aim_scope_body" style="display: none;">
        <div class="journal_details">
          <?php
           echo $journal_details['aim_and_scope'];
          ?>
        </div>
        </div>

        <div class="col-lg-12 tab-pane" id="editorial_board_body" style="display: none;">
        <div class="journal_details">
          <div class="row">
            <?php

            $board_html = '';
            foreach($board_details as $board_detail){
                $board_name = str_replace(' ', '-', $board_detail['name']);
              $board_html .="<div class='col-lg-4 col-md-6'>
  					    <div class='single-post'>
  							<div class='post-thumbnail' style='text-align: center;'>
  								<img src='".base_url()."uploads/board/".$board_detail['image']."' alt='team' height='150' width='150'>
  							</div>
  							<div class='post-details'>
  								<h4 class='post-title'><a target='_blank' href='".base_url()."home/editorial_board/".$board_name."'><h4>".$board_detail['title'].' '.$board_detail['name']."</h4></a></h4>
                  <p>".$board_detail['university']."</p>
                  <p>".$board_detail['country']."</p>
  							</div>
  						</div>
  					</div>";
            }
            echo $board_html;
            ?>
        </div>
        </div>
      </div>

        <div class="col-lg-12 tab-pane" id="articles_body" style="display: none;">
        <div class="journal_details">
          <?php
          $html = '';
          // echo"<pre>";print_r($article_details);
          $html .="<div class='row'>
          <div class='form-group col-lg-8'>
            <h3 class='gdlr-item-title' id='a_result'>Volume 1 Issue 1</h3>
          </div>
          <div class='form-group col-lg-4'>
          <select onChange='articlesTip(this.value)' class='form-control'>";
          if(!empty($category1_article_details)){
            foreach($category1_article_details as $category1_article_detail){
              $html .="<option value='".$category1_article_detail['volume'].",".$category1_article_detail['issue'].",".$category1_article_detail['journal_id']."'>Volume ".$category1_article_detail['volume']." Issue ".$category1_article_detail['issue']."</option>";
            }
          }
            $html .="</select>
          </div>
          </div>"; ?>

          <?php $html .="<div id='a_loop'>";
          if(!empty($initial_article_1)){
            // $html .="<h3 class='gdlr-item-title'>Volume 1 Issue 1</h3>";
            foreach($initial_article_1 as $initial_article_1_data){
              if($initial_article_1_data['category'] == 1){
                $html .="<div class='gdlr-divider'></div>
                <div class='single-showcase-box'>
                <div class='row'>
                <div class='col-md-8' style='padding-top: 10px;'>
                <h4>" .$initial_article_1_data['article_title']."</h4>
                <p>" .$initial_article_1_data['author']. "</p>
                <p>" .$initial_article_1_data['unique_article_id']. "</p>
                </div>
                <div class='col-md-4 align_button'>
                <div class='pull-right'>
                <a href='".base_url()."home/full_text/".$initial_article_1_data['journal_id']."' target='_blank' class='appao-btn appao-btn2'>View</a>
                <a href='".base_url()."articles/".$initial_article_1_data['pdf']."' target='_blank' class='appao-btn appao-btn2'>Download</a>
                </div>
                </div>
                </div>
                </div>";
              }else{
                $html ="<h3 class='gdlr-item-title'>No Articles</h3>";
              }
            }
          }

          echo $html;
          ?>
        </div>
        </div>
        </div>

        <div class="col-lg-12 tab-pane" id="articles_in_press_body" style="display: none;">
        <div class="journal_details">
          <?php
          $html = '';
          $html .="<div class='row'>
          <div class='form-group col-lg-8'>
            <h3 class='gdlr-item-title' id='aip_result'>Volume 1 Issue 1</h3>
          </div>
          <div class='form-group col-lg-4'>
            <select onChange='dropdownTip(this.value)' class='form-control'>";

            if(!empty($category2_article_details)){
              foreach($category2_article_details as $category2_article_detail){
                $html .="<option value='".$category2_article_detail['volume'].",".$category2_article_detail['issue'].",".$category2_article_detail['journal_id']."'>Volume ".$category2_article_detail['volume']." Issue ".$category2_article_detail['issue']."</option>";
              }
            }
              $html .="</select>
          </div>
          </div>"; ?>

          <?php $html .="<div id='aip_loop'>";
          if(!empty($initial_article_2)){
            foreach($initial_article_2 as $initial_article_2_data){
              if($initial_article_2_data['category'] == 2){
                $html .="<div class='gdlr-divider'></div>
                <div class='single-showcase-box'>

                <div class='row'>
                <div class='col-md-8' style='padding-top: 10px;'>
                <h4>" .$initial_article_2_data['article_title']."</h4>
                <p>" .$initial_article_2_data['author']. "</p>
                <p>" .$initial_article_2_data['unique_article_id']. "</p>
                </div>
                <div class='col-md-4 align_button'>
                <div class='pull-right'>
                <a href='".base_url()."home/full_text/".$initial_article_2_data['journal_id']."' target='_blank' class='appao-btn appao-btn2'>View</a>
                <a href='".base_url()."articles/".$initial_article_2_data['pdf']."' target='_blank' class='appao-btn appao-btn2'>Download</a>
                </div>
                </div>
                </div>
                </div>";
              }else{
                $html ="<h3 class='gdlr-item-title'>No Articles</h3>";
              }
            }
          }

          echo $html;
          ?>
        </div>
        </div>
        </div>

        <div class="col-lg-12 tab-pane" id="for_author_body" style="display: none;">
        <div class="journal_details">
          <?php
           echo $journal_details['for_author'];
          ?>
        </div>
        </div>

        <div class="col-lg-12 tab-pane" id="publication_charge_body" style="display: none;">
        <div class="journal_details">
          <?php
           echo $journal_details['publication_charges'];
          ?>
        </div>
        </div>

        <div class="col-lg-12 tab-pane" id="indexing_body" style="display: none;">
        <div class="journal_details">
          <div class="container">
          <div class="row">
            <?php
            $index_html = '';
            foreach($indexing_details as $indexing_detail){
              $index_html .= "<div class='col-xs-18 col-sm-6 col-md-3'>
                <div class='thumbnail'>
                  <img src='".base_url()."uploads/indexes/". $indexing_detail['image']."' >
                    <div class='caption'>
                      <h4>".$indexing_detail['title']."</h4>
                  </div>
                </div>
              </div>";
            }
            echo $index_html;
             ?>
             <!-- <div class="col-xs-18 col-sm-6 col-md-3">
               <div class="thumbnail">
                 <img src='http://placehold.it/500x250/EEE'>
                   <div class="caption">
                     <h4>Thumbnail label</h4>
                 </div>
               </div>
             </div> -->
        </div>
      </div>
    </div>
  </div>

        <div class="col-lg-12 tab-pane" id="special_issues_body" style="display: none;">
        <div class="journal_details">
          <?php
          $html = '';
          if(!empty($article_details)){
            $html .="<h3 class='gdlr-item-title'>Volume 1 Issue 1</h3>";
            foreach($article_details as $article_detail){
              if($article_detail['category'] == 3){
                $html .="<div class='gdlr-divider'></div>
                <div class='single-showcase-box'>
                <div class='row'>
                <div class='col-md-8' style='padding-top: 10px;'>
                <h4>" .$article_detail['article_title']."</h4>
                <p>" .$article_detail['author']. "</p>
                <p>" .$article_detail['unique_article_id']. "</p>
                </div>
                <div class='col-md-4 align_button'>
                <div class='pull-right'>
                <a href='".base_url()."home/full_text/".$article_detail['journal_id']."' target='_blank' class='appao-btn appao-btn2'>View</a>
                <a href='".base_url()."articles/".$article_detail['pdf']."' target='_blank' class='appao-btn appao-btn2'>Download</a>
                </div>
                </div>
                </div>
                </div>";
              }else{
                $html ="<h3 class='gdlr-item-title'>No Articles</h3>";
              }
            }
          }
          echo $html;
          ?>
        </div>
        </div>

      </div>
    </div>



</div>
<!-- <div class="clearfix"></div> -->
</div>
</div>
</div>
</section>
<?php include 'footer.php' ?>
<script>
function articlesTip(data){
  volume = data.split(',')[0];
  issue = data.split(',')[1];
  jid = data.split(',')[2];
  document.getElementById("a_result").innerHTML = 'Volume '+volume+ ' Issue '+issue;
  $.ajax({
                 url:'<?php echo base_url(); ?>home/get_journals_by_order_1',
                 data:{vol : volume,iss: issue,jid: jid},
                 type: 'post',
                 success:function(res){
                   var output = JSON.parse(res);
                   var html = '';
                   $('#a_loop').empty();
                   $.each(output, function(index, value) {
                     var base_url = '<?php echo base_url() ;?>';
                       var html = "<div class='gdlr-divider'></div><div class='single-showcase-box'><div class='row'><div class='col-md-8' style='padding-top: 10px;'><h4>"+value.article_title+"</h4><p>" +value.author+ "</p><p>" +value.unique_article_id+ "</p></div><div class='col-md-4 align_button'><div class='pull-right'><a href='"+base_url+"home/full_text/"+value.journal_id+"' target='_blank' class='appao-btn appao-btn2'>View</a><a href='"+base_url+"articles/"+value.pdf+"' target='_blank' class='appao-btn appao-btn2'>Download</a></div></div></div></div>";
                     $('#a_loop').append(html);
                  });
                 }
             });
}

function dropdownTip(data){
  volume = data.split(',')[0];
  issue = data.split(',')[1];
  jid = data.split(',')[2];
  document.getElementById("aip_result").innerHTML = 'Volume '+volume+ ' Issue '+issue;
  $.ajax({
                 url:'<?php echo base_url(); ?>home/get_journals_by_order_2',
                 data:{vol : volume,iss: issue,jid: jid},
                 type: 'post',
                 success:function(res){
                   var output = JSON.parse(res);
                   var html = '';
                   $('#aip_loop').empty();
                   $.each(output, function(index, value) {
                     var base_url = '<?php echo base_url() ;?>';
                       var html = "<div class='gdlr-divider'></div><div class='single-showcase-box'><div class='row'><div class='col-md-8' style='padding-top: 10px;'><h4>"+value.article_title+"</h4><p>" +value.author+ "</p><p>" +value.unique_article_id+ "</p></div><div class='col-md-4 align_button'><div class='pull-right'><a href='"+base_url+"home/full_text/"+value.journal_id+"' target='_blank' class='appao-btn appao-btn2'>View</a><a href='"+base_url+"articles/"+value.pdf+"' target='_blank' class='appao-btn appao-btn2'>Download</a></div></div></div></div>";
                     $('#aip_loop').append(html);
                  });
                 }
             });
}

$('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
    localStorage.setItem('activeTab', $(e.target).attr('href'));
});
var activeTab = localStorage.getItem('activeTab');
var newStr = activeTab.replace('_body', '');
if (activeTab) {
   $('a[href="' + activeTab + '"]').tab('show');
   $(activeTab).show();
   $(newStr).addClass('heading_active');
}

$(document).ready(function(){
    $('#about_journal,#about_journal,#aim_scope,#editorial_board,#articles,#articles_in_press,#for_author,#publication_charge,#indexing,#special_issues').click(function(){
  $('.nav_mob').removeClass('heading_active');
});

  $('#about_journal').click(function(){
    $('#about_journal_body').show();
    $('#aim_scope_body').hide();
    $('#editorial_board_body').hide();
    $('#articles_body').hide();
    $('#articles_in_press_body').hide();
    $('#for_author_body').hide();
    $('#publication_charge_body').hide();
    $('#indexing_body').hide();
    $('#special_issues_body').hide();
  });

  $('#aim_scope').click(function(){
    $('#about_journal_body').hide();
    $('#aim_scope_body').show();
    $('#editorial_board_body').hide();
    $('#articles_body').hide();
    $('#articles_in_press_body').hide();
    $('#for_author_body').hide();
    $('#publication_charge_body').hide();
    $('#indexing_body').hide();
    $('#special_issues_body').hide();
  });

  $('#editorial_board').click(function(){
    $('#about_journal_body').hide();
    $('#aim_scope_body').hide();
    $('#editorial_board_body').show();
    $('#articles_body').hide();
    $('#articles_in_press_body').hide();
    $('#for_author_body').hide();
    $('#publication_charge_body').hide();
    $('#indexing_body').hide();
    $('#special_issues_body').hide();
  });

  $('#articles').click(function(){
    $('#about_journal_body').hide();
    $('#aim_scope_body').hide();
    $('#editorial_board_body').hide();
    $('#articles_body').show();
    $('#articles_in_press_body').hide();
    $('#for_author_body').hide();
    $('#publication_charge_body').hide();
    $('#indexing_body').hide();
    $('#special_issues_body').hide();
  });
  $('#articles_in_press').click(function(){
    $('#about_journal_body').hide();
    $('#aim_scope_body').hide();
    $('#editorial_board_body').hide();
    $('#articles_body').hide();
    $('#articles_in_press_body').show();
    $('#for_author_body').hide();
    $('#publication_charge_body').hide();
    $('#indexing_body').hide();
    $('#special_issues_body').hide();
  });
  $('#for_author').click(function(){
    $('#about_journal_body').hide();
    $('#aim_scope_body').hide();
    $('#editorial_board_body').hide();
    $('#articles_body').hide();
    $('#articles_in_press_body').hide();
    $('#for_author_body').show();
    $('#publication_charge_body').hide();
    $('#indexing_body').hide();
    $('#special_issues_body').hide();
  });
  $('#publication_charge').click(function(){
    $('#about_journal_body').hide();
    $('#aim_scope_body').hide();
    $('#editorial_board_body').hide();
    $('#articles_body').hide();
    $('#articles_in_press_body').hide();
    $('#for_author_body').hide();
    $('#publication_charge_body').show();
    $('#indexing_body').hide();
    $('#special_issues_body').hide();
  });
  $('#indexing').click(function(){
    $('#about_journal_body').hide();
    $('#aim_scope_body').hide();
    $('#editorial_board_body').hide();
    $('#articles_body').hide();
    $('#articles_in_press_body').hide();
    $('#for_author_body').hide();
    $('#publication_charge_body').hide();
    $('#indexing_body').show();
    $('#special_issues_body').hide();
  });
  $('#special_issues').click(function(){
    $('#about_journal_body').hide();
    $('#aim_scope_body').hide();
    $('#editorial_board_body').hide();
    $('#articles_body').hide();
    $('#articles_in_press_body').hide();
    $('#for_author_body').hide();
    $('#publication_charge_body').hide();
    $('#indexing_body').hide();
    $('#special_issues_body').show();
  });

});
</script>
