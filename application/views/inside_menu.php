

<header class="header">
<div class="container-fluid">
<div class="row flexbox-center">
<div class="col-lg-1 col-md-3 col-6">
<div class="logo">
<h1><a href="<?php echo base_url();?>home/index" style="color: #fff;">eSciRes</a></h1>
</div>
</div>
<div class="col-lg-9 col-md-9 col-6">
<div class="responsive-menu"></div>
<div class="mainmenu">
<ul id="primary-menu">
<li><a class="nav-link active" href="<?php echo base_url();?>home/index">Home</a></li>
<li><a class="nav-link" href="<?php echo base_url();?>home/about_us">About us</a></li>
<li><a class="nav-link" href="<?php echo base_url();?>home/advisory_board">Advisory Board</a></li>
<li><a class="nav-link" href="<?php echo base_url();?>journals">Journals</a></li>
<li><a class="nav-link" href="<?php echo base_url();?>home/guidelines">Guidelines</a></li> 
<li><a class="nav-link" href="<?php echo base_url();?>home/submit_manuscript">Submit Manuscript</a></li>
<li><a class="nav-link" href="<?php echo base_url();?>home/contact_us">Contact Us</a></li>
<!-- <li></li> -->
</ul>
</div>
</div>
<div class="col-lg-2 col-md-2 col-6">
  <form class="navbar-form" role="search">
          <div class="input-group" style="border-radius: 0px; padding-top: -3px; top: 8px; ">
              <input type="text" class="form-control" placeholder="Search" name="q" style="height: 34px; border-right: none; border-radius: 0; background: #fff; color: #fff;">
              <div class="input-group-btn" style="border-radius: 0; ">
                  <button class="btn btn-default" type="submit" style=" border-radius: 0; color: #000; background: #fff; ">
                    <i class="icofont icofont-search"></i>
                  </button>
              </div>
          </div>
          </form>
</div>
</div>
</div>
</header>
