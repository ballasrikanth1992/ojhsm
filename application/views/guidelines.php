<?php include 'header.php' ?>

<?php include 'inside_menu.php' ?>
<!-- This is required to display the menu -->
<section class="hero-area">
</section>
<style>
.Guidelines p {
    text-align : left;
}
</style>
<section class="about-area ptb-90">
<div class="container">
  <div class="Guidelines">
    <h1 style="color: #333399;">Submission Guidelines for Authors<span class="sec-title-border"><span></span><span></span><span></span></span></h1>
    <p><span style="color: #000000;">eSciRes publishers welcomes all the cutting-edge latest and outstanding scientific contributions in all fields of science, medicine and technology.</span></p> <br />
    <p><span style="color: #000000;">Submissions can be in the form of</span></p> <br />
    <p><span style="color: #000000;">Original research article, review, short communication, commentary, clinical perspectives, clinical procedures, thesis, opinion article, editorial, letter to editor, technical briefs, scientific reports, meeting summary, case reports, news, conference proceedings, clinical/medical images.</span></p> <br />
    <p><span style="color: #000000;">Manuscript submission should include a cover letter along with the manuscript.</span></p> <br />
    <p><span style="color: #000000;"><b>Cover Letter :</b></span></p> <br />
    <p><span style="color: #000000;">Outlining the novelty of the paper and a statement explaining the paper has been submitted to the corresponding journal. Authors shall be required to ensure that their papers submitted for publication are neither submitted elsewhere nor under consideration.</span></p> <br />
    <p><span style="color: #000000;"><b>Manuscript Guidelines :</b></span></p> <br />
    <p><span style="color: #000000;">For Research/Review Article :</span></p> <br />
    <p><span style="color: #000000;">Title, Name of Authors with Complete affiliations (Degrees, Department, School, Institute, University, City, Country) and the corresponding authors’ current address including Telephone number and E-mail ID. Mostly it should be having the order of the fields like Abstract (Max of 250 words), keywords, Abbreviations (if any), Introduction, Background, Materials and Methods, Results and Discussion, Conclusion.</span></p> <br />
    <p><span style="color: #000000;"><b>Acknowledgement :</b>&nbsp;It should include acknowledgements to the assisting people including the funding information (if any).</span></p> <br />
    <p><span style="color: #000000;"><b>References:</b>&nbsp;References must include all the issued manuscripts with their online links. In case of additional authors more than three in number, do not write all the names, just the first three authors and et al. for others.</span></p> <br />
    <p><span style="color: #000000;">References format</span></p> <br />
    <p><span style="color: #000000;"><b>For published journal papers :</b></span></p> <br />
    <p><span style="color: #000000;">Author1, Author2, Author3, et al. (year) Title of the article in sentence case. Journal name volume number(issue number): page numbers.</span></p> <br />
    <p><span style="color: #000000;"><b>For books :</b></span></p> <br />
    <p><span style="color: #000000;">Authors (year) Title of the chapter. Editors, Title of the book, Publisher name, Country, page numbers(if any).</span></p> <br />
    <p><span style="color: #000000;">For web URL’s Please provide the URL.</span></p> <br />
    <p><span style="color: #000000;">Illustrations and figures (if any), Tables and captions (if any), Supplementary files (if any).</span></p> <br />
    <p><span style="color: #000000;"><b>Tables :</b>&nbsp;Tables should be simple, sequential and clearly understandable. Please make sure that they are in .doc format.</span></p> <br />
    <p><span style="color: #000000;"><b>Figures:</b>&nbsp;Figures should be in a sequence and must be written as Figure 1, Figure 2 in case of parts: Figure 1(a), Figure 1(b).</span></p> <br />
    <p><span style="color: #000000;"><b>Letter to the Editor :</b>&nbsp;There is no particular standard format for letter to editor. Please make sure that it is short and crisp.</span></p> <br />
    <p><span style="color: #000000;"><b>For Other Types of Articles :</b></span></p> <br />
    <p><span style="color: #000000;">Title, Author’s Name and Affiliation, Co-authors and Corresponding author details. Abstract (If applicable) , Content headings (Introduction, result, conclusion, References, etc, what ever applicable Figures and tables (if any: Figure must be in image format, table must be in table form).</span></p> <br />
    <p><span style="color: #000000;"><b>Submission Procedure :</b>&nbsp;Author submissions can be directed as an e-mail attachment to the editorial office of the concerned journal. Please visit the respective journal homepage for further details.</span></p> <br />
    <p><span style="color: #000000;"><b>Copyright :</b>&nbsp;All works published by eSciRes are under the terms of the Creative Commons Attribution License. This permits anyone to copy, distribute, transmit and adapt the work provided the original work and source is appropriately cited.</span></p> <br />
    <p><span style="color: #000000;"><b>Why to publish with us :</b></span></p> <br />
    <ul>
    <li><span style="color: #000000;">Rapid and stringent peer review process</span></li> <br />
    <li><span style="color: #000000;">Online-first policy</span></li> <br />
    <li><span style="color: #000000;">Eminent and active editorial board</span></li> <br />
    <li><span style="color: #000000;">Strong reviewer assistance</span></li> <br />
    <li><span style="color: #000000;">Prompt editorial responses</span></li> <br />
    </ul>
  </div>
</div>
</section>

<?php include 'footer.php' ?>
