<?php include 'header.php' ?>

<?php include 'inside_menu.php' ?>
<!-- This is required to display the menu -->
<section class="hero-area">
</section>

<section class="about-area ptb-90">
<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="about-us">
<h2>JOIN AS EDITOR<span class="sec-title-border"><span></span><span></span><span></span></span></h2>
<div class = "alert alert-dismissable" id="form_status">
<!-- <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
  &times;
</button> -->
<p id="msg"></p></div>

<?php
if(isset($_GET['is_success'])){
  if($_GET['is_success'] == 1){
    echo '<div class = "alert alert-success alert-dismissable" id="status">
    <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
    &times;
    </button>
    Email Sent Successfully.
    </div>';
  }elseif($_GET['is_success'] == 0){
    echo '<div class = "alert alert-danger alert-dismissable" id="status">
    <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
    &times;
    </button>
    Email Sending Failed.
    </div>';
  }
}
?>
<!-- <form method="POST" action="<?php echo base_url(); ?>home/submit_manuscript_email" accept-charset="UTF-8" enctype="multipart/form-data"> -->
  <form enctype="multipart/form-data" id="fupForm" method="POST" >
  <div class="form-group">
    <label for="email" class="col-sm-3 control-label">Your Name:</label>
    <div class="col-sm-9 ">
    <input type="text" class="form-control " id="name" name="name">
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="col-sm-3 control-label">Your Email:</label>
    <div class="col-sm-9 ">
    <input type="email" class="form-control" id="email" name="email">
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="col-sm-3 control-label">Affiliation:</label>
    <div class="col-sm-9 ">
    <input type="email" class="form-control" id="affiliation" name="affiliation">
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="col-sm-3 control-label">Your Country:</label>
    <div class="col-sm-9 ">
    <input type="text" class="form-control" id="country" name="country">
    </div>
  </div>
   <!-- <div class="form-group">
    <label for="sel1" class="col-sm-3 control-label">Journal:</label>
    <div class="col-sm-9 ">
      <select name="select_journal" class="form-control selectpicker" id="select_journal" >
        <option>select</option>
        <?php foreach($journals as $journal){
        echo '<option value="'.$journal['journal_title'].'">'.$journal['journal_title'].'</option>';
    }?>
      </select>
    </div>
  </div> -->
  <div class="form-group">
    <label for="email" class="col-sm-3 control-label">Short Biography:</label>
    <div class="col-sm-9 ">
    <input type="text" class="form-control" id="title" name="title">
    </div>
  </div>
  <!-- <div class="form-group">
    <label for="email" class="col-sm-3 control-label">Description</label>
    <div class="col-sm-9 ">
    <textarea class="form-control" id="message" name="message"></textarea>
    </div>
  </div> -->
  <div class="form-group">
    <label for="email" class="col-sm-3 control-label">Research Intrest:</label>
    <div class="col-sm-9 ">
    <input type="text" class="form-control" id="manu_type" name="manu_type">
    </div>
  </div>
  <div class="form-group">
     <label for="inputEmail3" class="col-sm-3 control-label">Upload CV</label>
     <div class="col-sm-9 ">
     <input name="document"  class="form-control required" type="file" id="document" >
     </div>
 </div>
 <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Upload Photograph</label>
    <div class="col-sm-9 ">
    <input name="document"  class="form-control required" type="file" id="document" >
    </div>
</div>
 <div class="row" style="padding: 10px 30px;">
 <div class=" vacation-capatcha-main-top ">              <div class="vacation-capatcha-main" style="position: relative;
    text-align: center;">              <img src="https://www.travelur.com/extras/assets/images/home/87735716.jpg" alt="Norway" style="">
  <div id="captcha" class="vacation-capatcha-main-content" style="font-size: 30px;
font-weight: 700;
font-family: unset;
color: #fff;
font-style: italic;
position: absolute;
top: 30%;
left: 6%;" oncopy="">
              <?php
$seed = str_split('abcdefghijklmnopqrstuvwxyz'
.'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
.'0123456789!@#$%^&*()'); // and any other characters
shuffle($seed); // probably optional since array_is randomized; this may be redundant
$rand = '';
foreach (array_rand($seed, 6) as $k) $rand .= $seed[$k];
echo $captcha=$rand;
?>
      </div>
     <input type="hidden" id="captcha_data" value="<?php echo $captcha;?>">      <span onclick="captcha_refresh()">            <i class="icofont icofont-refresh tarvel-captcha-image-refresh-icon" aria-hidden="true" style="">    </i>        </span>        </div>          </div>
     <div class=" vacation-capatcha-main-top">          <div class="form-group col-md-12">
     <input type="text" class="form-control" id="captcha_value" placeholder="Enter Captcha Here ">    </div>          </div>
</div>

 <div class="form-group">
   <div class="col-sm-offset-2 col-sm-10">
     <div class="checkbox">
         <!-- <input type="submit" name="submit" value="Add" class="btn btn-info pull-right" id="submit"> -->
         <!-- <button type="submit" onclick="validations()" class="btn btn-default submit_frontend">Submit</button> -->
         <input type="submit" name="submit" class="btn btn-default submit_frontend" value="Submit"/>
     </div>
   </div>
 </div>
</form>

</div>
</div>
</div>
<!-- <div class="row">
<div class="col-lg-4">
<div class="single-about-box">
<i class="icofont icofont-ruler-pencil"></i>
<h4>Responsive Design</h4>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text</p>
</div>
</div>
<div class="col-lg-4">
<div class="single-about-box active">
<i class="icofont icofont-computer"></i>
<h4>Fast Performance</h4>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text</p>
</div>
</div>
<div class="col-lg-4">
<div class="single-about-box">
<i class="icofont icofont-headphone-alt"></i>
<h4>Cross Platfrom</h4>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text</p>
</div>
</div>
</div> -->
</div>
</section>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
<script>
function validations(){
  // e.preventDefault();
  var email  = $.trim($('#email').val());
  var captcha_value=$('#captcha_value').val();
  var captcha_data=$('#captcha_data').val();
  var status = '';
  if(email=='')
  {
    $('#form_status').show();
    $('#form_status').addClass('alert-danger');
    $('#msg').text('Please enter email');
    $('#form_status').delay(4000).fadeOut("slow", function () { $(this).fadeOut(); });
    $('html, body').animate({
          scrollTop: $(".about-area").offset().top
      }, 1500);
      status = 'false';
    return false;
  }else
  {
    if (validateEmail(email))
    {
      status = 'true';
    $('.group_name_error').html('');
    }else
    {
      $('#form_status').show();
      $('#form_status').addClass('alert-danger');
      $('#msg').text('Please enter valid email');
      $('#form_status').delay(4000).fadeOut("slow", function () { $(this).fadeOut(); });
      $('html, body').animate({
            scrollTop: $(".about-area").offset().top
        }, 1500);
        status = 'false';
      return false;
    }
  }


  if(captcha_value=='')
  {
    $('#form_status').show();
    $('#form_status').addClass('alert-danger');
    $('#msg').text('Please enter captcha to continue');
    $('#form_status').delay(4000).fadeOut("slow", function () { $(this).fadeOut(); });
    $('html, body').animate({
          scrollTop: $(".about-area").offset().top
      }, 1500);
      status = 'false';
      return false;
  }else{
    if(captcha_value==captcha_data)
    {
      status = 'true';
    $('#form_status').hide();
    }else
    {
      $('#form_status').show();
      $('#form_status').addClass('alert-danger');
      $('#msg').text('Entered captcha text does not match');
      $('#form_status').delay(4000).fadeOut("slow", function () { $(this).fadeOut(); });
      $('html, body').animate({
            scrollTop: $(".about-area").offset().top
        }, 1500);
        status = 'false';
    return false;
    }
    $('#form_status').hide();
  }
  return status;
}


function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function captcha_refresh()
{

url = "<?php echo base_url("index.php/home/captcha_refresh");?>";
$.post(url, {}, function(data) {
if ($.trim(data)) {
$('#captcha').text(data);
$('#captcha_data').val(data);
}
});
}
</script>
<?php include 'footer.php' ?>
<script>
$("#fupForm").on('submit', function(e){
    e.preventDefault();
    var isValid = validations();
    if(isValid == 'true'){

      $.ajax({
          type: 'POST',
          url: '<?php echo base_url("index.php/home/submit_manu");?>',
          type : "POST",
          data: new FormData(this),
          contentType: false,
          cache: false,
          processData:false,
          beforeSend: function(){
              $('.submitBtn').attr("disabled","disabled");
              $('#fupForm').css("opacity",".5");
          },
          success: function(msg){
            if(msg){
              $('#fupForm').trigger("reset");
              $('#fupForm').css("opacity","1");
              $('#form_status').show();
              $('#form_status').addClass('alert-success');
              $('#msg').text('Email Sent Successfully');
              $('#form_status').delay(4000).fadeOut("slow", function () { $(this).fadeOut(); });
              $('html, body').animate({
                    scrollTop: $(".about-area").offset().top
                }, 1500);
            }
          }
      });
    }else{
      return false;
    }

});
</script>
