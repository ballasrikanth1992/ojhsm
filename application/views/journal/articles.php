<?php include "$_SERVER[DOCUMENT_ROOT]/escires/application/views/header.php" ?>

<?php include "$_SERVER[DOCUMENT_ROOT]/escires/application/views/inside_menu.php" ?>

<style>
header {
  width: 100%;
  display: inline-block;
  background-color: #404040;
}

.hamburger {
  height: 20px;
  width: 20px;
  padding: 20px;
  float: right;
  cursor: pointer;
}

.hamburger:before {
  content: "";
  display: block;
  background-color: #f3f3f3;
  width: 100%;
  height: 4px;
}

.hamburger:after {
  content: "";
  display: block;
  background-color: #f3f3f3;
  width: 100%;
  height: 4px;
  margin-top: 4px;
  box-shadow: 0px 8px 0 #f3f3f3;
}

nav {
  background-color: #2b2b2b;
  margin: 0;
  /* max-height: 0; */
  overflow: hidden;
  clear:both;
  transition: max-height .3s cubic-bezier(0.63, 0, 0.25, 1);
  /*float: left;  this is for seven tabs */
  /*width: 100%;  this is for seven tabs */
}

nav ul {
  margin: 0;
  padding: 0;
  list-style: none;
  display: block;
  /*float: left;  this is for seven tabs */
  /*width: 100%;  this is for seven tabs */
}

nav li {
  display: block;
  margin: 0;
  text-align: center;
}

nav a {
  color: white;
  display: block;
  padding: .4em;
}

header input[type="checkbox"]:checked ~ nav {
  max-height: 150px;
  border-bottom: #404040 5px solid;
}

header a:hover,
header a:focus,
header label:hover,
header label:focus {
  background-color: #191919;
}

@media (min-width: 700px) {
  .hamburger {
    display: none;
  }
  nav {
    background: transparent;
    float: right;
    border: 0 !important;
    max-height: none;
  }
  nav ul, nav li, nav li a {
    display: inline-block;
  }
  nav a {
    display: inline-block;
    padding: 15px 1em;
  }
}

.hidden {
  position: fixed;
  top: -100%;
  left: -100%;
}

header a:active{
    background-color: #191919;
}

.heading_active{
  background-color: #191919;
  color: #fff;
}
</style>

<!-- This is required to display the menu -->
<section class="hero-area">
</section>

<section class="about-area ptb-60">
<div class="container">
<div class="row">
<div class="col-lg-12">
<h2 style="margin-bottom: 15px;"><?php echo strtoupper($journal_details['journal_title']); ?><span class="sec-title-border"><span></span><span></span><span></span></span></h2>

<?php include 'journal_menu.php' ?>

<div class="about-us">
<div class="container">
      <div class="row tab-content">

        <div class="col-lg-12 tab-pan" id="articles_body" >
        <div class="journal_details">
          <?php
          $html = '';
          // echo"<pre>";print_r($article_details);
          $html .="<div class='row'>
          <div class='form-group col-lg-8'>";
          // if(!empty($category1_article_details)){
          //   $html .="<h3 class='gdlr-item-title' id='a_result'>Volume ".$category1_article_details[0]['volume']." Issue ".$category1_article_details[0]['issue']."</h3>";
          // }else{
          // }
          if(!empty($initial_article_1)){
          $html .="<h3 class='gdlr-item-title' id='a_result'>Volume ".$vol1." Issue ".$iss1."</h3>";
        }
          // $html .="</div>
          // <div class='form-group col-lg-4'>
          // <select onChange='articlesTip(this.value)' class='form-control'>";
          // if(!empty($category1_article_details)){
          //   foreach($category1_article_details as $category1_article_detail){
          //     $html .="<option value='".$category1_article_detail['volume'].",".$category1_article_detail['issue'].",".$category1_article_detail['journal_id']."'>Volume ".$category1_article_detail['volume']." Issue ".$category1_article_detail['issue']."</option>";
            // }
          // }
            $html .="</select>
          </div>
          </div>"; ?>

          <?php $html .="<div id='a_loop'>";
          if(!empty($initial_article_1)){

            // foreach($volumes as $key => $volume){
            //   $html .="<div class='col-lg-4 col-md-6'>
      			// 		    <div class='single-post'>
      			// 				<div class='post-details'>
      			// 					<h4>".$key."</h4>";
            //           foreach($volume as $issue){
            //           $html .="<p> Issue ".$issue['issue']."</p>";
            //           }
      			// 				$html .="</div>
      			// 			</div>
      			// 		</div>";
            // }
            // echo $html;
            foreach($initial_article_1 as $initial_article_1_data){
              if($initial_article_1_data['category'] == 1){
                $html .="<div class='gdlr-divider'></div>
                <div class='single-showcase-box'>
                <div class='row'>
                <div class='col-md-8' style='padding-top: 10px;'>
                <h4>" .$initial_article_1_data['article_title']."</h4>
                <p>" .$initial_article_1_data['author']. "</p>
                <p>" .$initial_article_1_data['article_type'].' : '.$initial_article_1_data['unique_article_id']. "</p>
                </div>
                <div class='col-md-4 align_button'>
                <div class='pull-right'>
                <a href='".base_url()."home/full_text/".$initial_article_1_data['journal_id']."' target='_blank' class='appao-btn appao-btn2'>View</a>
                <a href='".base_url()."articles/".$initial_article_1_data['pdf']."' target='_blank' class='appao-btn appao-btn2'>Download</a>
                </div>
                </div>
                </div>
                </div>";
              }else{
                $html ="<h3 class='gdlr-item-title'>No Articles</h3>";
              }
            }
          }else{
            $html ="<h3 class='gdlr-item-title'>No Articles</h3>";
          }

          echo $html;
          ?>
        </div>
        </div>
        </div>

      </div>
    </div>



</div>
<!-- <div class="clearfix"></div> -->
</div>
</div>
</div>
</section>
<?php include "$_SERVER[DOCUMENT_ROOT]/escires/application/views/footer.php" ?>
<script>
function articlesTip(data){
  volume = data.split(',')[0];
  issue = data.split(',')[1];
  jid = data.split(',')[2];
  document.getElementById("a_result").innerHTML = 'Volume '+volume+ ' Issue '+issue;
  $.ajax({
                 url:'<?php echo base_url(); ?>home/get_journals_by_order_1',
                 data:{vol : volume,iss: issue,jid: jid},
                 type: 'post',
                 success:function(res){
                   var output = JSON.parse(res);
                   var html = '';
                   $('#a_loop').empty();
                   $.each(output, function(index, value) {
                     var base_url = '<?php echo base_url() ;?>';
                       var html = "<div class='gdlr-divider'></div><div class='single-showcase-box'><div class='row'><div class='col-md-8' style='padding-top: 10px;'><h4>"+value.article_title+"</h4><p>" +value.author+ "</p><p>" +value.unique_article_id+ "</p></div><div class='col-md-4 align_button'><div class='pull-right'><a href='"+base_url+"home/full_text/"+value.journal_id+"' target='_blank' class='appao-btn appao-btn2'>View</a><a href='"+base_url+"articles/"+value.pdf+"' target='_blank' class='appao-btn appao-btn2'>Download</a></div></div></div></div>";
                     $('#a_loop').append(html);
                  });
                 }
             });
}

function dropdownTip(data){
  volume = data.split(',')[0];
  issue = data.split(',')[1];
  jid = data.split(',')[2];
  document.getElementById("aip_result").innerHTML = 'Volume '+volume+ ' Issue '+issue;
  $.ajax({
                 url:'<?php echo base_url(); ?>home/get_journals_by_order_2',
                 data:{vol : volume,iss: issue,jid: jid},
                 type: 'post',
                 success:function(res){
                   var output = JSON.parse(res);
                   var html = '';
                   $('#aip_loop').empty();
                   $.each(output, function(index, value) {
                     var base_url = '<?php echo base_url() ;?>';
                       var html = "<div class='gdlr-divider'></div><div class='single-showcase-box'><div class='row'><div class='col-md-8' style='padding-top: 10px;'><h4>"+value.article_title+"</h4><p>" +value.author+ "</p><p>" +value.unique_article_id+ "</p></div><div class='col-md-4 align_button'><div class='pull-right'><a href='"+base_url+"home/full_text/"+value.journal_id+"' target='_blank' class='appao-btn appao-btn2'>View</a><a href='"+base_url+"articles/"+value.pdf+"' target='_blank' class='appao-btn appao-btn2'>Download</a></div></div></div></div>";
                     $('#aip_loop').append(html);
                  });
                 }
             });
}
</script>
