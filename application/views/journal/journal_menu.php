<header>
<?php
$query_param = $this->uri->segment(1);
// echo $query_param;exit;
?>
  <nav>
  <ul id="myTab">
   <li class="col-xs-6 nav_mob active" ><a href="<?php echo base_url().$query_param;?>/about-journal">About Journal</a></li>
    <li class="col-xs-6 nav_mob" ><a href="<?php echo base_url().$query_param;?>/aims-and-scope" >Aims & Scope</a></li>
    <li class="col-xs-6 nav_mob" id="editorial_board"><a href="<?php echo base_url().$query_param;?>/editorial-board">Editorial Board</a></li>
    <li class="col-xs-6 nav_mob" id="articles"><a href="<?php echo base_url().$query_param;?>/articles">Articles</a></li>
    <li class="col-xs-6 nav_mob" ><a href="<?php echo base_url().$query_param;?>/articles-in-press">Articles in Press</a></li>
    <li class="col-xs-6 nav_mob" id="for_author"><a href="<?php echo base_url().$query_param;?>/for-author">For Author</a></li>
    <li class="col-xs-6 nav_mob" id="publication_charge"><a href="<?php echo base_url().$query_param;?>/publication-charges">Publication Charges</a></li>
    <li class="col-xs-6 nav_mob" id="indexing"><a href="<?php echo base_url().$query_param;?>/indexing">Indexing</a></li>
    <li class="col-xs-6 nav_mob" id="special_issues"><a href="<?php echo base_url().$query_param;?>/special-issues">Special Issues</a></li>
  </ul>
</nav>
</header>
