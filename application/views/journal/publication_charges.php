<?php include "$_SERVER[DOCUMENT_ROOT]/escires/application/views/header.php" ?>

<?php include "$_SERVER[DOCUMENT_ROOT]/escires/application/views/inside_menu.php" ?>

<style>
header {
  width: 100%;
  display: inline-block;
  background-color: #404040;
}

.hamburger {
  height: 20px;
  width: 20px;
  padding: 20px;
  float: right;
  cursor: pointer;
}

.hamburger:before {
  content: "";
  display: block;
  background-color: #f3f3f3;
  width: 100%;
  height: 4px;
}

.hamburger:after {
  content: "";
  display: block;
  background-color: #f3f3f3;
  width: 100%;
  height: 4px;
  margin-top: 4px;
  box-shadow: 0px 8px 0 #f3f3f3;
}

nav {
  background-color: #2b2b2b;
  margin: 0;
  /* max-height: 0; */
  overflow: hidden;
  clear:both;
  transition: max-height .3s cubic-bezier(0.63, 0, 0.25, 1);
  /*float: left;  this is for seven tabs */
  /*width: 100%;  this is for seven tabs */
}

nav ul {
  margin: 0;
  padding: 0;
  list-style: none;
  display: block;
  /*float: left;  this is for seven tabs */
  /*width: 100%;  this is for seven tabs */
}

nav li {
  display: block;
  margin: 0;
  text-align: center;
}

nav a {
  color: white;
  display: block;
  padding: .4em;
}

header input[type="checkbox"]:checked ~ nav {
  max-height: 150px;
  border-bottom: #404040 5px solid;
}

header a:hover,
header a:focus,
header label:hover,
header label:focus {
  background-color: #191919;
}

@media (min-width: 700px) {
  .hamburger {
    display: none;
  }
  nav {
    background: transparent;
    float: right;
    border: 0 !important;
    max-height: none;
  }
  nav ul, nav li, nav li a {
    display: inline-block;
  }
  nav a {
    display: inline-block;
    padding: 15px 1em;
  }
}

.hidden {
  position: fixed;
  top: -100%;
  left: -100%;
}

header a:active{
    background-color: #191919;
}

.heading_active{
  background-color: #191919;
  color: #fff;
}
</style>

<!-- This is required to display the menu -->
<section class="hero-area">
</section>

<section class="about-area ptb-60">
<div class="container">
<div class="row">
<div class="col-lg-12">
<h2 style="margin-bottom: 15px;"><?php echo strtoupper($journal_details['journal_title']); ?><span class="sec-title-border"><span></span><span></span><span></span></span></h2>

<?php include 'journal_menu.php' ?>

<div class="about-us">
<div class="container">
      <div class="row tab-content">

        <div class="col-lg-12 tab-pan" id="publication_charge_body" >
        <div class="journal_details">
          <?php
           echo $journal_details['publication_charges'];
          ?>
        </div>
        </div>

      </div>
    </div>



</div>
<!-- <div class="clearfix"></div> -->
</div>
</div>
</div>
</section>
<?php include "$_SERVER[DOCUMENT_ROOT]/escires/application/views/footer.php" ?>
