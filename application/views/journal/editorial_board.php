<?php include "$_SERVER[DOCUMENT_ROOT]/escires/application/views/header.php" ?>

<?php include "$_SERVER[DOCUMENT_ROOT]/escires/application/views/inside_menu.php" ?>

<style>
header {
  width: 100%;
  display: inline-block;
  background-color: #404040;
}

.hamburger {
  height: 20px;
  width: 20px;
  padding: 20px;
  float: right;
  cursor: pointer;
}

.hamburger:before {
  content: "";
  display: block;
  background-color: #f3f3f3;
  width: 100%;
  height: 4px;
}

.hamburger:after {
  content: "";
  display: block;
  background-color: #f3f3f3;
  width: 100%;
  height: 4px;
  margin-top: 4px;
  box-shadow: 0px 8px 0 #f3f3f3;
}

nav {
  background-color: #2b2b2b;
  margin: 0;
  /* max-height: 0; */
  overflow: hidden;
  clear:both;
  transition: max-height .3s cubic-bezier(0.63, 0, 0.25, 1);
  /*float: left;  this is for seven tabs */
  /*width: 100%;  this is for seven tabs */
}

nav ul {
  margin: 0;
  padding: 0;
  list-style: none;
  display: block;
  /*float: left;  this is for seven tabs */
  /*width: 100%;  this is for seven tabs */
}

nav li {
  display: block;
  margin: 0;
  text-align: center;
}

nav a {
  color: white;
  display: block;
  padding: .4em;
}

header input[type="checkbox"]:checked ~ nav {
  max-height: 150px;
  border-bottom: #404040 5px solid;
}

header a:hover,
header a:focus,
header label:hover,
header label:focus {
  background-color: #191919;
}

@media (min-width: 700px) {
  .hamburger {
    display: none;
  }
  nav {
    background: transparent;
    float: right;
    border: 0 !important;
    max-height: none;
  }
  nav ul, nav li, nav li a {
    display: inline-block;
  }
  nav a {
    display: inline-block;
    padding: 15px 1em;
  }
}

.hidden {
  position: fixed;
  top: -100%;
  left: -100%;
}

header a:active{
    background-color: #191919;
}

.heading_active{
  background-color: #191919;
  color: #fff;
}
</style>

<!-- This is required to display the menu -->
<section class="hero-area">
</section>

<section class="about-area ptb-60">
<div class="container">
<div class="row">
<div class="col-lg-12">
<h2 style="margin-bottom: 15px;"><?php echo strtoupper($journal_details['journal_title']); ?><span class="sec-title-border"><span></span><span></span><span></span></span></h2>

<?php include 'journal_menu.php' ?>

<div class="about-us">
<div class="container">
      <div class="row tab-content">

        <div class="col-lg-12 tab-pa" id="editorial_board_body" >
        <div class="journal_details">
          <!--<div class="row">-->
            <?php

            $cheif_head = '';
            $cheif_html = '';
            $executive_head = '';
            $executive_html = '';
            $associate_head = '';
            $associate_html = '';
            $assistant_head = '';
            $assistant_html = '';
            foreach($board_details as $board_detail){
                $board_name = str_replace(' ', '-', $board_detail['name']);
                if($board_detail['rank'] == 1){
                  $cheif_head ="<h2>Editor in chief</h2><div class='row'>";
                  $cheif_html .="<div class='col-lg-4 col-md-6'>
      					    <div class='single-post'>
      							<div class='post-thumbnail' style='text-align: center;'>
      								<img src='".base_url()."uploads/board/".$board_detail['image']."' alt='team' height='150' width='150'>
      							</div>
      							<div class='post-details'>
      								<h4 class='post-title'><a target='_blank' href='".base_url()."home/editorial_board/".$board_name."'><h4>".$board_detail['title'].' '.$board_detail['name']."</h4></a></h4>
                      <p>".$board_detail['university']."</p>
                      <p>".$board_detail['country']."</p>
      							</div>
      						</div>
      					</div>";
                }elseif ($board_detail['rank'] == 2) {
                  $executive_head ="<h2 style='padding-top: 15px;'>Executive Editor</h2><div class='row'>";
                  $executive_html .="<div class='col-lg-4 col-md-6'>
      					    <div class='single-post'>
      							<div class='post-thumbnail' style='text-align: center;'>
      								<img src='".base_url()."uploads/board/".$board_detail['image']."' alt='team' height='150' width='150'>
      							</div>
      							<div class='post-details'>
      								<h4 class='post-title'><a target='_blank' href='".base_url()."home/editorial_board/".$board_name."'><h4>".$board_detail['title'].' '.$board_detail['name']."</h4></a></h4>
                      <p>".$board_detail['university']."</p>
                      <p>".$board_detail['country']."</p>
      							</div>
      						</div>
      					</div>";
                }elseif ($board_detail['rank'] == 3) {
                  $associate_head ="<h2 style='padding-top: 15px;'>Associate Editor</h2><div class='row'>";
                  $associate_html .="<div class='col-lg-4 col-md-6'>
      					    <div class='single-post'>
      							<div class='post-thumbnail' style='text-align: center;'>
      								<img src='".base_url()."uploads/board/".$board_detail['image']."' alt='team' height='150' width='150'>
      							</div>
      							<div class='post-details'>
      								<h4 class='post-title'><a target='_blank' href='".base_url()."home/editorial_board/".$board_name."'><h4>".$board_detail['title'].' '.$board_detail['name']."</h4></a></h4>
                      <p>".$board_detail['university']."</p>
                      <p>".$board_detail['country']."</p>
      							</div>
      						</div>
      					</div>";
                }elseif ($board_detail['rank'] == 4) {
                  $assistant_head ="<h2 style='padding-top: 15px;'>Assistant Editor</h2><div class='row'>";
                  $assistant_html .="<div class='col-lg-4 col-md-6'>
      					    <div class='single-post'>
      							<div class='post-thumbnail' style='text-align: center;'>
      								<img src='".base_url()."uploads/board/".$board_detail['image']."' alt='team' height='150' width='150'>
      							</div>
      							<div class='post-details'>
      								<h4 class='post-title'><a target='_blank' href='".base_url()."home/editorial_board/".$board_name."'><h4>".$board_detail['title'].' '.$board_detail['name']."</h4></a></h4>
                      <p>".$board_detail['university']."</p>
                      <p>".$board_detail['country']."</p>
      							</div>
      						</div>
      					</div>";
                }
            }
            if (!empty($cheif_html)) {
                echo $cheif_head.''.$cheif_html.'</div>';    
            }
            if(!empty($executive_html)){
                echo $executive_head.''.$executive_html.'</div>';    
            }
            if(!empty($associate_html)){
                echo $associate_head.''.$associate_html.'</div>';        
            }
            if(!empty($assistant_html)){
                echo $assistant_head.''.$assistant_html.'</div>';    
            }
            
            ?>
        <!--</div>-->
        </div>
      </div>

      </div>
    </div>



</div>
<!-- <div class="clearfix"></div> -->
</div>
</div>
</div>
</section>
<?php include "$_SERVER[DOCUMENT_ROOT]/escires/application/views/footer.php" ?>
