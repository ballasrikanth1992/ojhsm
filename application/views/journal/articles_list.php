<?php include "$_SERVER[DOCUMENT_ROOT]/escires/application/views/header.php" ?>

<?php include "$_SERVER[DOCUMENT_ROOT]/escires/application/views/inside_menu.php" ?>

<style>
header {
  width: 100%;
  display: inline-block;
  background-color: #404040;
}

.hamburger {
  height: 20px;
  width: 20px;
  padding: 20px;
  float: right;
  cursor: pointer;
}

.hamburger:before {
  content: "";
  display: block;
  background-color: #f3f3f3;
  width: 100%;
  height: 4px;
}

.hamburger:after {
  content: "";
  display: block;
  background-color: #f3f3f3;
  width: 100%;
  height: 4px;
  margin-top: 4px;
  box-shadow: 0px 8px 0 #f3f3f3;
}

nav {
  background-color: #2b2b2b;
  margin: 0;
  /* max-height: 0; */
  overflow: hidden;
  clear:both;
  transition: max-height .3s cubic-bezier(0.63, 0, 0.25, 1);
  /*float: left;  this is for seven tabs */
  /*width: 100%;  this is for seven tabs */
}

nav ul {
  margin: 0;
  padding: 0;
  list-style: none;
  display: block;
  /*float: left;  this is for seven tabs */
  /*width: 100%;  this is for seven tabs */
}

nav li {
  display: block;
  margin: 0;
  text-align: center;
}

nav a {
  color: white;
  display: block;
  padding: .4em;
}

header input[type="checkbox"]:checked ~ nav {
  max-height: 150px;
  border-bottom: #404040 5px solid;
}

header a:hover,
header a:focus,
header label:hover,
header label:focus {
  background-color: #191919;
}

@media (min-width: 700px) {
  .hamburger {
    display: none;
  }
  nav {
    background: transparent;
    float: right;
    border: 0 !important;
    max-height: none;
  }
  nav ul, nav li, nav li a {
    display: inline-block;
  }
  nav a {
    display: inline-block;
    padding: 15px 1em;
  }
}

.hidden {
  position: fixed;
  top: -100%;
  left: -100%;
}

header a:active{
    background-color: #191919;
}

.heading_active{
  background-color: #191919;
  color: #fff;
}

.collapsible {
  background-color: #777;
  color: white;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
}

.active, .collapsible:hover {
  background-color: #555;
}

.content {
  padding: 0 18px;
  display: none;
  overflow: hidden;
  background-color: #f1f1f1;
}

.card-headers {
    padding: .75rem 1.25rem;
    margin-bottom: 0;
    background-color: rgba(0,0,0,.03);
    border-bottom: 1px solid rgba(0,0,0,.125);
}
</style>

<!-- <link href="<?php echo base_url();?>assets/css/foundation.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/normalize.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/style-new.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<script src="<?php echo base_url();?>assets/js/vendor/modernizr.js"></script> -->

<!-- This is required to display the menu -->
<section class="hero-area">
</section>

<section class="about-area ptb-60">
<div class="about-us">
<div class="container">
<div class="row">
<div class="col-lg-12">
<h2 style="margin-bottom: 15px;"><?php echo strtoupper($journal_details['journal_title']); ?><span class="sec-title-border"><span></span><span></span><span></span></span></h2>

<?php include 'journal_menu.php';
if(!empty($volumes)){
?>
<h2 style="margin-bottom: 15px;">Articles<span class="sec-title-border"><span></span><span></span><span></span></span></h2>

<?php
}
$html = '';
if(!empty($volumes)){
foreach($volumes as $key => $volume){
  // echo"<pre>";print_r($volume);exit;
  $html .="<div class='card-headers collapsible'><button class='btn btn-link'>Volume ".$key."</button></div><div class='content'>";
          foreach($volume as $issue){
            $article_link = base_url().$query_param.'/articles/volume-'.$key.'-issue-'.$issue['issue'];
            // echo $article_link;exit;
          $html .="<li><a class='btn-link' style='font-size: 16px;' href=".$article_link."> Issue ".$issue['issue']."</a></li><hr>";
          }
        $html .="</div>";
}
}
else{
  $html ="<h3 class='gdlr-item-title'>No Articles</h3>";
}
echo $html;
?>
</div></div></div></div>


<script>
var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
    } else {
      content.style.display = "block";
    }
  });
}
</script>


<!-- <div class="about-us">
<div class="container">
      <div class="row tab-content">

        <div class="col-lg-12 tab-pa" id="editorial_board_body" >
        <div class="journal_details"> -->

            <?php
            // $html='';
            // $html .="<div class='row'>";
            // foreach($volumes as $key => $volume){
            //   // echo"<pre>";print_r($volume);exit;
            //   $html .="<div class='col-lg-4 col-md-6'>
      			// 		    <div class='single-post'>
      			// 				<div class='post-details'>
      			// 					<h4>".$key."</h4>";
            //           foreach($volume as $issue){
            //           $html .="<p> Issue ".$issue['issue']."</p>";
            //           }
      			// 				$html .="</div>
      			// 			</div>
      			// 		</div>";
            // }
            // $html .="</div>";
            // echo $html;
            ?>

        <!-- </div>
      </div>

      </div>
    </div> -->



</div>
<!-- <div class="clearfix"></div> -->
</div>
</div>
</div>
</section>

<!-- <script src="<?php echo base_url();?>assets/js/vendor/jquery.js"></script>
<script src="<?php echo base_url();?>assets/js/vendor/foundation.min.js"></script>
<script>
$(document).foundation();
</script> -->
<?php include "$_SERVER[DOCUMENT_ROOT]/escires/application/views/footer.php" ?>
