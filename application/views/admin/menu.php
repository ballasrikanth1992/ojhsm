    <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url();?>admin/dashboard" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>Admin</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url();?>admin_css/dist/img/user2-160x160.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->session->userdata('admin_name');?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url();?>admin_css/dist/img/user2-160x160.png" class="img-circle" alt="User Image">

                <p>
                  <?php echo $this->session->userdata('admin_name');?>
                  <small>Admin</small>
                </p>
              </li>
              <!-- Menu Body -->

              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url();?>admin/edit_profile" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url();?>admin/log_out" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->

        </ul>
      </div>
    </nav>
  </header>
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url();?>admin_css/dist/img/user2-160x160.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p> <?php echo $this->session->userdata('admin_name');?></p>

        </div>
      </div>
      <!-- search form -->

      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">





		    <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Homepage Banners</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url();?>admin/banners_list"><i class="fa fa-circle-o"></i> All Homepage Banners</a></li>
            <li><a href="<?php echo base_url();?>admin/add_banner"><i class="fa fa-circle-o"></i> Add Homepage Banners</a></li>

          </ul>
        </li> -->
        <li><a href="<?php echo base_url();?>admin/home"><i class="fa fa-home text-red"></i> <span>Home</span></a></li>
        <li><a href="<?php echo base_url();?>admin/add_journals"><i class="fa fa-plus-square text-red"></i> <span>Add Journal</span></a></li>
        <li><a href="<?php echo base_url();?>admin/add_article"><i class="fa fa-plus-square text-red"></i> <span>Add Article</span></a></li>
        <li><a href="<?php echo base_url();?>admin/add_board"><i class="fa fa-plus-square text-red"></i> <span>Add Board</span></a></li>
        <li><a href="<?php echo base_url();?>admin/view_journals"><i class="fa fa-table text-red"></i> <span>View Journals</span></a></li>
        <li><a href="<?php echo base_url();?>admin/view_articles"><i class="fa fa-list text-red"></i> <span>View Articles</span></a></li>
        <li><a href="<?php echo base_url();?>admin/view_boards"><i class="fa fa-columns text-red"></i> <span>View Boards</span></a></li>


		   <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-picture-o" aria-hidden="true"></i>
            <span>Home Page  Categories</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url();?>admin/gallery_list"><i class="fa fa-circle-o"></i>Home Page Categories List</a></li>
            <li><a href="<?php echo base_url();?>admin/add_gallery"><i class="fa fa-circle-o"></i> Add Home Page  Categories</a></li>

          </ul>
        </li>
					   <li class="treeview">
          <a href="#">
            <i class="fa fa-picture-o" aria-hidden="true"></i>
            <span> Gallery</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url();?>admin/clients_list"><i class="fa fa-circle-o"></i>  Gallery List</a></li>
            <li><a href="<?php echo base_url();?>admin/add_clients"><i class="fa fa-circle-o"></i> Add  Gallery</a></li>

          </ul>
        </li>

				   <li class="treeview">
          <a href="#">
            <i class="fa fa-picture-o" aria-hidden="true"></i>
            <span> Products</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url();?>admin/sister_compannies_list"><i class="fa fa-circle-o"></i>  Products List</a></li>
            <li><a href="<?php echo base_url();?>admin/add_siter_company"><i class="fa fa-circle-o"></i> Add  Products</a></li>

          </ul>
        </li>


				   <li class="treeview">
          <a href="#">
            <i class="fa fa-picture-o" aria-hidden="true"></i>
            <span> Services</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url();?>admin/services_list_list"><i class="fa fa-circle-o"></i>  Services List</a></li>
            <li><a href="<?php echo base_url();?>admin/add_services_list"><i class="fa fa-circle-o"></i> Add  Services</a></li>

          </ul>
        </li>


			   <li class="treeview">
          <a href="#">
            <i class="fa fa-fw fa-hospital-o"></i>
            <span>Contact page</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url();?>admin/contacts_list"><i class="fa fa-circle-o"></i> Contacts List</a></li>


          </ul>
        </li> -->

 <li class="header">Profile</li>
        <li><a href="<?php echo base_url();?>admin/edit_profile"><i class="fa fa-circle-o text-red"></i> <span>Edit Profile</span></a></li>
        <li><a href="<?php echo base_url();?>admin/update_password"><i class="fa fa-unlock-alt text-red" aria-hidden="true"></i> <span>Change Password</span></a></li>
        <li><a href="<?php echo base_url();?>admin/log_out"><i class="fa fa-sign-out text-red" aria-hidden="true"></i> <span>Log out</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
