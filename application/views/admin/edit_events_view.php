<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Add Events</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php  include('menu.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Add Events

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Add Events</a></li>
        <!-- <li class="active">Edit Profile</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">

        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">

		  <?php echo $this->session->flashdata('cmsg');
      $str = explode(' ', $event_details[0]['event_date'], 2);
      ?>

 <div class="col-md-8">
   <h1>Update Event Details</h1>
 <form method="POST" class="form-horizontal" action="<?php echo base_url();?>admin/update_edited_events" accept-charset="UTF-8" enctype="multipart/form-data">
   <input type="hidden" name="id" value="<?php echo $event_details[0]['id'];?>" />
              <div class="box-body">
                <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label">Event Type</label>
                      <div class="col-sm-9">
                        <select name="select_event" class="selectpicker" id="select_event" onchange="change_val()">
                          <option>select</option>
                          <?php foreach($event_types as $each_event_type){
                              // print_r($event_name);die();
                              if($event_details[0]['event_type_id'] == $each_event_type['id']){
                                $selected = 'selected';
                                echo '<option value="'.$each_event_type['id'].'" selected>'.$each_event_type['name'].'</option>';
                              }else{
                                $selected = '';
                                echo '<option value="'.$each_event_type['id'].'">'.$each_event_type['name'].'</option>';
                              }
                          }?>
                            <!-- <option>Ketchup</option>
                            <option>Relish</option> -->
                        </select>
                      </div>
                </div>
			      <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Event Title</label>

                  <div class="col-sm-9">
                <input name="event_title" value="<?php echo $event_details[0]['event_title'];?>" id="event_title" class="form-control required" type="text">

                  </div>
                </div>
					      <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Event Date</label>

                  <div class="col-sm-4">
                    <div class="input-group">
                    <input name="datepicker" value="<?php echo $str[0];?>" id="datepicker" class="form-control required" type="text" readonly>
                      <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                      </div>
                    </div>
                  </div>

                  <div class="col-sm-5">
                    <div class="input-group">
                  <input name="time_element" value="<?php echo $str[1];?>" id="time_element" class="form-control required" type="text" readonly>
                  <div class="input-group-addon">
                    <span class="glyphicon glyphicon-time"></span>
                  </div>
                </div>
                </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Venue Details</label>

                  <div class="col-sm-9">
                     <input name="venue_details" value="<?php echo $event_details[0]['venue_details'];?>" id="venue_details" class="form-control required" type="text" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Event Summary</label>

                  <div class="col-sm-9">
                     <input name="event_summary" value="<?php echo $event_details[0]['summary'];?>" id="event_summary" class="form-control required" type="text" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Name1</label>

                  <div class="col-sm-9">
                     <input name="name1" value="<?php echo $event_details[0]['name1'];?>" id="name1" class="form-control required" type="text">
                  </div>
                </div>
                <div class="form-group" id="name2_div">
                  <label for="name2" class="col-sm-3 control-label">Name2</label>

                  <div class="col-sm-9">
                     <input name="name2" value="<?php echo $event_details[0]['name2'];?>" id="name2" class="form-control required" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Embedded Link</label>

                  <div class="col-sm-9">
                     <input name="embedded_link" value="<?php echo $event_details[0]['live_streaming_link'];?>" id="embedded_link" class="form-control required" type="text" >
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Banner Image</label>

                  <div class="col-sm-9">
                     <input type="file" name="banner_image" id="banner_image_event" class="form-control required" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Circle Image Boy</label>

                  <div class="col-sm-9">
                     <input type="file" name="circle_image_boy" id="circle_image_boy" class="form-control required" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Circle Image Girl</label>

                  <div class="col-sm-9">
                     <input type="file" name="circle_image_girl" id="circle_image_girl" class="form-control required" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Gallery Images</label>

                  <div class="col-sm-9">
                     <input type="file" name="files[]" id="gallery_image" multiple="multiple" class="form-control required" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Boy Text</label>

                  <div class="col-sm-9">
                     <textarea rows="4" cols="50" id="boy_text" name="boy_text"><?php echo $event_details[0]['boy_text'];?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-3 control-label">Girl Text</label>

                  <div class="col-sm-9">
                     <textarea rows="4" cols="50" id="girl_text" name="girl_text"><?php echo $event_details[0]['girl_text'];?></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                        <input type="submit" name="submit" value="Add" class="btn btn-info pull-right">
                    </div>
                  </div>
                </div>
              </div>
            </form>
        </div>
            <!-- /.col -->



            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->

      </div>
      <!-- /.box -->



    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>admin_css/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>admin_css/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url();?>admin_css/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url();?>admin_css/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url();?>admin_css/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url();?>admin_css/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url();?>admin_css/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url();?>admin_css/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url();?>admin_css/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url();?>admin_css/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?php echo base_url();?>admin_css/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>admin_css/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url();?>admin_css/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>admin_css/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>admin_css/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>admin_css/dist/js/demo.js"></script>
<!-- Page script -->
<!-- <link href="http://senthilraj.github.io/TimePicki/css/timepicki.css" rel="stylesheet"/> -->
<!-- <link href="<?php echo base_url();?>admin_css/plugins/dateTimePicker/timepicki.css" rel="stylesheet"/> -->
<link rel="stylesheet" href="<?php echo base_url();?>admin_css/plugins/dateTimePicker/timepicki.css">
<script src="<?php echo base_url();?>admin_css/plugins/dateTimePicker/timepicki.js"></script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    $("#time_element").timepicki()

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
  function change_val() {
   var sel = document.getElementById('select_event');
   if(sel.selectedIndex == 1) { //marriage
      $("#name2").show();
      $("#name2_div").show();
   } else {
      // $("#name2").hide();
      $("#name2_div").hide();
   }
}
//
// $(document).ready(function(){
//   var sel = document.getElementById('select_event');
//   alert(sel.selectedIndex);
//   if(sel.selectedIndex === 1) { //marriage
//      $("#name2").show();
//   } else {
//      // $("#name2").hide();
//      $("#name2_div").hide();
//   }
// })
</script>
</body>
</html>
