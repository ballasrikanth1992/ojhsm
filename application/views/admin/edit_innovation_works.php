<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Edit Innovation Works</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php  include('menu.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
     Edit Images
      
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Innovation Works </a></li>
        <li class="active">Edit Images </li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
   
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
		  
		  <?php echo $this->session->flashdata('cmsg');?>
		 
	   
 <div class="col-md-10">
 <form method="POST" class="form-horizontal" action="<?php echo base_url();?>admin/edit_innovation_works" accept-charset="UTF-8" enctype="multipart/form-data">
 <input type="hidden" name="gallery_id" value="<?php echo $edit_banner['gallery_id'];?>" />

              <div class="box-body">
			      <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Gallery Title</label>

                  <div class="col-sm-6">
                <input name="banner_title" value="<?php echo $edit_banner['gallery_title'];?>" id="demo-hor-1" class="form-control required col-md-6" type="text" required>

                  </div>
                </div>
			 <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Gallery Image</label>

                  <div class="col-sm-6">
                <input name="banner_image"  class="form-control required col-md-6" type="file" id="service_image" >
 <?php
								   if($edit_banner['gallery_image']!='')
								   {
								   ?>
                                   <img src="<?php  echo base_url();?>uploads/banners/<?php echo $edit_banner['gallery_image'];?>" width="50" height="50"/>
                                   <?php
								   }
								   ?>
                  </div>
                </div>
			
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Sort Order</label>

                  <div class="col-sm-6">
                <input name="sort_order" value="<?php echo $edit_banner['sort_order'];?>" id="demo-hor-1" class="form-control required col-md-6" type="text" >

                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-3">
                    <div class="checkbox">
                        <input type="submit" name="submit" value="Update" class="btn btn-info pull-left">
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
          
              <!-- /.box-footer -->
            </form>
        </div>
            <!-- /.col -->

			
			
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
       
      </div>
      <!-- /.box -->



    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>admin_css/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>admin_css/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url();?>admin_css/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url();?>admin_css/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url();?>admin_css/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url();?>admin_css/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url();?>admin_css/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url();?>admin_css/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url();?>admin_css/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url();?>admin_css/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?php echo base_url();?>admin_css/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>admin_css/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url();?>admin_css/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>admin_css/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>admin_css/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>admin_css/dist/js/demo.js"></script>
<!-- Page script -->
<script>
function open_div(t)
{
	if(t=='video')
	{
	$('#service_video').show();	
	$('#service_image').hide();	
	}else
	{
	$('#service_video').hide();	
	$('#service_image').show();		
		
	}
	
	
}
</script>
</body>
</html>
