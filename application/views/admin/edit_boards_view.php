<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Edit Board</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>admin_css/dist/css/skins/_all-skins.min.css">

  <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=qagffr3pkuv17a8on1afax661irst1hbr4e6tbv888sz91jc"></script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <?php  include('menu.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Edit Board

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Edit Board</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">

        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">

		  <?php echo $this->session->flashdata('cmsg');
      // print_r($journal_details);die();
      // $str = explode(' ', $event_details[0]['event_date'], 2);
      ?>

 <div class="col-md-8">
   <h1>Update Board Details</h1>
   <form method="POST" class="form-horizontal" action="<?php echo base_url();?>admin/edit_board_logic" accept-charset="UTF-8" enctype="multipart/form-data">
                <div class="box-body">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Select Journal</label>
                    <div class="col-sm-9">
                      <select name="select_journal" class="form-control selectpicker" id="select_journal" onchange="change_val()">
                        <option>select</option>
                        <?php foreach($journals as $journal){
                          if( $board_details[0]['journal_id'] == $journal['journal_id']){
                            echo '<option value="'.$journal['journal_id'].'" selected>'.$journal['journal_title'].'</option>';
                          }else{
                            $selected = '';
                            echo '<option value="'.$journal['journal_id'].'">'.$journal['journal_title'].'</option>';
                          }
                    }?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Editor/Reviewer</label>
                    <div class="col-sm-9">
                      <select name="editor_reviewer" class="form-control selectpicker" id="editor_reviewer" onchange="change_val()">
                        <option>select</option>
                          <option value='1' <?php if ($board_details[0]['editor_reviewer'] == 1) { echo ' selected="selected"'; } ?>>Editor</option>
                          <option value='2' <?php if ($board_details[0]['editor_reviewer'] == 2) { echo ' selected="selected"'; } ?>>Reviewer</option>
                      </select>
                    </div>
                  </div>
  			      <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Name</label>
                    <div class="col-sm-9">
                  <input name="name" id="name" class="form-control required" type="text" value="<?php echo $board_details[0]['name'];?>">
                  <input name="board_id" id="board_id" class="form-control" type="hidden" value="<?php echo $board_details[0]['id'];?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Title</label>
                    <div class="col-sm-9">
                      <select name="title" class="form-control selectpicker" id="title">
                        <option>select</option>
                          <option value='Mr.'  <?php if ($board_details[0]['title'] == 'Mr.') { echo ' selected="selected"'; } ?>>Mr.</option>
                          <option value='Ms.'  <?php if ($board_details[0]['title'] == 'Ms.') { echo ' selected="selected"'; } ?>>Ms.</option>
                          <option value='Mrs.'  <?php if ($board_details[0]['title'] == 'Mrs.') { echo ' selected="selected"'; } ?>>Mrs.</option>
                          <option value='Dr.'  <?php if ($board_details[0]['title'] == 'Dr.') { echo ' selected="selected"'; } ?>>Dr.</option>
                          <option value='Prof.' <?php if ($board_details[0]['title'] == 'Prof.') { echo ' selected="selected"'; } ?>>Prof.</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Rank</label>
                    <div class="col-sm-9">
                      <select name="rank" class="form-control selectpicker" id="rank" onchange="change_val()">
                        <option>select</option>
                          <option value='1'  <?php if ($board_details[0]['rank'] == '1') { echo ' selected="selected"'; } ?>>Editor in Chief</option>
                          <option value='2'  <?php if ($board_details[0]['rank'] == '2') { echo ' selected="selected"'; } ?>>Executive Editor</option>
                          <option value='3' <?php if ($board_details[0]['rank'] == '3') { echo ' selected="selected"'; } ?>>Associate Editor</option>
                          <option value='4' <?php if ($board_details[0]['rank'] == '4') { echo ' selected="selected"'; } ?>>Assistant Editor</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Affliation</label>
                        <div class="col-sm-9">
                      <input name="affliation" id="affliation" class="form-control required" type="text" value="<?php echo $board_details[0]['affliation'];?>">
                        </div>
                      </div>
                      <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">University</label>
                            <div class="col-sm-9">
                          <input name="university" id="university" class="form-control required" type="text" value="<?php echo $board_details[0]['university'];?>">
                            </div>
                          </div>
                          <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Country</label>
                                <div class="col-sm-9">
                              <input name="country" id="country" class="form-control required" type="text" value="<?php echo $board_details[0]['country'];?>">
                                </div>
                              </div>
                          <div class="form-group">
                                     <label for="inputEmail3" class="col-sm-3 control-label">Image</label>
                                     <div class="col-sm-9">
                                   <input name="service_image"  class="form-control required col-md-6" type="file" id="service_image" >
                                     </div>
                                   </div>

  					      <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">Bio</label>
                    <div class="col-sm-9">
                      <textarea id="basic-example" name="bio"><?php echo $board_details[0]['bio'];?></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                        <label for="inputEmail3" class="col-sm-3 control-label">Research Interest</label>
                        <div class="col-sm-9">
                      <input name="research_intrest" id="research_intrest" class="form-control required" type="text" value="<?php echo $board_details[0]['research_intrest'];?>">
                        </div>
                      </div>

                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <div class="checkbox">
                          <input type="submit" name="submit" value="Save" class="btn btn-info pull-right">
                      </div>
                    </div>
                  </div>
                </div>
              </form>
        </div>
            <!-- /.col -->



            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->

      </div>
      <!-- /.box -->



    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>admin_css/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>admin_css/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url();?>admin_css/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url();?>admin_css/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url();?>admin_css/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url();?>admin_css/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url();?>admin_css/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url();?>admin_css/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url();?>admin_css/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url();?>admin_css/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?php echo base_url();?>admin_css/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>admin_css/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url();?>admin_css/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>admin_css/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>admin_css/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>admin_css/dist/js/demo.js"></script>
<!-- Page script -->
<!-- <link href="http://senthilraj.github.io/TimePicki/css/timepicki.css" rel="stylesheet"/> -->
<!-- <link href="<?php echo base_url();?>admin_css/plugins/dateTimePicker/timepicki.css" rel="stylesheet"/> -->
<link rel="stylesheet" href="<?php echo base_url();?>admin_css/plugins/dateTimePicker/timepicki.css">
<script src="<?php echo base_url();?>admin_css/plugins/dateTimePicker/timepicki.js"></script>
<script>

  $(function () {

    tinymce.init({
      selector: 'textarea#basic-example',
      height: 250,
      menubar: false,
      plugins: [
        'advlist autolink lists link image charmap print preview anchor textcolor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table paste code help wordcount'
      ],
      toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tiny.cloud/css/codepen.min.css'
      ]
    });
    tinymce.init({
      selector: 'textarea#aim_and_scope',
      height: 250,
      menubar: false,
      plugins: [
        'advlist autolink lists link image charmap print preview anchor textcolor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table paste code help wordcount'
      ],
      toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tiny.cloud/css/codepen.min.css'
      ]
    });

    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    $("#time_element").timepicki()

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    });

  })
  function change_val() {
   var sel = document.getElementById('select_event');
   if(sel.selectedIndex == 1) { //marriage
      $("#name2").show();
      $("#name2_div").show();
   } else {
      // $("#name2").hide();
      $("#name2_div").hide();
   }
}

tinymce.init({
  selector: 'textarea#article_fulltext',
  height: 250,
  menubar: false,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor textcolor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table paste code help wordcount'
  ],
  toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tiny.cloud/css/codepen.min.css'
  ]
});

</script>
</body>
</html>
