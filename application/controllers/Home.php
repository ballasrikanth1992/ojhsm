<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		// $q=$this->db->query("SELECT p.*,c.category_name FROM `products` p inner join categories c on c.`category_id`=p.`category_id` where product_id='$product_id'");
		$q = $this->db->query("SELECT * FROM journals INNER JOIN articles ON journals.journal_id=articles.journal_id INNER JOIN board on journals.journal_id=board.journal_id where journals.status = 1");
		$data['journals'] = $q->result_array();
		$indexes = $this->db->query("SELECT group_concat(image) as images, group_concat(title) as titles, journal_id  FROM indexes GROUP BY journal_id");
		$data['indexes'] = $indexes->result_array();		
		// echo "<pre>";print_r($data);die();
		 $this->load->view('index');
	}

	public function about_us(){
		$this->load->view('aboutus');
	}
	public function advisory_board(){
		$this->load->view('advisory_board');
	}
	public function contact_us(){
		$this->load->view('contact_us');
	}
	public function terms_and_cond(){
		$this->load->view('terms_and_cond');
	}
	public function submit_manuscript(){
		$get_journals = $this->db->query('SELECT * from journals');
		$data['journals'] = $get_journals->result_array();
		$this->load->view('submit_manuscript',$data);
	}

	public function join_as_reviewer(){
		$get_journals = $this->db->query('SELECT * from journals');
		$data['journals'] = $get_journals->result_array();
		$this->load->view('join_as_reviewer',$data);
	}


	public function join_as_editor(){
		$get_journals = $this->db->query('SELECT * from journals');
		$data['journals'] = $get_journals->result_array();
		$this->load->view('join_as_editor',$data);
	}

	public function guidelines(){
		$this->load->view('guidelines');
	}

	public function journals(){
		$get_journals = $this->db->query('SELECT * from journals ORDER BY journal_title');
		$data['journals'] = $get_journals->result_array();
		$this->load->view('journals',$data);
	}

	public function view_journal_details($jtit = NULL){
	    $query_param = str_replace("-"," ",trim($jtit));
		$get_jid = $this->db->query("SELECT journal_id FROM journals where journal_title = '$query_param'");
		$jid_arr = $get_jid->row_array();
		$jid = $jid_arr['journal_id'];
		$articles_query1 = $this->db->query("SELECT volume,issue,category,journal_id FROM articles where journal_id = $jid and category = 1 ORDER BY volume");
		$res1 = $articles_query1->result_array();
		$category1_article_details = array_unique($res1, SORT_REGULAR);
		$data['category1_article_details'] = $category1_article_details;
		$vol1 = (int)$category1_article_details[0]['volume'];
		$iss1 = (int)$category1_article_details[0]['issue'];

		$articles_query2 = $this->db->query("SELECT volume,issue,category,journal_id FROM articles where journal_id = $jid and category = 2 ORDER BY volume");
		$res2 = $articles_query2->result_array();
		$category2_article_details = array_unique($res2, SORT_REGULAR);
		$data['category2_article_details'] = $category2_article_details;
		$vol2 = (int)$category2_article_details[0]['volume'];
		$iss2 = (int)$category2_article_details[0]['issue'];

		$articles_query3 = $this->db->query("SELECT volume,issue,category,journal_id FROM articles where journal_id = $jid and category = 2 ORDER BY volume");
		$res3 = $articles_query3->result_array();
		$category3_article_details = array_unique($res3, SORT_REGULAR);
		$data['category3_article_details'] = $category3_article_details;
		$vol3 = (int)$category3_article_details[0]['volume'];
		$iss3 = (int)$category3_article_details[0]['issue'];

		// var_dump();die();
		$get_article_query_1 = $this->db->query("SELECT * FROM articles where journal_id = $jid and volume = $vol1 and issue = $iss1 and category = 1");
		$data['initial_article_1'] = $get_article_query_1->result_array();

		$get_article_query_2 = $this->db->query("SELECT * FROM articles where journal_id = $jid and volume = $vol2 and issue = $iss2 and category = 2");
		$data['initial_article_2'] = $get_article_query_2->result_array();
		// echo $this->db->last_query(); exit;
		// echo"<pre>";print_r($data['initial_article_2']);die();
		$get_article_query_3 = $this->db->query("SELECT * FROM articles where journal_id = $jid and volume = $vol3 and issue = $iss3 and category = 3");
		$data['initial_article_3'] = $get_article_query_3->result_array();


		$get_journals = $this->db->query("SELECT * FROM journals where status = 1 and journal_id = $jid");
		$data['journal_details'] = $get_journals->row_array();
		$get_indexing_query = $this->db->query("SELECT * FROM indexes where journal_id = $jid");
		$data['indexing_details'] = $get_indexing_query->result_array();
		$get_board_query = $this->db->query("SELECT * FROM board where journal_id = $jid");
		$data['board_details'] = $get_board_query->result_array();
		// echo"<pre>";print_r($data);die();
		$this->load->view('view_journal_details',$data);
	}

	public function get_journals_by_order_1(){
		$jid = $_POST['jid'];
		$iss = $_POST['iss'];
		$vol = $_POST['vol'];
		$get_article_query = $this->db->query("SELECT * FROM articles where journal_id = $jid and volume = $vol and issue = $iss and category = 1");
		$articles_by_order = $get_article_query->result_array();
// 		echo"<pre>"; print_($articles_by_order);
		echo json_encode($articles_by_order);
		exit;
	}

	public function get_journals_by_order_2(){
		$jid = $_POST['jid'];
		$iss = $_POST['iss'];
		$vol = $_POST['vol'];
		$get_article_query = $this->db->query("SELECT * FROM articles where journal_id = $jid and volume = $vol and issue = $iss and category = 2");
		$articles_by_order = $get_article_query->result_array();
		echo json_encode($articles_by_order);
		exit;
	}

	public function get_journals_by_order_3(){
		$jid = $_POST['jid'];
		$iss = $_POST['iss'];
		$vol = $_POST['vol'];
		$get_article_query = $this->db->query("SELECT * FROM articles where journal_id = $jid and volume = $vol and issue = $iss and category = 3");
		$articles_by_order = $get_article_query->result_array();
		echo json_encode($articles_by_order);
		exit;
	}

	public function full_text($jid = NULL){
		$q = $this->db->query("SELECT journals.journal_title, articles.full_text FROM journals INNER JOIN articles ON journals.journal_id=articles.journal_id where journals.journal_id=$jid");
		$data['journal_details'] = $q->row_array();
		// echo"<pre>";print_r($data);die();
		$this->load->view('view_full_text',$data);
	}

    public function editorial_board($eid = NULL){
		// print_r($eid);die();
		$name = $board_name = str_replace('-', ' ', $eid);
		$q = $this->db->query("SELECT * FROM board where name LIKE '$name'");
		$data['board_details'] = $q->row_array();
		if(is_array($data['board_details'])){
			$data['board_details']['status'] = true;
		}else{
			$data['board_details']['status'] = false;
		}
		// echo $this->db->last_query();exit;
		// echo"<pre>";print_r($data);die();
		$this->load->view('editorial_member',$data);
	}

	function captcha_refresh()
	{
	$seed = str_split('abcdefghijklmnopqrstuvwxyz'
		.'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
		.'0123456789!@#$%^&*()'); // and any other characters
		shuffle($seed); // probably optional since array_is randomized; this may be redundant
		$rand = '';
		foreach (array_rand($seed, 6) as $k) $rand .= $seed[$k];
		echo $captcha=$rand;
	    exit;

	}

    public function submit_query()
		{
			if(!empty($_POST['email'])){
				 // error_reporting(-1);
				 $from=$_POST['email'];
				 $subject=$_POST['subject'];
				 $to = 'submissions@escires.com';
				 $mail_message = '<html> <head></head> <body> You have a new query <br><br>
							<strong>Message:</strong> '.$_POST['message'].' </body></html>';
				 $sendEmail = $this->send_email_without_attachment($from,$subject,$mail_message,$to);
				 $reply_message = 'Thank you for your query, our team will get back to you.';
				 $reply_subject = 'Reply : Escires';
				 $sendEmail = $this->send_email_without_attachment($to,$reply_subject,$reply_message,$from);
				 echo $sendEmail;
				 if($sendEmail){
						 redirect('home/contact_us');
				 }else{
						redirect('home/contact_us');
				 }
			}

	}

	public function send_email_without_attachment($from,$subject,$message,$to){

	    	//error_reporting(-1);
	    $config = Array(
      'protocol' => 'smtp',
      'smtp_host' => 'tls://smtp-mail.outlook.com',
      'smtp_port' => 587,
      'smtp_crypto' => 'tls',
      'smtp_user' => 'contact@escires.com',
      'smtp_pass' => 'akhila@123',
      'mailtype' => 'html',
      'charset' => 'iso-8859-1',
      'wordwrap' => TRUE
    );

          $this->load->library('email', $config);
          $this->email->set_newline("\r\n");
          $this->email->from($from);
          $this->email->to($to);
          $this->email->subject($subject);
          $this->email->message($message);
		  //$this->email->attach($attachment);
		  $send = $this->email->send();
		  return $send;

// 		require_once(APPPATH.'libraries/SwiftMail/swift_required.php');


// $transport = Swift_SmtpTransport::newInstance('smtp.office365.com', 25, "tls")
// 		 ->setUsername('submissions@escires.com')
// 		 ->setPassword('Akhilapasupuleti@123@123')
// 		 ->setAuthMode('LOGIN');

// 		 $mailer = Swift_Mailer::newInstance($transport);

// 		 $send_message = Swift_Message::newInstance($subject)
// 		 ->setFrom(array($from))
// 		 ->setTo(array($to))
// 		 ->setBody($message,'text/html');
// 		 $result = $mailer->send($send_message);
// 		 return $result;
	}

	public function send_email_with_attachment($from,$subject,$message,$to,$attachment){
		require_once(APPPATH.'libraries/SwiftMail/swift_required.php');


$transport = Swift_SmtpTransport::newInstance('smtp.office365.com', 587, "tls")
		 ->setUsername('submissions@escires.com')
		 ->setPassword('Akhilapasupuleti@123')
		 ->setAuthMode('LOGIN');

		 $mailer = Swift_Mailer::newInstance($transport);

		 $send_message = Swift_Message::newInstance($subject)
		 ->setFrom(array($from))
		 ->setTo(array($to))
		 ->setBody($message,'text/html')
		 ->attach(Swift_Attachment::fromPath($attachment));


		 $result = $mailer->send($send_message);
		 return $result;
	}

	public function submit_manu(){
		if(!empty($_FILES['document']['name'])){
			//error_reporting(0);
			$img_date = date('Ymdhis');
			$upload_path = 'uploads/email';
			$tmp_img = $_FILES["document"]['tmp_name'];
			$pdf_name = $img_date.'_'.$_FILES['document']['name'];
			move_uploaded_file($tmp_img, "$upload_path/$img_date");
		}

		$attachment = base_url()."$upload_path/$img_date" ;
		$subject = "Manuscript from " .$_POST['name'];
		$from=$_POST['email'];
		$message="
		New Manuscript <br>
		Name:" .$_POST['name']. "<br>
		Country:" .$_POST['country']. "<br>
		Journal:" .$_POST['select_journal']. "<br>
		Manuscript titl:" .$_POST['title']. "<br>
		Manuscript type:" .$_POST['manu_type']. "<br>";
		$to = 'submissions@escires.com';
		$sendEmail = $this->send_email_with_attachment($from,$subject,$message,$to,$attachment);
		if($sendEmail){
			$success_to = $from;
			$fromEmail = 'submissions@escires.com';
			$subject = "Reply : Escires";
			$message = 'Thank you for your submission our team will get back to you.';
			$sendSuccessEmail = $this->send_email_without_attachment($fromEmail,$subject,$message,$success_to);
			redirect('home/submit_manuscript?is_success=1');
		}else{
			redirect('home/submit_manuscript?is_success=1');
		}
	}


}
