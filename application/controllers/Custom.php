<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Custom extends CI_Controller {

  function about_journal($jtit = null){
		$query_param = str_replace("-"," ",trim($jtit));
		$get_jid = $this->db->query("SELECT journal_id FROM journals where journal_title = '$query_param'");
		$jid_arr = $get_jid->row_array();
		$jid = $jid_arr['journal_id'];
		$get_journals = $this->db->query("SELECT * FROM journals where status = 1 and journal_id = $jid");
		$data['journal_details'] = $get_journals->row_array();
		$this->load->view('journal/about_journal',$data);
	}

  function aims_and_scope($jtit = null){
    // echo $jtit;exit;
    $query_param = str_replace("-"," ",trim($jtit));
    $get_jid = $this->db->query("SELECT journal_id FROM journals where journal_title = '$query_param'");
    $jid_arr = $get_jid->row_array();
    $jid = $jid_arr['journal_id'];
    $get_journals = $this->db->query("SELECT * FROM journals where status = 1 and journal_id = $jid");
    $data['journal_details'] = $get_journals->row_array();
    $this->load->view('journal/aims_and_scope',$data);
  }

  function editorial_board($jtit = null){
    $query_param = str_replace("-"," ",trim($jtit));
    $get_jid = $this->db->query("SELECT journal_id FROM journals where journal_title = '$query_param'");
    $jid_arr = $get_jid->row_array();
    $jid = $jid_arr['journal_id'];
    $get_journals = $this->db->query("SELECT * FROM journals where status = 1 and journal_id = $jid");
    $data['journal_details'] = $get_journals->row_array();
    $get_board_query = $this->db->query("SELECT * FROM board where journal_id = $jid");
		$data['board_details'] = $get_board_query->result_array();
    $this->load->view('journal/editorial_board',$data);
  }
	function articles($jtit = null){
    $query_param = str_replace("-"," ",trim($jtit));
    $get_jid = $this->db->query("SELECT journal_id FROM journals where journal_title = '$query_param'");
    $jid_arr = $get_jid->row_array();
    $jid = $jid_arr['journal_id'];

    $articles_query1 = $this->db->query("SELECT DISTINCT volume,issue,category,journal_id FROM articles where journal_id = $jid and category = 1 ORDER BY volume");
		$res1 = $articles_query1->result_array();
    // echo $this->db->last_query();exit;
    $finalArray = array();
    $finalArray1 = array();


    $return = array();
     foreach($res1 as $val) {
         $return[$val['volume']][] = $val;
     }
     // echo"<pre>";print_r($return);exit;
     $data['volumes']  = $return;
     $data['query_param'] = $jtit;

		// if(!empty($res1)){
		// $category1_article_details = array_unique($res1, SORT_REGULAR);
		// $data['category1_article_details'] = $category1_article_details;
		// $vol1 = (int)$category1_article_details[0]['volume'];
		// $iss1 = (int)$category1_article_details[0]['issue'];
    // $get_article_query_1 = $this->db->query("SELECT * FROM articles where journal_id = $jid and volume = $vol1 and issue = $iss1 and category = 1");
		// $data['initial_article_1'] = $get_article_query_1->result_array();
		// }
    $get_journals = $this->db->query("SELECT * FROM journals where status = 1 and journal_id = $jid");
    $data['journal_details'] = $get_journals->row_array();
    // echo"<pre>";print_r($data);exit;
    $this->load->view('journal/articles_list',$data);
  }

  function articles_list($jid,$vol_iss){
    $query_param = str_replace("-"," ",trim($jid));
    $get_jid = $this->db->query("SELECT journal_id FROM journals where journal_title = '$query_param'");
    $jid_arr = $get_jid->row_array();
    $jid = $jid_arr['journal_id'];
    // echo $vol_iss;exit;
    // $vol_iss = str_replace("-"," ",trim($vol_iss));
    $vol_iss = explode("-",trim($vol_iss));
    $data['vol1'] = $vol_iss[1];
    $data['iss1'] = $vol_iss[3];

    $get_article_query_1 = $this->db->query("SELECT * FROM articles where journal_id = $jid and volume = $vol_iss[1] and issue = $vol_iss[3] and category = 1");
    $data['initial_article_1'] = $get_article_query_1->result_array();

    $articles_query1 = $this->db->query("SELECT DISTINCT volume,issue,category,journal_id FROM articles where journal_id = $jid and category = 1 ORDER BY volume");
		$res1 = $articles_query1->result_array();
    if(!empty($res1)){
		$category1_article_details = array_unique($res1, SORT_REGULAR);
		$data['category1_article_details'] = $category1_article_details;
		$vol1 = (int)$category1_article_details[0]['volume'];
		$iss1 = (int)$category1_article_details[0]['issue'];
    // $get_article_query_1 = $this->db->query("SELECT * FROM articles where journal_id = $jid and volume = $vol1 and issue = $iss1 and category = 1");
		// $data['initial_article_1'] = $get_article_query_1->result_array();
		}
    $get_journals = $this->db->query("SELECT * FROM journals where status = 1 and journal_id = $jid");
    $data['journal_details'] = $get_journals->row_array();
    $this->load->view('journal/articles',$data);
  }
  //PAGE REDIRECTION FUNCTION
	// function articles_in_press($jtit = null){
  //   $query_param = str_replace("-"," ",trim($jtit));
  //   $get_jid = $this->db->query("SELECT journal_id FROM journals where journal_title = '$query_param'");
  //   $jid_arr = $get_jid->row_array();
  //   $jid = $jid_arr['journal_id'];
  //   $articles_query1 = $this->db->query("SELECT DISTINCT volume,issue,category,journal_id FROM articles where journal_id = $jid and category = 2 ORDER BY volume");
	// 	$res1 = $articles_query1->result_array();
  //   $finalArray = array();
  //   $finalArray1 = array();
  //   $return = array();
  //    foreach($res1 as $val) {
  //        $return[$val['volume']][] = $val;
  //    }
  //    $data['volumes']  = $return;
  //    $data['query_param'] = $jtit;
  //   $get_journals = $this->db->query("SELECT * FROM journals where status = 1 and journal_id = $jid");
  //   $data['journal_details'] = $get_journals->row_array();
  //   $this->load->view('journal/articles_in_press_list',$data);
  // }

  function articles_in_press($jtit = null){
    $query_param = str_replace("-"," ",trim($jtit));
    $get_jid = $this->db->query("SELECT journal_id FROM journals where journal_title = '$query_param'");
    $jid_arr = $get_jid->row_array();
    $jid = $jid_arr['journal_id'];

    $articles_query2 = $this->db->query("SELECT volume,issue,category,journal_id FROM articles where journal_id = $jid and category = 2 ORDER BY volume");
		$res2 = $articles_query2->result_array();

    if(!empty($res2)){
      $category2_article_details = array_unique($res2, SORT_REGULAR);
      $data['category2_article_details'] = $category2_article_details;
      $vol2 = (int)$category2_article_details[0]['volume'];
      $iss2 = (int)$category2_article_details[0]['issue'];
      $get_article_query_2 = $this->db->query("SELECT * FROM articles where journal_id = $jid and volume = $vol2 and issue = $iss2 and category = 2");
      $data['initial_article_2'] = $get_article_query_2->result_array();
      // print_r($data);exit;
    }
    $get_journals = $this->db->query("SELECT * FROM journals where status = 1 and journal_id = $jid");
    $data['journal_details'] = $get_journals->row_array();
    $this->load->view('journal/articles_in_press',$data);
  }

  function articles_in_press_list($jid,$vol_iss){
    $query_param = str_replace("-"," ",trim($jid));
    $get_jid = $this->db->query("SELECT journal_id FROM journals where journal_title = '$query_param'");
    $jid_arr = $get_jid->row_array();
    $jid = $jid_arr['journal_id'];
    $vol_iss = explode("-",trim($vol_iss));
    $data['vol1'] = $vol_iss[1];
    $data['iss1'] = $vol_iss[3];

    $get_article_query_1 = $this->db->query("SELECT * FROM articles where journal_id = $jid and volume = $vol_iss[1] and issue = $vol_iss[3] and category = 2");
    $data['initial_article_1'] = $get_article_query_1->result_array();

    $articles_query1 = $this->db->query("SELECT DISTINCT volume,issue,category,journal_id FROM articles where journal_id = $jid and category = 2 ORDER BY volume");
		$res1 = $articles_query1->result_array();
    if(!empty($res1)){
		$category1_article_details = array_unique($res1, SORT_REGULAR);
		$data['category1_article_details'] = $category1_article_details;
		$vol1 = (int)$category1_article_details[0]['volume'];
		$iss1 = (int)$category1_article_details[0]['issue'];
		}
    // echo"<pre>";print_r($data);exit;
    $get_journals = $this->db->query("SELECT * FROM journals where status = 1 and journal_id = $jid");
    $data['journal_details'] = $get_journals->row_array();
    $this->load->view('journal/articles_in_press',$data);
  }
	function for_author($jtit = null){
    $query_param = str_replace("-"," ",trim($jtit));
    $get_jid = $this->db->query("SELECT journal_id FROM journals where journal_title = '$query_param'");
    $jid_arr = $get_jid->row_array();
    $jid = $jid_arr['journal_id'];
    $get_journals = $this->db->query("SELECT * FROM journals where status = 1 and journal_id = $jid");
    $data['journal_details'] = $get_journals->row_array();
    $this->load->view('journal/for_author',$data);
  }
	function publication_charges($jtit = null){
    $query_param = str_replace("-"," ",trim($jtit));
    $get_jid = $this->db->query("SELECT journal_id FROM journals where journal_title = '$query_param'");
    $jid_arr = $get_jid->row_array();
    $jid = $jid_arr['journal_id'];
    $get_journals = $this->db->query("SELECT * FROM journals where status = 1 and journal_id = $jid");
    $data['journal_details'] = $get_journals->row_array();
    $this->load->view('journal/publication_charges',$data);
  }
	function indexing($jtit = null){
    $query_param = str_replace("-"," ",trim($jtit));
    $get_jid = $this->db->query("SELECT journal_id FROM journals where journal_title = '$query_param'");
    $jid_arr = $get_jid->row_array();
    $jid = $jid_arr['journal_id'];
    $get_journals = $this->db->query("SELECT * FROM journals where status = 1 and journal_id = $jid");
    $data['journal_details'] = $get_journals->row_array();
    $get_indexing_query = $this->db->query("SELECT * FROM indexes where journal_id = $jid");
		$data['indexing_details'] = $get_indexing_query->result_array();
    $this->load->view('journal/indexing',$data);
  }
  //PAGE REDIRECTION FUNCTION
	// function special_issues($jtit = null){
  //   $query_param = str_replace("-"," ",trim($jtit));
  //   $get_jid = $this->db->query("SELECT journal_id FROM journals where journal_title = '$query_param'");
  //   $jid_arr = $get_jid->row_array();
  //   $jid = $jid_arr['journal_id'];
  //   $articles_query1 = $this->db->query("SELECT DISTINCT volume,issue,category,journal_id FROM articles where journal_id = $jid and category = 3 ORDER BY volume");
	// 	$res1 = $articles_query1->result_array();
  //   $finalArray = array();
  //   $finalArray1 = array();
  //   $return = array();
  //    foreach($res1 as $val) {
  //        $return[$val['volume']][] = $val;
  //    }
  //    $data['volumes']  = $return;
  //    $data['query_param'] = $jtit;
  //   $get_journals = $this->db->query("SELECT * FROM journals where status = 1 and journal_id = $jid");
  //   $data['journal_details'] = $get_journals->row_array();
  //   $this->load->view('journal/special_issues_list',$data);
  // }

  function special_issues($jtit = null){
    $query_param = str_replace("-"," ",trim($jtit));
    $get_jid = $this->db->query("SELECT journal_id FROM journals where journal_title = '$query_param'");
    $jid_arr = $get_jid->row_array();
    $jid = $jid_arr['journal_id'];
    $articles_query3 = $this->db->query("SELECT volume,issue,category,journal_id FROM articles where journal_id = $jid and category = 3 ORDER BY volume");
    $res3 = $articles_query3->result_array();
    if(!empty($res3)){
      $category3_article_details = array_unique($res3, SORT_REGULAR);
      $data['category3_article_details'] = $category3_article_details;
      $vol3 = (int)$category3_article_details[0]['volume'];
      $iss3 = (int)$category3_article_details[0]['issue'];
      $get_article_query_3 = $this->db->query("SELECT * FROM articles where journal_id = $jid and volume = $vol3 and issue = $iss3 and category = 3");
      $data['initial_article_3'] = $get_article_query_3->result_array();
      // echo $this->db->last_query();exit;
      // print_r($data);exit;
    }
    $get_journals = $this->db->query("SELECT * FROM journals where status = 1 and journal_id = $jid");
    $data['journal_details'] = $get_journals->row_array();
    // echo '<pre>';print_r($data);die();
    $this->load->view('journal/special_issues',$data);
  }

  function special_issues_list($jid,$vol_iss){
    $query_param = str_replace("-"," ",trim($jid));
    $get_jid = $this->db->query("SELECT journal_id FROM journals where journal_title = '$query_param'");
    $jid_arr = $get_jid->row_array();
    $jid = $jid_arr['journal_id'];
    $vol_iss = explode("-",trim($vol_iss));
    $data['vol1'] = $vol_iss[1];
    $data['iss1'] = $vol_iss[3];

    $get_article_query_1 = $this->db->query("SELECT * FROM articles where journal_id = $jid and volume = $vol_iss[1] and issue = $vol_iss[3] and category = 3");
    $data['initial_article_1'] = $get_article_query_1->result_array();

    $articles_query1 = $this->db->query("SELECT DISTINCT volume,issue,category,journal_id FROM articles where journal_id = $jid and category = 3 ORDER BY volume");
		$res1 = $articles_query1->result_array();
    if(!empty($res1)){
		$category1_article_details = array_unique($res1, SORT_REGULAR);
		$data['category1_article_details'] = $category1_article_details;
		$vol1 = (int)$category1_article_details[0]['volume'];
		$iss1 = (int)$category1_article_details[0]['issue'];
		}
    // echo"<pre>";print_r($data);exit;
    $get_journals = $this->db->query("SELECT * FROM journals where status = 1 and journal_id = $jid");
    $data['journal_details'] = $get_journals->row_array();
    $this->load->view('journal/special_issues',$data);
  }

}
