<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
   public function __construct()
    {
        ini_set('display_errors', 0);
        parent::__construct();
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
$this->output->set_header('Pragma: no-cache');
$this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		 $this->layout=false;;
		$this->load->model("admin_model");

    }

	public function index()
	{
	    $this->layout=false;
		$this->load->view('admin/index');
	}
		public function log_out()
	{
	 $this->session->sess_destroy();
	 redirect('admin/index');

	}


	public function upload()
	{
	 $this->load->view('admin/upload');
	}


	public function check_login()
	{
	 if(!$this->session->userdata('admin_email'))
	 redirect('admin/index');

	}
		public function login_action()
	{

		if($this->input->post('submit'))
		{
		$userdata=$this->admin_model->KM_first(array("class"=>"admins","fields"=>array('*'),"conditions"=>array("admin_email"=>$this->input->post('email'),"admin_password"=>(md5($this->input->post('password'))))));
			if(!empty($userdata))
			{
        // print_r($userdata);die();
				$this->session->set_userdata($userdata);
				redirect('admin/home');
			}else

		{
		$msg='<div class = "alert alert-danger alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Please login with valid credentials
		</div>';
		 $this->session->set_flashdata('msg',$msg);
		redirect('admin/index');
		}
	   }
   }
   	public function contacts_list()
	{

	$this->check_login();

	$q  = $this->db->query("select * from  contacts");
	$data['contacts_list']=  $q->result_array();

	 $this->load->view('admin/contacts_list',$data);
	}
	public function inactive_users()
	{

	$this->check_login();

	$q  = $this->db->query("select * from  users where user_status='INACTIVE' order by id desc");
	$data['active_users']=  $q->result_array();

	 $this->load->view('admin/inactive_users',$data);
	}
	public function inactive_user($id)
	{

	$this->check_login();

	$q  = $this->db->query("update users set user_status='INACTIVE' where id='$id'");
	$msg='<div class = "alert alert-success alert-success">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		User Deactivated successfully.
		</div>';
		 $this->session->set_flashdata('msg',$msg);
		redirect('admin/active_users');
	}
	public function active_user($id)
	{

	$this->check_login();

	$q  = $this->db->query("update users set user_status='ACTIVE' where id='$id'");
	$msg='<div class = "alert alert-success alert-success">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		User Deactivated successfully.
		</div>';
		 $this->session->set_flashdata('msg',$msg);
		redirect('admin/active_users');
	}
		public function dashboard()
	{

	$this->check_login();

	}
	public function countries()
	{
	  $this->check_login();
	  $data['countries'] = $this->admin_model->km_all(array(
            "class" => 'countries'
        ));
	 $this->load->view('admin/countries_list',$data);
	}
	function seo_friendly_url($string)
	{
    $string = str_replace(array('[\', \']'), '', $string);
    $string = preg_replace('/\[.*\]/U', '', $string);
    $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
    $string = htmlentities($string, ENT_COMPAT, 'utf-8');
    $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
    $string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);
    return strtolower(trim($string, '-'));
    }
	public function add_service()
	{
	   if($this->input->post('submit'))
		{


		$service_title=$this->input->post('service_title');
		//$country_slug= $this->seo_friendly_url($country_name);

		$service_slug=$this->input->post('service_slug');

		$system_array=array('service_title'=>$service_title,
						    'service_slug'=>$service_slug,
							'type'=>$this->input->post('type')



		);
		if($_POST['type']=='image')
		{
					if(!empty($_FILES) && $_FILES['service_image']['name']!='')
		{
		$temp = explode(".", $_FILES["service_image"]["name"]);
		$newfilename = round(microtime(true)) . '.' . end($temp);
		if (move_uploaded_file($_FILES['service_image']['tmp_name'],'uploads/images/'.$newfilename)) {
		$system_array['service_image']=$newfilename;
		} else {

		}
		}
		}else
		{

		$system_array['service_video']=$this->input->post('service_video');
		}
		$this->admin_model->KM_save(array('class'=>'services','insert'=>$system_array));
		$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		service added successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
		redirect('admin/services_list');
		}

		$this->load->view('admin/add_service');
	}
	public function edit_service($service_id=NULL)
	{
	if($this->input->post('submit'))
	{
     $service_id=$this->input->post('service_id');
	$service_title=$this->input->post('service_title');
		//$country_slug= $this->seo_friendly_url($country_name);

		$service_slug=$this->input->post('service_slug');

		$system_array=array('service_title'=>$service_title,
						    'service_slug'=>$service_slug,
							'type'=>$this->input->post('type')



		);
		if($_POST['type']=='image')
		{
					if(!empty($_FILES) && $_FILES['service_image']['name']!='')
		{
		$temp = explode(".", $_FILES["service_image"]["name"]);
		$newfilename = round(microtime(true)) . '.' . end($temp);
		if (move_uploaded_file($_FILES['service_image']['tmp_name'],'uploads/images/'.$newfilename)) {
		$system_array['service_image']=$newfilename;
		} else {

		}
		}
		}else
		{

		$system_array['service_video']=$this->input->post('service_video');
		}
	$this->admin_model->KM_update(array("class"=>"services","update"=>$system_array),array('service_id'=>$service_id));
	 $msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		service Updated successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
	redirect('admin/services_list/');
	}
    $data['view_data']=$this->admin_model->KM_first(array("class"=>"services","fields"=>array('*'),"conditions"=>array("service_id"=>$service_id)));

	$data["service_id"]=$service_id;
	$this->load->view("admin/edit_service",$data);
	}

	public function delete_country($country_id)
	{
		if(isset($country_id) && $country_id != "")
		{

		$this->admin_model->KM_delete(array("class"=>"countries","conditions"=>array("id"=>$country_id)));
		}
		 $msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Country Deleted successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
	    redirect('admin/countries/');
	}
	public function admins_list()
	{
	$q  = $this->db->query("select count(id) as users_count from  users  where user_status='INACTIVE'");
	$data['admins_list']=  $q->row_array();
	 $data['admins_list']=$this->admin_model->cities_list(); ;
	 $this->load->view('admin/admins_list',$data);
	}
    	public function add_city()
	{
	   if($this->input->post('submit'))
		{


		$country_id	=$this->input->post('country_id');
		$city_name	=$this->input->post('city_name');
		$city_slug= $this->seo_friendly_url($city_name);
		$system_array=array('country_id'=>$country_id,
						    'city_name'=>$city_name,
							'city_slug'=>$city_slug

		);
		$this->admin_model->KM_save(array('class'=>'cities','insert'=>$system_array));
		$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		 City added successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
		redirect('admin/cities');
		}
		 $data['countries'] = $this->admin_model->km_all(array(
            "class" => 'countries'
        ));
		$this->load->view('admin/add_city',$data);
	}
	public function edit_city($city_id=null)
	{
	if($this->input->post('submit'))
	{
	$city_id=$this->input->post('city_id');
	$country_id	=$this->input->post('country_id');
	$city_name	=$this->input->post('city_name');
	$city_slug= $this->seo_friendly_url($city_name);
	$system_array=array('country_id'=>$country_id,
	'city_name'=>$city_name,
	'city_slug'=>$city_slug

	);
	$this->admin_model->KM_update(array("class"=>"cities","update"=>$system_array),array('city_id'=>$city_id));
	$msg='<div class = "alert alert-success alert-dismissable">
	<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
	&times;
	</button>
	City Updated successfully.
	</div>';
	$this->session->set_flashdata('cmsg',$msg);
	redirect('admin/cities');
	}
   $data['city_edit']=$this->admin_model->KM_first(array("class"=>"cities","fields"=>array('*'),"conditions"=>array("city_id"=>$city_id)));

	$data["city_id"]=$city_id;
	 $data['countries'] = $this->admin_model->km_all(array(
            "class" => 'countries'
        ));
	$this->load->view("admin/edit_city",$data);
	}

	public function delete_city($city_id)
	{
		if(isset($city_id) && $city_id != "")
		{

		$this->admin_model->KM_delete(array("class"=>"cities","conditions"=>array("city_id"=>$city_id)));
		}
		 $msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		City deleted successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
	   redirect('admin/cities');
	}

	public function categories()
	{
	  $data['categories'] = $this->admin_model->km_all(array(
            "class" => 'categories'
        ));
	 $this->load->view('admin/categories_list',$data);
	}
    	public function add_category()
	{
	   if($this->input->post('submit'))
		{

		$category_name = $this->input->post('category_name');
		$category_slug= $this->seo_friendly_url($category_name);
		$sort_order=$this->input->post('sort_order');
		$system_array=array('category_name'=>$category_name,
						    'category_slug'=>$category_slug,
							'sort_order'=>$sort_order


		);
				if($_FILES['course_image']['name']!='')
		{
			$date=date('Ymdhis');
			$configVideo['upload_path'] = 'uploads/category_images/';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$banner_image = $date.$_FILES['course_image']['name'];
			$configVideo['file_name'] = $banner_image;
			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('course_image')) {
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();

			$course_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array(
			"source_image"    => $videoDetails1['full_path'],
			"new_image"       => 'uploads/category_images/thumbnails/'.$banner_image,
			"maintain_ratio"  => FALSE,

			"width"		   => 280,
			"height"		   => 170	);
			$config['image_library'] = 'gd2';
			// creating thumbnail
			$this->load->library('image_lib');
			// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation

			// $this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}

			}

		$system_array['category_image']=$banner_image;
		}
		$this->admin_model->KM_save(array('class'=>'categories','insert'=>$system_array));
		$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		 Category added successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
		redirect('admin/categories');
		}

		$this->load->view('admin/add_category');
	}
	public function edit_category($category_id=null)
	{
	if($this->input->post('submit'))
	{
	$category_id=$this->input->post('category_id');
	$category_name = $this->input->post('category_name');
	$category_slug= $this->seo_friendly_url($category_name);
	$sort_order=$this->input->post('sort_order');
	$system_array=array('category_name'=>$category_name,
	'category_slug'=>$category_slug,
    'sort_order'=>$sort_order

	);
					if($_FILES['course_image']['name']!='')
		{
			$date=date('Ymdhis');
			$configVideo['upload_path'] = 'uploads/category_images/';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$banner_image = $date.$_FILES['course_image']['name'];
			$configVideo['file_name'] = $banner_image;
			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('course_image')) {
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();

			$course_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array(
			"source_image"    => $videoDetails1['full_path'],
			"new_image"       => 'uploads/category_images/thumbnails/'.$banner_image,
			"maintain_ratio"  => FALSE,

			"width"		   => 280,
			"height"		   => 170	);
			$config['image_library'] = 'gd2';
			// creating thumbnail
			$this->load->library('image_lib');
			// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation

			// $this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}

			}

		$system_array['category_image']=$banner_image;
		}
	$this->admin_model->KM_update(array("class"=>"categories","update"=>$system_array),array('category_id'=>$category_id));
	$msg='<div class = "alert alert-success alert-dismissable">
	<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
	&times;
	</button>
	Category Updated successfully.
	</div>';
	$this->session->set_flashdata('cmsg',$msg);
	redirect('admin/categories');
	}
   $data['category_edit']=$this->admin_model->KM_first(array("class"=>"categories","fields"=>array('*'),"conditions"=>array("category_id"=>$category_id)));
	$data["category_id"]=$category_id;
	$this->load->view("admin/edit_category",$data);
	}

	public function delete_category($category_id)
	{
		if(isset($category_id) && $category_id != "")
		{

		$this->admin_model->KM_delete(array("class"=>"categories","conditions"=>array("category_id"=>$category_id)));
		}
		 $msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Category deleted successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
	   redirect('admin/categories');
	}

		public function subcategories()
	{
		$q=$this->db->query('SELECT c.category_name,cc.sub_cat_name,cc.sub_cat_id,c.category_slug,cc.sort_order FROM `categories` c inner join sub_categories cc on cc.`category_id`=c.`category_id`');
		$data['categories'] = $q->result_array();
		$this->load->view('admin/sub_cat_list',$data);
	}
    	public function add_sub_category()
	{
	   if($this->input->post('submit'))
		{

		$category_name = $this->input->post('category_name');
		$category_id = $this->input->post('category_id');
		$category_slug= $this->seo_friendly_url($category_name);
	    $sort_order=$this->input->post('sort_order');
	    $status=$this->input->post('status');

	    $meta_title=$this->input->post('meta_title');
	    $meta_description=$this->input->post('meta_description');
	    $meta_keywords=$this->input->post('meta_keywords');
	    $canonical_url=$this->input->post('canonical_url');


		$system_array=array('category_id'=>$category_id,
		                    'sub_cat_name'=>$category_name,
				             'sub_cat_slug'=>$category_slug,
							 'sub_status'=>$status,
                                 'sort_order'=>$sort_order,
                                 'meta_title'=>$meta_title,
                                 'meta_description'=>$meta_description,
                                 'meta_keywords'=>$meta_keywords,
                                 'canonical_url'=>$canonical_url,


		);
		$this->admin_model->KM_save(array('class'=>'sub_categories','insert'=>$system_array));
		$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		 SubCategory added successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
		redirect('admin/subcategories');
		}
		$data['categories'] = $this->admin_model->km_all(array(
		"class" => 'categories'
		));
		$this->load->view('admin/add_sub_category',$data);
	}
	public function edit_sub_category($sub_cat_id=null)
	{
	if($this->input->post('submit'))
	{
		$category_name = $this->input->post('category_name');
		$sub_cat_id = $this->input->post('sub_cat_id');
		$category_id = $this->input->post('category_id');
		$category_slug= $this->seo_friendly_url($category_name);
                 $sort_order=$this->input->post('sort_order');
                 $status=$this->input->post('status');

		$meta_title=$this->input->post('meta_title');
	    $meta_description=$this->input->post('meta_description');
	    $meta_keywords=$this->input->post('meta_keywords');
	    $canonical_url=$this->input->post('canonical_url');

		$system_array=array('category_id'=>$category_id,
		'sub_cat_name'=>$category_name,
		'sub_cat_slug'=>$category_slug,
		'sub_status'=>$status,
        'sort_order'=>$sort_order,

		'meta_title'=>$meta_title,
		'meta_description'=>$meta_description,
		'meta_keywords'=>$meta_keywords,
		'canonical_url'=>$canonical_url,


		);
	$this->admin_model->KM_update(array("class"=>"sub_categories","update"=>$system_array),array('sub_cat_id'=>$sub_cat_id));
	$msg='<div class = "alert alert-success alert-dismissable">
	<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
	&times;
	</button>
	Sub Category Updated successfully.
	</div>';
	$this->session->set_flashdata('cmsg',$msg);
	redirect('admin/subcategories');
	}
   $data['category_edit']=$this->admin_model->KM_first(array("class"=>"sub_categories","fields"=>array('*'),"conditions"=>array("sub_cat_id"=>$sub_cat_id)));
	$data["sub_cat_id"]=$sub_cat_id;
		$data['categories'] = $this->admin_model->km_all(array(
		"class" => 'categories'
		));

	$this->load->view("admin/edit_sub_category",$data);
	}

	public function delete_sub_category($category_id)
	{
		if(isset($category_id) && $category_id != "")
		{

		$this->admin_model->KM_delete(array("class"=>"sub_categories","conditions"=>array("sub_cat_id"=>$category_id)));
		}
		 $msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Sub Category deleted successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
	   redirect('admin/subcategories');
	}

	public function subsubcategories()
	{
		//$q=$this->db->query('SELECT c.category_name,cc.sub_cat_name,cc.sub_cat_id,c.category_slug FROM `categories` c inner join sub_categories cc on cc.`category_id`=c.`category_id`');
		$q=$this->db->query('SELECT ccc.sort_order,c.category_name,cc.sub_cat_name,ccc.sub_sub_cat_id,ccc.sub_sub_cat_name,ccc.sub_sub_cat_slug FROM `categories` c inner join sub_categories cc on cc.`category_id`=c.`category_id` inner join sub_sub_categories ccc on cc.sub_cat_id=ccc.sub_cat_id');
		$data['categories'] = $q->result_array();
		$this->load->view('admin/sub_sub_cat_list',$data);
	}

		public function add_sub_sub_category()
	{
	   if($this->input->post('submit'))
		{

		$category_name = $this->input->post('category_name');
		$category_id = $this->input->post('category_id');
		$sub_category_id = $this->input->post('sub_cat_id');
		$category_slug= $this->seo_friendly_url($category_name);
               $sort_order=$this->input->post('sort_order');
			   $status = $this->input->post('status');


		$meta_title=$this->input->post('meta_title');
	    $meta_description=$this->input->post('meta_description');
	    $meta_keywords=$this->input->post('meta_keywords');
	    $canonical_url=$this->input->post('canonical_url');

		$system_array=array('category_id'=>$category_id,
		                    'sub_cat_id'=>$sub_category_id,
		                    'sub_sub_cat_name'=>$category_name,
				            'sub_sub_cat_slug'=>$category_slug,
							'sub_sub_status'=>$status,
                            'sort_order'=>$sort_order,
							'meta_title'=>$meta_title,
							'meta_description'=>$meta_description,
							'meta_keywords'=>$meta_keywords,
							'canonical_url'=>$canonical_url,


		);
		$this->admin_model->KM_save(array('class'=>'sub_sub_categories','insert'=>$system_array));
		$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		 SubCategory added successfully.
		</div>';
		$this->session->set_flashdata('ssmsg',$msg);
		redirect('admin/subsubcategories');
		}
		$data['categories'] = $this->admin_model->km_all(array(
		"class" => 'categories'
		));
		$this->load->view('admin/add_sub_sub_category',$data);
	}

	public function edit_sub_sub_category($sub_sub_cat_id=null)
	{
	if($this->input->post('submit'))
	{
		$category_name = $this->input->post('category_name');
		$sub_sub_cat_id = $this->input->post('sub_sub_cat_id');
		$sub_cat_id = $this->input->post('sub_cat_id');
		$category_id = $this->input->post('category_id');
		$status = $this->input->post('status');
		$category_slug= $this->seo_friendly_url($category_name);
                 $sort_order=$this->input->post('sort_order');


		$meta_title=$this->input->post('meta_title');
	    $meta_description=$this->input->post('meta_description');
	    $meta_keywords=$this->input->post('meta_keywords');
	    $canonical_url=$this->input->post('canonical_url');

		$system_array=array('category_id'=>$category_id,
		                    'sub_cat_id'=>$sub_cat_id,
		                    'sub_sub_cat_name'=>$category_name,
				            'sub_sub_cat_slug'=>$category_slug,
				            'sub_sub_status'=>$status,
                            'sort_order'=>$sort_order,
							'meta_title'=>$meta_title,
							'meta_description'=>$meta_description,
							'meta_keywords'=>$meta_keywords,
							'canonical_url'=>$canonical_url,


		);
	$up=$this->admin_model->KM_update(array("class"=>"sub_sub_categories","update"=>$system_array),array('sub_sub_cat_id'=>$sub_sub_cat_id));
	//echo $this->db->last_query();
	if($up == true){
		$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Sub Sub Category Updated successfully.
		</div>';
	}else{
		$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Failed Updated Sub Sub Category.
		</div>';
	}

	$this->session->set_flashdata('ssmsg',$msg);
	redirect('admin/subsubcategories');
	}
   $data['category_edit']=$this->admin_model->KM_first(array("class"=>"sub_sub_categories","fields"=>array('*'),"conditions"=>array("sub_sub_cat_id"=>$sub_sub_cat_id)));
	//echo $this->db->last_query();
	$data["sub_sub_cat_id"]=$sub_sub_cat_id;
		$data['categories'] = $this->admin_model->km_all(array(
		"class" => 'categories'
		));
		$data['sub_categories']=$this->admin_model->KM_all(array("class"=>"sub_categories","fields"=>array('*'),"conditions"=>array("category_id"=>$data['category_edit']['category_id'])));

	$this->load->view("admin/edit_sub_sub_category",$data);
	}

	//delete_sub_sub_category
	public function delete_sub_sub_category($category_id)
	{
		if(isset($category_id) && $category_id != "")
		{

		$this->admin_model->KM_delete(array("class"=>"sub_sub_categories","conditions"=>array("sub_sub_cat_id"=>$category_id)));
		}
		 $msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Sub Category deleted successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
	   redirect('admin/subsubcategories');
	}


	/* 	public function products_list()
	{
	  $data['products'] = $this->admin_model->km_all(array(
            "class" => 'products'
        ));
	 $this->load->view('admin/products_list',$data);
	} */
		public function delete_product($product_id,$cat_id=null)
	{

	if(isset($product_id) && $product_id != "")
	{
	$this->admin_model->KM_delete(array("class"=>"products","conditions"=>array("product_id"=>$product_id)));

	}
	redirect('Admin/products_list/'.$cat_id);

	}
	public function load_sub_categories()
	{
	 $this->layout=false;
	 $category_id=$this->input->post('category_id');
	 $q=$this->db->query("SELECT * FROM `sub_categories` where category_id='$category_id'");
	 $data['cities']=$q->result_array();
	  $this->load->view('admin/load_sub_cat_list',$data);

	}

	public function load_sub_sub_categories()
	{
	 $this->layout=false;
	 $category_id=$this->input->post('category_id');
	 $q=$this->db->query("SELECT * FROM `sub_sub_categories` where sub_cat_id='$category_id'");
	// echo $this->db->last_query();
	 $data['cities1']=$q->result_array();
	  $this->load->view('admin/load_sub_sub_cat_list',$data);

	}

	/* public function add_product()
	{
  	ini_set('post_max_size', '64M');
		ini_set('upload_max_filesize', '64M');
	   if($this->input->post('submit'))
		{
		   ini_set('memory_limit', "256M");
			$date=date('Ymdhis');
			$configVideo['upload_path'] = 'uploads/product_images';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$class_image1=$product_image = $date.$_FILES['product_image']['name'];
			$configVideo['file_name'] = $product_image;

			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('product_image')) {
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();

			$product_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array(
			"source_image"    => $videoDetails1['full_path'],
			"new_image"       => 'uploads/product_images/thumbnails/'.$product_image,
			"maintain_ratio"  => FALSE,

			"width"		   => 700,
			"height"		   => 850	);
			$config['image_library'] = 'gd2';
			// creating thumbnail
			$this->load->library('image_lib');
			// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation

			// $this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}

			}

		$product_name=$this->input->post('product_name');
		$item_code=$this->input->post('item_code');
		$product_desc=$this->input->post('product_desc');
		$category_id=$this->input->post('category_id');
		$sub_cat_id=$this->input->post('sub_cat_id');
		$no_of_items=$this->input->post('no_of_items');
		$product_slug= $this->seo_friendly_url($product_name);
		$product_actual_price=$this->input->post('product_actual_price');
		$product_discount_price	=$this->input->post('product_discount_price');
		$product_in_stock	=$this->input->post('product_in_stock');
		$sort_order	=$this->input->post('sort_order');
		$product_features=$this->input->post('product_features');
		$product_specifications=$this->input->post('product_specifications');
		$system_array=array(
		                   'product_name'=>$product_name,
						   	 'item_code'=>$item_code,
							'product_desc'=>$product_desc,
                             'category_id'=>$category_id,
							  'sub_cat_id'=>$sub_cat_id,
							 'no_of_items'=>$no_of_items,
							'product_slug'=>$product_slug,
							'product_image'=>$product_image,
		                    'product_actual_price'=>$product_actual_price,
							'product_discount_price'=>$product_discount_price,
							'product_in_stock'=>$product_in_stock,
							'sort_order'=>$sort_order,
							 'product_features'=>$product_features,
							 'product_specifications'=>$product_specifications,
                                                          'added_date'=>date('Y-m-d')
		);
		$this->db->insert("products",$system_array);
       $last_id = $this->db->insert_id();
		redirect('admin/products_list/');
	}
		$data['categories'] = $this->admin_model->km_all(array(
		"class" => 'categories'
		));
		$this->load->view('admin/add_product',$data);
	} */


	public function add_product()
	{
  	ini_set('post_max_size', '64M');
		ini_set('upload_max_filesize', '64M');
	   if($this->input->post('submit'))
		{
		   ini_set('memory_limit', "256M");
			$date=date('Ymdhis');
			$configVideo['upload_path'] = 'uploads/product_images';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$class_image1=$product_image = $date.$_FILES['product_image']['name'];
			$configVideo['file_name'] = $product_image;

			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('product_image')) {
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();

			$product_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array(
			"source_image"    => $videoDetails1['full_path'],
			"new_image"       => 'uploads/product_images/thumbnails/'.$product_image,
			"maintain_ratio"  => FALSE,

			"width"		   => 210,
			"height"		   => 210	);
			$config['image_library'] = 'gd2';
			// creating thumbnail
			$this->load->library('image_lib');
			// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation

			// $this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}

			}

		$color=implode(",", $this->input->post('color_id'));

		$product_name=$this->input->post('product_name');
		$item_code=$this->input->post('item_code');
		$product_desc=$this->input->post('product_desc');
		$category_id=$this->input->post('category_id');
		$sub_cat_id=$this->input->post('sub_cat_id');
		$sub_sub_cat_id=$this->input->post('sub_sub_cat_id');
		$no_of_items=$this->input->post('no_of_items');
		$product_slug= $this->seo_friendly_url($product_name);
		$product_actual_price=$this->input->post('product_actual_price');
		$product_discount_price	=$this->input->post('product_discount_price');
		$product_in_stock	=$this->input->post('product_in_stock');
		$sort_order	=$this->input->post('sort_order');
		$rating=$this->input->post('rating');
		$product_features=$this->input->post('product_features');
		$product_specifications=$this->input->post('product_specifications');

		$sale_pack=$this->input->post('sale_pack');
		$in_box_type=$this->input->post('in_box_type');
		$compatable_brand=$this->input->post('compatable_brand');
		$compatable_model=$this->input->post('compatable_model');
		$availability=$this->input->post('availability');
		$fulfillment_ratio=$this->input->post('fulfillment_ratio');
		$covered_in_warranty=$this->input->post('covered_in_warranty');
		$warranty_summary=$this->input->post('warranty_summary');
		$warranty_service_type=$this->input->post('warranty_service_type');
		$warranty_details=$this->input->post('warranty_details');
		$product_status=$this->input->post('product_status');

		$hash_tags=$this->input->post('hash_tags');

		if($sizes!='')
		{
			$sizes=implode(',',$this->input->post('sizes'));
		}else
		{

			$sizes='';
		}
		if($colors!='')
		{
			$colors=implode(',',$this->input->post('colors'));
		}else
		{

			$colors='';
		}
		$system_array=array(
		                   'product_name'=>$product_name,
						   	 'item_code'=>$item_code,
							'product_desc'=>$product_desc,
                             'category_id'=>$category_id,
							  'sub_cat_id'=>$sub_cat_id,
							  'sub_sub_cat_id'=>$sub_sub_cat_id,
							 'no_of_items'=>$no_of_items,
							'product_slug'=>$product_slug,
							'product_image'=>$product_image,
		                    'product_actual_price'=>$product_actual_price,
							'product_discount_price'=>$product_discount_price,
							'product_in_stock'=>$no_of_items,
							'sort_order'=>$sort_order,
							 'product_features'=>$product_features,
							 'product_specifications'=>$product_specifications,
                             'added_date'=>date('Y-m-d'),
							 'sale_pack'=>$sale_pack,
							'in_box_type'=>$in_box_type,
							'compatable_brand'=>$compatable_brand,
							'compatable_model'=>$compatable_model,
							'availability'=>$availability,
							'fulfillment_ratio'=>$fulfillment_ratio,
							'covered_in_warranty'=>$covered_in_warranty,
							'warranty_summary'=>$warranty_summary,
							'warranty_service_type'=>$warranty_service_type,
							'warranty_details'=>$warranty_details,
							'colors'=>$color,
							'hash_tags'=>$hash_tags,
							 'rating'=>$rating,
							 'product_status'=>$product_status

		);
		$this->db->insert("products",$system_array);
       			$last_id = $this->db->insert_id();
			$error = '';
			// for($i=0; $i<count($_FILES); $i++)
			for($i=0; $i< count($_FILES['images']['name']); $i++)
			{
            if($_FILES['images']['size'][$i] > 0)
			{
			$_FILES['userfile']['name']         = $_FILES['images']['name'][$i];
			$_FILES['userfile']['type']         = $_FILES['images']['type'][$i];
			$_FILES['userfile']['tmp_name']     = $_FILES['images']['tmp_name'][$i];
			$_FILES['userfile']['error']    = $_FILES['images']['error'][$i];
			$_FILES['userfile']['size']     = $_FILES['images']['size'][$i];

			$config['file_name']     = time().rand(1000,9999).$i;
			$config['upload_path']   = 'uploads/product_images';
			$config['allowed_types'] = 'jpg|jpeg|gif|png';

			$config['overwrite']     = FALSE;
            $this->load->library('upload', $config);
			$this->upload->initialize($config);

			if($this->upload->do_upload())
			{
			$upload_result = $this->upload->data();

			$image_config = array(
			'source_image'      =>$upload_result['full_path'],
			'new_image'             => 'uploads/product_images/thumbnails/',
			'create_thumb'      => TRUE,
			'maintain_ratio'    => TRUE,
			'width'                     => 210,
			'height'                    => 210
			);

			$this->load->library('image_lib', $image_config);
			$resize_rc = $this->image_lib->resize();
			$error += 0;
			}
			else
			{
			//if the image was not uploaded successfully, try resizing
			$error += 1;
			}
			$images_array=array('product_id'=>$last_id,'image_name'=>$upload_result['file_name']);
			$this->db->insert("product_images",$images_array);
			}
		   }
		redirect('admin/products_list/');
	}
		$data['categories'] = $this->admin_model->km_all(array(
		"class" => 'categories'
		));
		$this->load->view('admin/add_product',$data);
	}
	public function edit_product($product_id=null)
	{		ini_set('post_max_size', '64M');
		ini_set('upload_max_filesize', '64M');
	   if($this->input->post('submit'))
		{
		$product_id=$this->input->post('product_id');
		$product_name=$this->input->post('product_name');
		$item_code=$this->input->post('item_code');
		$product_desc=$this->input->post('product_desc');
		$category_id=$this->input->post('category_id');
		$sub_sub_cat_id=$this->input->post('sub_sub_cat_id');
		$sub_cat_id=$this->input->post('sub_cat_id');
		$no_of_items=$this->input->post('no_of_items');
		$product_slug= $this->seo_friendly_url($product_name);
		$product_actual_price=$this->input->post('product_actual_price');
		$product_discount_price	=$this->input->post('product_discount_price');
		$product_in_stock	=$this->input->post('product_in_stock');
		$sort_order	=$this->input->post('sort_order');
		$rating=$this->input->post('rating');
		$product_features=$this->input->post('product_features');
		$product_specifications=$this->input->post('product_specifications');

		$sale_pack=$this->input->post('sale_pack');
		$in_box_type=$this->input->post('in_box_type');
		$compatable_brand=$this->input->post('compatable_brand');
		$compatable_model=$this->input->post('compatable_model');
		$availability=$this->input->post('availability');
		$fulfillment_ratio=$this->input->post('fulfillment_ratio');
		$covered_in_warranty=$this->input->post('covered_in_warranty');
		$warranty_summary=$this->input->post('warranty_summary');
		$warranty_service_type=$this->input->post('warranty_service_type');
		$warranty_details=$this->input->post('warranty_details');
		$product_status=$this->input->post('product_status');

		$hash_tags=$this->input->post('hash_tags');

		$color=implode(",", $this->input->post('color_id'));
		$system_array=array(
		                   'product_name'=>$product_name,
						   	 'item_code'=>$item_code,
							'product_desc'=>$product_desc,
                             'category_id'=>$category_id,
							  'sub_cat_id'=>$sub_cat_id,
							  'sub_sub_cat_id'=>$sub_sub_cat_id,
							 'no_of_items'=>$no_of_items,
							'product_slug'=>$product_slug,

		                    'product_actual_price'=>$product_actual_price,
							'product_discount_price'=>$product_discount_price,
							'product_in_stock'=>$product_in_stock,
							'sort_order'=>$sort_order,
							 'product_features'=>$product_features,
							 'product_specifications'=>$product_specifications,
							 'sale_pack'=>$sale_pack,
							'in_box_type'=>$in_box_type,
							'compatable_brand'=>$compatable_brand,
							'compatable_model'=>$compatable_model,
							'availability'=>$availability,
							'fulfillment_ratio'=>$fulfillment_ratio,
							'covered_in_warranty'=>$covered_in_warranty,
							'warranty_summary'=>$warranty_summary,
							'warranty_service_type'=>$warranty_service_type,
							'warranty_details'=>$warranty_details,
							'colors'=>$color,
							'hash_tags'=>$hash_tags,
							'product_status'=>$product_status,
							 'rating'=>$rating
		);
		if($_FILES['product_image']['name']!='')
		{

		   ini_set('memory_limit', "256M");
			$date=date('Ymdhis');
			$configVideo['upload_path'] = 'uploads/product_images';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$class_image1=$product_image = $date.$_FILES['product_image']['name'];
			$configVideo['file_name'] = $product_image;

			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('product_image')) {
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();

			$product_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array(
			"source_image"    => $videoDetails1['full_path'],
			"new_image"       => 'uploads/product_images/thumbnails/'.$product_image,
			"maintain_ratio"  => FALSE,
            "width"		   => 210,
			"height"		   => 210	);
			$config['image_library'] = 'gd2';
			// creating thumbnail
			$this->load->library('image_lib');
			// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation

			// $this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}

			}
		$system_array['product_image']=$product_image;


		}
		 $this->admin_model->KM_update(array("class"=>"products","update"=>$system_array),array('product_id'=>$product_id));
							$error = '';
			// for($i=0; $i<count($_FILES); $i++)
			for($i=0; $i< count($_FILES['images']['name']); $i++)
			{
            if($_FILES['images']['size'][$i] > 0)
			{
			$_FILES['userfile']['name']         = $_FILES['images']['name'][$i];
			$_FILES['userfile']['type']         = $_FILES['images']['type'][$i];
			$_FILES['userfile']['tmp_name']     = $_FILES['images']['tmp_name'][$i];
			$_FILES['userfile']['error']    = $_FILES['images']['error'][$i];
			$_FILES['userfile']['size']     = $_FILES['images']['size'][$i];

			$config['file_name']     = time().rand(1000,9999).$i;
			$config['upload_path']   = 'uploads/product_images';
			$config['allowed_types'] = 'jpg|jpeg|gif|png';

			$config['overwrite']     = FALSE;
            $this->load->library('upload', $config);
			$this->upload->initialize($config);

			if($this->upload->do_upload())
			{
			$upload_result = $this->upload->data();

			$image_config = array(
			'source_image'      =>$upload_result['full_path'],
			'new_image'             => 'uploads/product_images/thumbnails/',
			'create_thumb'      => TRUE,
			'maintain_ratio'    => TRUE,
			'width'                     => 210,
			'height'                    => 210
			);

			$this->load->library('image_lib', $image_config);
			$resize_rc = $this->image_lib->resize();
			$error += 0;
			}
			else
			{
			//if the image was not uploaded successfully, try resizing
			$error += 1;
			}
			$images_array=array('product_id'=>$product_id,'image_name'=>$upload_result['file_name']);
			$this->db->insert("product_images",$images_array);
			}
		   }
		redirect('admin/products_list/');
	}



	   $data['categories'] = $this->admin_model->km_all(array(
		"class" => 'categories'
		));
		$data['edit_product']=$this->admin_model->KM_first(array("class"=>"products","fields"=>array('*'),"conditions"=>array("product_id"=>$product_id)));
	    $data["product_id"]=$product_id;
	    $data['sub_categories']=$this->admin_model->KM_all(array("class"=>"sub_categories","fields"=>array('*'),"conditions"=>array("category_id"=>$data['edit_product']['category_id'])));
	    $data['sub_sub_categories']=$this->admin_model->KM_all(array("class"=>"sub_sub_categories","fields"=>array('*'),"conditions"=>array("sub_sub_cat_id"=>$data['edit_product']['sub_sub_cat_id'])));
		$q=$this->db->query("SELECT * FROM `product_images` where product_id='$product_id'");
		$data['product_images']=$q->result_array();
		$this->load->view('admin/edit_product',$data);
	}

function delete_image()
	{
	$image_id = $this->input->post('image_id');
	 $this->admin_model->KM_delete(array(
                "class" => "product_images",
                "conditions" => array(
                    "image_id" => $image_id
                )
            ));
	exit;
	}
	public function subscribers_list()
	{
	  $data['subscribers'] = $this->admin_model->km_all(array(
            "class" => 'subscribers'
        ));
	 $this->load->view('admin/subscribers_list',$data);
	}
	public function users_list()
	{
	  $data['users'] = $this->admin_model->km_all(array(
            "class" => 'users'
        ));
	 $this->load->view('admin/users_list',$data);
	}
	public function orders_list()
	{
	  /* $data['orders'] = $this->admin_model->km_all(array(
            "class" => 'orders'
        )); */

		$q=$this->db->query("select * from orders  order by order_id desc");
		$data['orders']=$q->result_array();
	 $this->load->view('admin/orders_list',$data);
	}

	public function pending_orders_list()
	{
	  /* $data['orders'] = $this->admin_model->km_all(array(
            "class" => 'orders'
        )); */

		$q=$this->db->query("select * from orders where order_status='pending' order by order_id desc");
		$data['orders']=$q->result_array();
	 $this->load->view('admin/orders_list',$data);
	}

	public function completed_orders_list()
	{
	  /* $data['orders'] = $this->admin_model->km_all(array(
            "class" => 'orders'
        )); */

		$q=$this->db->query("select * from orders where order_status='completed'  order by order_id desc");
		$data['orders']=$q->result_array();
	 $this->load->view('admin/orders_list',$data);
	}


		public function view_products($order_id)
	{
		if(isset($order_id) && $order_id!= "")
		{

		$data['orders'] =$this->admin_model->km_all(array("class"=>"order_products","conditions"=>array("order_id"=>$order_id)));
		}
		 $this->load->view('admin/view_products',$data);
	}
		public function delete_user($user_id)
	{
		if(isset($user_id) && $user_id != "")
		{

		$this->admin_model->KM_delete(array("class"=>"users","conditions"=>array("user_id"=>$user_id)));
		}
		 $msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		User deleted successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
	   redirect('admin/users_list');
	}





	public function faq_list($course_id='')
	{
	  $data['faq_list'] = $this->admin_model->faq_list($course_id);
	  $data['course_id']=$course_id;
	   $data['courses'] = $this->admin_model->km_all(array(
            "class" => 'courses'
        ));
	 $this->load->view('admin/faq_list',$data);
	}
    	public function add_faq()
	{
	   if($this->input->post('submit'))
		{

		$course_id = $this->input->post('course_id');
		$question = $_POST['question'];
		$answer = $_POST['answer'];
		for($i=0;$i< count($question);$i++)
		{
			$system_array=array('course_id'=>$course_id,
			'question'=>$question[$i],
			'answer'=>$answer[$i]
			);
			$this->admin_model->KM_save(array('class'=>'faq_list','insert'=>$system_array));
		}
		$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		 Faq added successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
		redirect('admin/faq_list');
		}
         $data['courses'] = $this->admin_model->km_all(array(
            "class" => 'courses'
        ));

		$this->load->view('admin/add_faq',$data);
	}
		public function delete_faq($faq_id)
	{
		if(isset($faq_id) && $faq_id != "")
		{

		$this->admin_model->KM_delete(array("class"=>"faq_list","conditions"=>array("faq_id"=>$faq_id)));
		}
		 $msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		FAQ deleted successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
	   redirect('admin/faq_list');
	}
	public function edit_faq($faq_id,$course_id='')
	{
	if($this->input->post('submit'))
	{
		$faq_id=$this->input->post('faq_id');
		$question = $_POST['question'];
		$answer = $_POST['answer'];
		$system_array=array('question'=>$question,
		'answer'=>$answer
		);
		$this->admin_model->KM_update(array("class"=>"faq_list","update"=>$system_array),array('faq_id'=>$faq_id));
		$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		FAQ Updated successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
		redirect('admin/faq_list');
	}
	$data['faq_list']=$this->admin_model->KM_first(array("class"=>"faq_list","fields"=>array('*'),"conditions"=>array("faq_id"=>$faq_id)));
	$data["course_id"]=$course_id;
	$data["faq_id"]=$faq_id;
	$this->load->view("admin/edit_faq",$data);

	}

    public function banners_list()
    {
	  //  $this->check_session();
        $data['all_banners'] = $this->admin_model->KM_all(array(
            "class" => "banners"
        ));
        $this->load->view('admin/banners_list', $data);
    }
    public function delete_banner($banner_id)
    {
        if (isset($banner_id) && $banner_id != "") {
            $this->admin_model->KM_delete(array(
                "class" => "banners",
                "conditions" => array(
                    "banner_id" => $banner_id
                )
            ));
        }
		$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Banner Deleted successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
        redirect(base_url().'admin/banners_list');
    }
    public function add_banner()
    {
	 //  $this->check_session();
        if ($this->input->post('submit')) {


			 $sort_order = $this->input->post('sort_order');
        	$date=date('Ymdhis');
			$configVideo['upload_path'] = 'uploads/banners';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$class_image1=$banner_image = $date.$_FILES['banner_image']['name'];
			$configVideo['file_name'] = $banner_image;
			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('banner_image'))
			{
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();
			$banner_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array("source_image"    => $videoDetails1['full_path'],
			"new_image" => 'uploads/banners/thumbnails/'.$banner_image,
			"maintain_ratio"  => FALSE,
			"width"	 =>  100,
			"height"  =>  100	);
			$config['image_library'] = 'gd2';
			$this->load->library('image_lib');		// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation
			$this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}
			}


            $banners_array = array(


                'banner_image' => $banner_image,
				'sort_order' => $sort_order



            );
            $this->admin_model->KM_save(array(
                'class' => 'banners',
                'insert' => $banners_array
            ));
				$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Banner Added successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
            redirect(base_url().'admin/banners_list');
        }

        $this->load->view('admin/add_banner');
    }

	    public function edit_banner($banner_id='')
    {

        if ($this->input->post('submit')) {
			 $banner_id   = $this->input->post('banner_id');


			 $sort_order = $this->input->post('sort_order');

           $banners_array = array(


				'sort_order' => $sort_order

            );
			$date=date('Ymdhis');
			if($_FILES['banner_image']['name']!='')
			{
			$configVideo['upload_path'] = 'uploads/banners';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$class_image1=$banner_image = $date.$_FILES['banner_image']['name'];
			$configVideo['file_name'] = $banner_image;
			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('banner_image'))
			{
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();
			$banner_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array("source_image"    => $videoDetails1['full_path'],
			"new_image" => 'uploads/banners/thumbnails/'.$banner_image,
			"maintain_ratio"  => FALSE,
			"width"	 => 100,
			"height"  =>100	);
			$config['image_library'] = 'gd2';
			$this->load->library('image_lib');		// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation
			$this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}
            $banners_array['banner_image']= $banner_image;
			}
			}else
			{



			}

            $this->admin_model->KM_update(array(
                "class" => "banners",
                "update" => $banners_array
            ), array(
                'banner_id' => $banner_id
            ));
			$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Banner Updated successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
           redirect(base_url().'admin/banners_list');
        }
       $data['edit_banner']=$this->admin_model->KM_first(array("class"=>"banners","fields"=>array('*'),"conditions"=>array("banner_id"=>$banner_id)));

        $this->load->view('admin/edit_banner', $data);
    }

    public function gallery_list()
    {
	  //  $this->check_session();
        $data['all_banners'] = $this->admin_model->KM_all(array(
            "class" => "gallery"
        ));
        $this->load->view('admin/gallery_list', $data);
    }
    public function delete_gallery($gallery_id)
    {
        if (isset($gallery_id) && $gallery_id != "") {
            $this->admin_model->KM_delete(array(
                "class" => "gallery",
                "conditions" => array(
                    "gallery_id" => $gallery_id
                )
            ));
        }
		$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Image Deleted successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
        redirect(base_url().'admin/gallery_list');
    }
    public function add_gallery()
    {
	 //  $this->check_session();
        if ($this->input->post('submit')) {


        	$date=date('Ymdhis');
			$configVideo['upload_path'] = 'uploads/banners';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$class_image1=$banner_image = $date.$_FILES['banner_image']['name'];
			$configVideo['file_name'] = $banner_image;
			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('banner_image'))
			{
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();
			$banner_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array("source_image"    => $videoDetails1['full_path'],
			"new_image" => 'uploads/banners/thumbnails/'.$banner_image,
			"maintain_ratio"  => FALSE,
			"width"	 =>  100,
			"height"  =>  100	);
			$config['image_library'] = 'gd2';
			$this->load->library('image_lib');		// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation
			$this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}
			}


            $banners_array = array(


                'gallery_image' => $banner_image,
				'gallery_title'=> $this->input->post('gallery_title'),
				'sort_order'=> $this->input->post('sort_order')



            );
            $this->admin_model->KM_save(array(
                'class' => 'gallery',
                'insert' => $banners_array
            ));
				$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Banner Added successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
            redirect(base_url().'admin/gallery_list');
        }

        $this->load->view('admin/add_gallery');
    }

	    public function edit_gallery($gallery_id='')
    {

        if ($this->input->post('submit')) {
			 $gallery_id   = $this->input->post('gallery_id');




           $banners_array = array(

               'gallery_title'=> $this->input->post('gallery_title'),
				'sort_order'=> $this->input->post('sort_order')

            );
			$date=date('Ymdhis');
			if($_FILES['banner_image']['name']!='')
			{
			$configVideo['upload_path'] = 'uploads/banners';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$class_image1=$banner_image = $date.$_FILES['banner_image']['name'];
			$configVideo['file_name'] = $banner_image;
			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('banner_image'))
			{
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();
			$banner_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array("source_image"    => $videoDetails1['full_path'],
			"new_image" => 'uploads/banners/thumbnails/'.$banner_image,
			"maintain_ratio"  => FALSE,
			"width"	 => 100,
			"height"  => 100	);
			$config['image_library'] = 'gd2';
			$this->load->library('image_lib');		// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation
			$this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}
            $banners_array['gallery_image']= $banner_image;
			}
			}else
			{



			}

            $this->admin_model->KM_update(array(
                "class" => "gallery",
                "update" => $banners_array
            ), array(
                'gallery_id' => $gallery_id
            ));
			$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Image Updated successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
           redirect(base_url().'admin/gallery_list');
        }
       $data['edit_banner']=$this->admin_model->KM_first(array("class"=>"gallery","fields"=>array('*'),"conditions"=>array("gallery_id"=>$gallery_id)));

        $this->load->view('admin/edit_gallery', $data);
    }
	 public function delete_subscriber($subscriber_id)
    {
        if (isset($subscriber_id) && $subscriber_id != "") {
            $this->admin_model->KM_delete(array(
                "class" => "subscribers",
                "conditions" => array(
                    "subscriber_id" => $subscriber_id
                )
            ));
        }
        redirect(base_url().'admin/subscribers_list');
    }
	   public function widgets_list()
    {
	  //  $this->check_session();
        $data['widgets'] = $this->admin_model->KM_all(array(
            "class" => "widgets"
        ));
        $this->load->view('admin/widgets_list', $data);
    }
    public function delete_widget($widget_id)
    {
        if (isset($widget_id) && $widget_id != "") {
            $this->admin_model->KM_delete(array(
                "class" => "widgets",
                "conditions" => array(
                    "widget_image_id" => $widget_id
                )
            ));
        }
        redirect(base_url().'admin/widgets_list');
    }
	 public function add_widget_banner()
    {
	 //  $this->check_session();
        if ($this->input->post('submit')) {

            $banner_title = $this->input->post('banner_title');
        	if($_POST['widget_id']=='Widget-1')
			{
			$width='570';
			$height='140';
			}else
			if($_POST['widget_id']=='Widget-2')
			{
			$width='280';
			$height='145';

			}else
			if($_POST['widget_id']=='Widget-3')
			{
			$width='280';
			$height='145';

			}else
			{
			$width='270';
			$height='290';

			}

			$configVideo['upload_path'] = 'banners';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$class_image1=$banner_image = $date.$_FILES['banner_image']['name'];
			$configVideo['file_name'] = $banner_image;
			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('banner_image'))
			{
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();
			$banner_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array("source_image"    => $videoDetails1['full_path'],
			"new_image" => 'banners/thumbnails/'.$banner_image,
			"maintain_ratio"  => FALSE,
			"width"	 => $width,
			"height"  => $height	);
			$config['image_library'] = 'gd2';
			$this->load->library('image_lib');		// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation
			$this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}
			}
			$widget_id = $this->input->post('widget_id');
            $banners_array = array(
                'widget_name' => $widget_id,
                'image_title' => $banner_title,
                'image' => $banner_image


            );
            $this->admin_model->KM_save(array(
                'class' => 'widgets',
                'insert' => $banners_array
            ));
            redirect(base_url().'admin/widgets_list');
        }

        $this->load->view('admin/add_widget');
    }
	//Srikanth 8/8/2018-start
  public function home()
	{
    $this->check_login();
	$admin_id=$this->session->userdata('admin_id');
	$data['admin_data']=$this->admin_model->KM_first(array("class"=>"admins","fields"=>array('*'),"conditions"=>array("admin_id"=>$admin_id)));

	 $this->load->view('admin/home',$data);
	}
  public function add_events()
	{
    	$this->check_login();
  $query_event_types = $this->db->query("select * from  event_type");
  $event_types = $query_event_types->result_array();
	$data['event_types']=$event_types;
	 $this->load->view('admin/add_events',$data);
	}
  public function view_events()
	{
    $this->check_login();
    $query = $this->db->query("select * from  event_details where status = 1");
    $event_details_list = $query->result_array();
    if(empty($event_details_list)){
      $data['list'] = "No events to display.";
      $data['status'] = false;
    }else{
      $data['list'] = $event_details_list;
      $data['status'] = true;
    }
    $this->load->view('admin/view_events',$data);

	}
  public function view_event_types(){
    $this->check_login();
    $query = $this->db->query("select * from event_type where status = 1");
    $event_type_details_list = $query->result_array();
    if(empty($event_type_details_list)){
      $data['list'] = "No event types to display.";
      $data['status'] = false;
    }else{
      $data['list'] = $event_type_details_list;
      $data['status'] = true;
    }
    $this->load->view('admin/view_event_types',$data);
  }

  public function add_journals()
	{
    $this->check_login();
	$admin_id=$this->session->userdata('admin_id');
	$data['admin_data']=$this->admin_model->KM_first(array("class"=>"admins","fields"=>array('*'),"conditions"=>array("admin_id"=>$admin_id)));

	 $this->load->view('admin/add_journals',$data);
	}

  public function view_journals()
	{
    $this->check_login();
    $query = $this->db->query("select * from  journals");
    $event_details_list = $query->result_array();
    // echo"<pre>";print_r($event_details_list);die();
    if(empty($event_details_list)){
      $data['list'] = "No events to display.";
      $data['status'] = false;
    }else{
      $data['list'] = $event_details_list;
      $data['status'] = true;
    }
	 $this->load->view('admin/view_journals',$data);
	}

  public function view_articles()
  {
    $this->check_login();
    $query = $this->db->query("select * from  articles");
    $event_details_list = $query->result_array();
    // echo"line number 1929 admin controller";
    // echo"<pre>";print_r($event_details_list);die();
    if(empty($event_details_list)){
      $data['list'] = "No events to display.";
      $data['status'] = false;
    }else{
      $data['list'] = $event_details_list;
      $data['status'] = true;
    }
   $this->load->view('admin/view_articles',$data);
  }

  public function view_boards()
  {
    $this->check_login();
    // $query = $this->db->query("select * from  board");
    $query = $this->db->query("SELECT journals.journal_title, board.* from board INNER JOIN journals ON journals.journal_id = board.journal_id ORDER BY board.id DESC ");
    $event_details_list = $query->result_array();
    // echo"line number 1945 admin controller";
    // echo"<pre>";print_r($event_details_list);die();
    if(empty($event_details_list)){
      $data['list'] = "No events to display.";
      $data['status'] = false;
    }else{
      $data['list'] = $event_details_list;
      $data['status'] = true;
    }
   $this->load->view('admin/view_boards',$data);
  }



  public function add_article()
  {
    $this->check_login();
    $query_journals = $this->db->query("select * from  journals where status = 1");
    $data['journals'] = $query_journals->result_array();
    // echo'<pre>';print_r($data['unique_article_id']);die();
    $admin_id=$this->session->userdata('admin_id');
    $data['admin_data']=$this->admin_model->KM_first(array("class"=>"admins","fields"=>array('*'),"conditions"=>array("admin_id"=>$admin_id)));
    $this->load->view('admin/add_article',$data);
  }

  public function add_board()
  {
    $this->check_login();
    $query_journals = $this->db->query("select * from  journals where status = 1");
    $data['journals'] = $query_journals->result_array();
    // $this->check_login();
    $admin_id=$this->session->userdata('admin_id');
    $data['admin_data']=$this->admin_model->KM_first(array("class"=>"admins","fields"=>array('*'),"conditions"=>array("admin_id"=>$admin_id)));
    $this->load->view('admin/add_board',$data);
  }

  public function edit_journals_view($jid = NULL){
    $this->check_login();
    $query = $this->db->query("select * from  journals where journal_id='$jid'");
    $event_details['journal_details'] = $query->result_array();
    // echo"<pre>";print_r($event_details);die();
    // $event_details['event_types']=$event_types;
  	 $this->load->view('admin/edit_journals_view',$event_details);
  }

  public function edit_journal_logic (){
    $this->check_login();
    $data=$this->input->post();
    $gallery_images_array = array();
    $status = 0;
    if(isset($data['status'])){
      if($data['status'] == 'on'){
        $status = 1;
      }else{
        $status = 0;
      }
    }
    if(!empty($_FILES['journal_image']['name'])){
      $img_date = date('Ymdhis');
      $upload_path = 'uploads/journals';
      $tmp_img = $_FILES["journal_image"]['tmp_name'];
      $image_name = $img_date.'_'.$_FILES['journal_image']['name'];
      move_uploaded_file($tmp_img, "$upload_path/$image_name");
    }
    $update_data = array(
        'journal_title'=>strtoupper($data['journal_title']),
        'about_journal'=>trim($data['about_journal']),
        'special_issues' => $data['special_issues'],
        'aim_and_scope' => trim($data['aim_and_scope']),
        'for_author' => trim($data['for_author']),
        'publication_charges' => trim($data['publication_charges']),
        'status' => $status
    );

      if(!empty($image_name)){
    $update_data['journal_image'] = $image_name;
    }


    $titles = $data['image_title'];

    if(!empty($_FILES['image']['name'])){
      $i = 0;
      foreach($_FILES["image"]['name'] as $tmp_name){
        $img_date = date('Ymdhis');
        $upload_path = 'uploads/indexes';
        $tmp_img = $_FILES["image"]['tmp_name'][$i];
        $index_image_name = $img_date.'_'.$tmp_name;
        if(move_uploaded_file($tmp_img, "$upload_path/$index_image_name")){
            $gallery_images_array[$i] = $index_image_name;
        }
        $i++;
      }
    }

    $update_insert_index = array();
    $id = $data['journal_id'];
    $del = $this->db->where('journal_id', $id);
    $del = $this->db->delete('indexes');
    for($i=0; $i < count($gallery_images_array); $i++) {
      $update_insert_index = array(
        'journal_id'=> $id,
        'image'=> $gallery_images_array[$i],
        'title'=> $titles[$i]
      );
      $add_index=$this->db->insert('indexes',$update_insert_index);
      // $add_index=$this->db->update('indexes',$update_insert_index);
    }

      $q=$this->db->where('journal_id', $id);
      $q=$this->db->update('journals', $update_data);

      $query = $this->db->query("select * from  journals where journal_id='$id'");
      $event_details['journal_details'] = $query->result_array();
    	 $this->load->view('admin/edit_journals_view',$event_details);

  }

  public function edit_articles_view($article_id = NULL){
    $this->check_login();
    $query = $this->db->query("select * from  articles where article_id='$article_id'");
    $data['article_details'] = $query->result_array();
    //echo"<pre>";print_r($data['article_details']);die();
    $query_journals = $this->db->query("select * from  journals where status = 1");
    $data['journals'] = $query_journals->result_array();
    $this->load->view('admin/edit_articles_view',$data);
  }

  public function edit_article_logic(){
    $this->check_login();
      $data=$this->input->post();
      if(!empty($_FILES['fileToUpload']['name'])){
        $img_date = date('Ymdhis');
        $upload_path = 'articles';
        $tmp_img = $_FILES["fileToUpload"]['tmp_name'];
        $pdf_name = $img_date.'_'.$_FILES['fileToUpload']['name'];
        move_uploaded_file($tmp_img, "$upload_path/$pdf_name");
      }
//echo"<pre>";print_r($data);die();
      $jid = $data['select_journal'];
      $update_data = array(
          'journal_id'=>$data['select_journal'],
          'article_title'=>$data['title'],
          'author' => $data['author'],
          'co_author'=>$data['co_author'],
          'unique_article_id'=>$data['unique_article_id'],
          'article_type'=>$data['article_type'],
          'category'=>$data['category'],
          'volume'=>$data['volume'],
          'issue'=>$data['issue'],
          'full_text'=>trim($data['article_fulltext'])
          // 'status'=>$status
      );
    //   echo"<pre >";print_r($update_data);die();
        if(!empty($pdf_name)){
      $update_data = array(
          'pdf'=>$pdf_name
        );
      }

      $id = $data['article_id'];
      $q=$this->db->where('article_id', $id);
      $q=$this->db->update('articles', $update_data);
      // echo $this->db->last_query();exit;
      $query = $this->db->query("select * from  articles where article_id='$id'");
      $view_data['article_details'] = $query->result_array();
      $query_journals = $this->db->query("select * from  journals where status = 1");
      $view_data['journals'] = $query_journals->result_array();
      $this->load->view('admin/edit_articles_view',$view_data);
  }

  public function edit_boards_view($board_id = NULL){
    $this->check_login();
    $query = $this->db->query("select * from  board where id='$board_id'");
    $data['board_details'] = $query->result_array();
    $query_journals = $this->db->query("select * from  journals where status = 1");
    $data['journals'] = $query_journals->result_array();
    // echo"<pre>";print_r($data);die();
    $this->load->view('admin/edit_boards_view',$data);
  }

  public function edit_board_logic(){
    $this->check_login();
    $data=$this->input->post();
    if(!empty($_FILES['service_image']['name'])){
      $img_date = date('Ymdhis');
      $upload_path = 'uploads/board';
      $tmp_img = $_FILES["service_image"]['tmp_name'];
      $image_name = $img_date.'_'.$_FILES['service_image']['name'];
      move_uploaded_file($tmp_img, "$upload_path/$image_name");
    }

    $update_data = array(
        'journal_id	'=>$data['select_journal'],
        'editor_reviewer'=>$data['editor_reviewer'],
        'name'=>$data['name'],
        'title' => $data['title'],
        'rank' => $data['rank'],
        'affliation' => $data['affliation'],
        'university' => $data['university'],
        'country' => $data['country'],
        'bio' => trim($data['bio']),
        'research_intrest' => $data['research_intrest']
    );

    if(!empty($image_name)){
    $update_data = array(
        'image' => $image_name,
      );
  }

    $id = $data['board_id'];
    $q=$this->db->where('id', $id);
    $q=$this->db->update('board', $update_data);
    $query = $this->db->query("select * from  board where id='$id'");
    $data['board_details'] = $query->result_array();
    $query_journals = $this->db->query("select * from  journals where status = 1");
    $data['journals'] = $query_journals->result_array();
    $this->load->view('admin/edit_boards_view',$data);
  }

  public function delete_journal($jid = NULL){
    $query = $this->db->query("update journals set status='0' where journal_id='$jid'");
    $this->check_login();
    redirect(base_url().'admin/view_journals');
  }

  public function add_journal(){
		$data=$this->input->post();
    // echo"<pre>";print_r($data);die();
    $gallery_images_array = array();
    $status = 0;
    if(isset($data['status'])){
      if($data['status'] == 'on'){
        $status = 1;
      }else{
        $status = 0;
      }
    }
    if(!empty($_FILES['journal_image']['name'])){
      $img_date = date('Ymdhis');
      $upload_path = 'uploads/journals';
      $tmp_img = $_FILES["journal_image"]['tmp_name'];
      $image_name = $img_date.'_'.$_FILES['journal_image']['name'];
      move_uploaded_file($tmp_img, "$upload_path/$image_name");
    }
    $insert_data = array(
        'journal_title'=>strtoupper($data['journal_title']),
        'about_journal'=>trim($data['about_journal']),
        'special_issues' => $data['special_issues'],
        'aim_and_scope' => trim($data['aim_and_scope']),
        'for_author' => trim($data['for_author']),
        'publication_charges' => trim($data['publication_charges']),
        'journal_image' => $image_name,
        'status' => $status
    );
    $add_journal=$this->db->insert('journals',$insert_data);
    $insert_id = $this->db->insert_id();
    $images = $data['image'];
    $titles = $data['image_title'];

    if(!empty($_FILES['image']['name'])){
      $i = 0;
      foreach($_FILES["image"]['name'] as $tmp_name){
        $img_date = date('Ymdhis');
        $upload_path = 'uploads/indexes';
        $tmp_img = $_FILES["image"]['tmp_name'][$i];
        $image_name = $img_date.'_'.$tmp_name;
        if(move_uploaded_file($tmp_img, "$upload_path/$image_name")){
            $gallery_images_array[$i] = $image_name;
        }
        $i++;
      }
    }

    for($i=0; $i < count($gallery_images_array); $i++) {
      $insert_index = array(
        'journal_id'=> $insert_id,
        'image'=> $gallery_images_array[$i],
        'title'=> $titles[$i]
      );
      // print_r($insert_index);
      $add_index=$this->db->insert('indexes',$insert_index);
    }
    $this->check_login();
		if($add_journal && $add_index){
    redirect(base_url().'admin/add_journals?is_success=1');
  }else{
      redirect(base_url().'admin/add_journals?is_success=0');
    }
	}

  public function add_article_logic(){
		$data=$this->input->post();

    if(!empty($_FILES['fileToUpload']['name'])){
      $img_date = date('Ymdhis');
      $upload_path = 'articles';
      $tmp_img = $_FILES["fileToUpload"]['tmp_name'];
      $pdf_name = $img_date.'_'.$_FILES['fileToUpload']['name'];
      move_uploaded_file($tmp_img, "$upload_path/$pdf_name");
    }

    $jid = $data['select_journal'];
    // $unique_article_id = $this->db->query("Select unique_article_id from articles where journal_id='$jid' ORDER BY article_id DESC limit 1");
    $find_journal = $this->db->query("Select * from articles where journal_id='$jid'");
    $find_journal = $find_journal->row_array();
    if($find_journal){
      $article_journal = $this->db->query("SELECT journals.journal_title, articles.unique_article_id from journals INNER JOIN articles ON journals.journal_id = articles.journal_id where journals.journal_id='$jid' ORDER BY articles.article_id DESC limit 1");
      $article_journal = $article_journal->row_array();
      $journal_title= $article_journal['journal_title'];
    }else{
      $journal_title_q = $this->db->query("Select journal_title from journals where journal_id='$jid'");
      $journal_title_r = $journal_title_q->row_array();
      $journal_title= $journal_title_r['journal_title'];
    }

    if(!empty($article_journal['unique_article_id'])){
      $code= explode("-",$article_journal['unique_article_id']);
      $data['code'] = $code[2]+1;
    }else{
      $data['code'] = 101;
    }

    // echo"<pre>";print_r($jid);die();
    $words = explode(" ", trim($journal_title));
    $acronym = "";
    foreach ($words as $w) {
      $acronym .= $w[0];
    }
    $unique_article_id = $acronym.'-'.$data['volume'].'-'.$data['code'];
    // print_r($unique_article_id);die();
    if(isset($data['status'])){
      if($data['status'] == 'on'){
        $status = 1;
      }else{
        $status = 0;
      }
    }
    $insert_data = array(
        'journal_id'=>$data['select_journal'],
        'article_title'=>$data['title'],
        'author' => $data['author'],
        'co_author'=>$data['co_author'],
        'unique_article_id'=>$data['article_id'],
        'article_type'=>$data['article_type'],
        'category'=>$data['category'],
        'volume'=>$data['volume'],
        'issue'=>$data['issue'],
        'pdf'=>$pdf_name,
        'full_text'=>trim($data['article_fulltext'])
        // 'status'=>$status
    );
    $add_article=$this->db->insert('articles',$insert_data);
    // $insert_id = $this->db->insert_id();
    $this->check_login();
		if($add_article){
    redirect(base_url().'admin/add_article?is_success=1');
  }else{
      redirect(base_url().'admin/add_article?is_success=0');
    }
	}

  public function add_board_logic(){
		$data=$this->input->post();
    if(!empty($_FILES['service_image']['name'])){
      $img_date = date('Ymdhis');
      $upload_path = 'uploads/board';
      $tmp_img = $_FILES["service_image"]['tmp_name'];
      $image_name = $img_date.'_'.$_FILES['service_image']['name'];
      move_uploaded_file($tmp_img, "$upload_path/$image_name");
    }
    // print_r($image_name);die();
    $insert_data = array(
        'journal_id	'=>$data['select_journal'],
        'editor_reviewer'=>$data['editor_reviewer'],
        'name'=>$data['name'],
        'title' => $data['title'],
        'rank' => $data['rank'],
        'affliation' => $data['affliation'],
        'university' => $data['university'],
        'country' => $data['country'],
        'image' => $image_name,
        'bio' => trim($data['bio']),
        'research_intrest' => $data['research_intrest']
    );
    // echo"<pre>";print_r($insert_data);die();
    $add_board=$this->db->insert('board',$insert_data);
    $this->check_login();
    if($add_board){
    redirect(base_url().'admin/add_board?is_success=1');
  }else{
      redirect(base_url().'admin/add_board?is_success=0');
    }
	}

public function upload_images($image_name,$path){
  ini_set('post_max_size', '64M');
  ini_set('upload_max_filesize', '64M');
  $date=date('Ymdhis');
  $configVideo['upload_path'] = 'uploads/'.$path;
  $configVideo['allowed_types'] = '*';
  $configVideo['overwrite'] = FALSE;
  $configVideo['remove_spaces'] = TRUE;
  $class_image1=$banner_image = $date.$image_name;
  $configVideo['file_name'] = $banner_image;
  $this->load->library('upload', $configVideo);
  $this->upload->initialize($configVideo);

  if (!$this->upload->do_upload($image))
  {
  echo $this->upload->display_errors();
  } else {
  $videoDetails1 = $this->upload->data();
  $banner_image = $tumbnail=$videoDetails1['file_name'];
  $helthy['thumbnail'] = $tumbnail;
  $config = array("source_image"    => $videoDetails1['full_path'],
  "new_image" => 'uploads/thumbnails/'.$banner_image,
  "maintain_ratio"  => FALSE,
  "width"	 =>  100,
  "height"  =>  100	);
  $config['image_library'] = 'gd2';
  $this->load->library('image_lib');		// Set your config up
  $this->image_lib->initialize($config);
  // Do your manipulation
  $this->image_lib->resize();
  if ( ! $this->image_lib->resize())
  {
  echo $this->image_lib->display_errors();
  }
  return $banner_image;
  }
}
  public function update_events(){
		$data = $this->input->post();
    $banner_image = '';
    $circle_image_boy = '';
    $circle_image_girl = '';
    $gallery_images_array = array();
    if(!empty($_FILES['banner_image']['name'])){
      $banner_image = $this->upload_images($_FILES['banner_image']['name'], 1);
    }
    if(!empty($_FILES['circle_image_boy']['name'])){
      $circle_image_boy = $this->upload_images($_FILES['circle_image_boy']['name'], 2);
    }
    if(!empty($_FILES['circle_image_girl']['name'])){
      $circle_image_girl = $this->upload_images($_FILES['circle_image_girl']['name'], 3);
    }

    if(!empty($_FILES['files']['name'])){
      $i = 0;
      foreach($_FILES["files"]['name'] as $tmp_name){
        $img_date = date('Ymdhis');
        $upload_path = 'uploads/banners';
        $tmp_img = $_FILES["files"]['tmp_name'][$i];
        $image_name = $img_date.'_'.$tmp_name;
        if(move_uploaded_file($tmp_img, "$upload_path/$image_name")){
            $gallery_images_array[$i] = $image_name;
        }
         // move_uploaded_file($tmp_img, "$upload_path/$image_name");
        // $gallery_images_array[] = $this->upload_images($tmp_name, 4);
        $i++;
      }
    }
      // echo "<pre>";print_r(json_encode($gallery_images_array));die();
    if($data['select_event'] == 1){
      $link = trim($data['name1']).'weds'.trim($data['name2']);
    }else{
      $link = $data['name1'];
    }

    $insert_data = array(
        'link'=>trim($link),
        'event_title'=>$data['event_title'],
        'event_type_id'=>(int)$data['select_event'],
        'event_date'=>$data['datepicker'].' '.trim($data['time_element']),
        'venue_details'=>$data['venue_details'],
        'summary'=>$data['event_summary'],
        'name1'=>$data['name1'],
        'name2'=>$data['name2'],
        'live_streaming_link'=>$data['embedded_link'],
        'banner_image' => $banner_image,
        'circle_img_boy' => $circle_image_boy,
        'circle_img_girl' => $circle_image_girl,
        'gallery_img' => json_encode($gallery_images_array),
        'boy_text' => $data['boy_text'],
        'girl_text' => $data['girl_text'],
    );
    // echo"<pre>"; print_r($insert_data);die();
		$q=$this->db->insert('event_details', $insert_data);
		if($q){
			$msg='<div class = "alert alert-success alert-dismissable">
			<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
			&times;
			</button>
			 Events added successfully.
			</div>';
			$this->session->set_flashdata('fmsg',$msg);
		}
    $this->check_login();
		redirect('admin/add_events');
	}

  public function update_edited_events(){

    $data = $this->input->post();

    $banner_image = '';
    $circle_image_boy = '';
    $circle_image_girl = '';
    $gallery_images_array = array();
    $data1 = array();

    if(!empty($_FILES['files']['name'])){
      $i = 0;
      foreach($_FILES["files"]['name'] as $tmp_name){
        $img_date = date('Ymdhis');
        $upload_path = 'uploads/banners';
        $tmp_img = $_FILES["files"]['tmp_name'][$i];
        $image_name = $img_date.'_'.$tmp_name;
        if(move_uploaded_file($tmp_img, "$upload_path/$image_name")){
            $gallery_images_array[$i] = $image_name;
        }
         // move_uploaded_file($tmp_img, "$upload_path/$image_name");
        // $gallery_images_array[] = $this->upload_images($tmp_name, 4);
        $i++;
      }
    }

    if($data['select_event'] == 1){
      $link = trim($data['name1']).'weds'.trim($data['name2']);
    }else{
      $link = $data['name1'];
    }
        // $event_title = $data['event_title'];
        // $event_type_id = (int)$data['select_event'];
        // $event_date = $data['datepicker'].' '.trim($data['time_element']);
        // $venue_details = $data['venue_details'];
        // $summary = $data['event_summary'];
        // $name1 = $data['name1'];
        // $name2 = $data['name2'];
        // $live_streaming_link = $data['embedded_link'];
        // $id = $data['id'];
        // $gallery_img = json_encode($gallery_images_array);
        // $boy_text = $data['boy_text'];
        // $girl_text = $data['girl_text'];

    $data1 = array(
        'link'=>trim($link),
        'event_title'=>$data['event_title'],
        'event_type_id'=>(int)$data['select_event'],
        'event_date'=>$data['datepicker'].' '.trim($data['time_element']),
        'venue_details'=>$data['venue_details'],
        'summary'=>$data['event_summary'],
        'name1'=>$data['name1'],
        'name2'=>$data['name2'],
        'live_streaming_link'=>$data['embedded_link'],
        'gallery_img' => json_encode($gallery_images_array),
        'boy_text' => $data['boy_text'],
        'girl_text' => $data['girl_text'],
    );
    if(!empty($_FILES['banner_image']['name'])){
      $data1['banner_image'] = $this->upload_images($_FILES['banner_image']['name'], 1);
    }
    if(!empty($_FILES['circle_image_boy']['name'])){
      $data1['circle_img_boy'] = $this->upload_images($_FILES['circle_image_boy']['name'], 2);
    }
    if(!empty($_FILES['circle_image_girl']['name'])){
      $data1['circle_img_girl'] = $this->upload_images($_FILES['circle_image_girl']['name'], 3);
    }

    $id = $data['id'];
		$q=$this->db->where('id', $id);
    $q=$this->db->update('event_details', $data1);
    // $q=$this->db->query("update event_details set link='$link',event_title='$event_title',event_type_id='$event_type_id',event_date='$event_date',venue_details='$venue_details',summary='$summary',name1='$name1',name2='$name2',live_streaming_link='$live_streaming_link',banner_image='$banner_image',circle_img_boy='$circle_image_boy', circle_img_girl='$circle_image_girl',gallery_img='$gallery_img',boy_text='$boy_text', girl_text='$girl_text' where id='$id'");
     // echo $this->db->last_query();exit;
    if($q){
			$msg='<div class = "alert alert-success alert-dismissable">
			<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
			&times;
			</button>
			 Event Updated successfully.
			</div>';
			$this->session->set_flashdata('fmsg',$msg);
      $this->check_login();
      redirect('admin/view_events');
		}
  }

  public function deactivateEvent(){
    // $this->check_login();
    $id = $_GET['id'];
    $status = 0;
    $q=$this->db->query("update event_details set status='$status' where id='$id'");
    // print_r($q);die();
    if($q){
			$msg='<div class = "alert alert-success alert-dismissable">
			<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
			&times;
			</button>
			 Event Deactivated successfully.
			</div>';
			$this->session->set_flashdata('fmsg',$msg);
      $this->check_login();
      redirect('admin/view_events');
		}
  }

  public function deactivateEventType(){
    $id = $_GET['id'];
    $status = 0;
    $q=$this->db->query("update event_type set status='$status' where id='$id'");
    // print_r($q);die();
    if($q){
      $msg='<div class = "alert alert-success alert-dismissable">
      <button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
      &times;
      </button>
       Event Type Deactivated successfully.
      </div>';
      $this->session->set_flashdata('fmsg',$msg);
      $this->check_login();
      redirect('admin/view_event_types');
    }
  }

  //Srikanth 8/8/2018-end
	public function edit_profile()
	{
    $this->check_login();
	$admin_id=$this->session->userdata('admin_id');
	$data['admin_data']=$this->admin_model->KM_first(array("class"=>"admins","fields"=>array('*'),"conditions"=>array("admin_id"=>$admin_id)));

	 $this->load->view('admin/edit_profile',$data);
	}
	function contact_messages()
	{
	   $data['contact_messages'] = $this->admin_model->KM_all(array(
            "class" => "contact_messages"
        ));
        $this->load->view('admin/contact_messages', $data);

	}
	 public function delete_contact($message_id)
    {
        if (isset($message_id) && $message_id != "") {
            $this->admin_model->KM_delete(array(
                "class" => "contact_messages",
                "conditions" => array(
                    "message_id" => $message_id
                )
            ));
        }
        redirect(base_url().'admin/contact_messages');
    }
		public function report_stock()
	{
	  $data['products'] = $this->admin_model->km_all(array(
            "class" => 'products'
        ));
	 $this->load->view('admin/report_stock',$data);
	}
	public function view_product($product_id)
	{
		$q=$this->db->query("SELECT p.*,c.category_name FROM `products` p inner join categories c on c.`category_id`=p.`category_id` where product_id='$product_id'");
		$data['view_product'] = $q->row_array();
		$this->load->view('admin/view_product',$data);
	}

	function color()
	{
	   $data['color'] = $this->admin_model->KM_all(array(
            "class" => "color"
        ));
        $this->load->view('admin/color_list', $data);

	}
	function add_color(){
		if($this->input->post('submit')){
			$color_name = ucfirst($this->input->post('color_name'));
			$sort_order=$this->input->post('sort_order');
			$system_array=array('color_name'=>$color_name,
						    'sort_order'=>$sort_order
			);
			$this->admin_model->KM_save(array('class'=>'color','insert'=>$system_array));
			$msg='<div class = "alert alert-success alert-dismissable">
			<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
			&times;
			</button>
			 Color added successfully.
			</div>';
			$this->session->set_flashdata('cmsg',$msg);
			redirect('admin/color');

		}
		$this->load->view('admin/add_color');
	}
	public function edit_color($color_id=null)
	{
	if($this->input->post('submit'))
	{
		$color_name = ucfirst($this->input->post('color_name'));
		$color_id=$this->input->post('color_id');
		$sort_order=$this->input->post('sort_order');

		$system_array=array('color_name'=>$color_name,
							'sort_order'=>$sort_order


		);
	$this->admin_model->KM_update(array("class"=>"color","update"=>$system_array),array('color_id'=>$color_id));
	$msg='<div class = "alert alert-success alert-dismissable">
	<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
	&times;
	</button>
	Color Updated successfully.
	</div>';
	$this->session->set_flashdata('cmsg',$msg);
	redirect('admin/color');
	}
   $data['color_edit']=$this->admin_model->KM_first(array("class"=>"color","fields"=>array('*'),"conditions"=>array("color_id"=>$color_id)));
	$data["color_id"]=$color_id;


	$this->load->view("admin/edit_color",$data);
	}

	public function delete_color($category_id)
	{
		if(isset($category_id) && $category_id != "")
		{

		$this->admin_model->KM_delete(array("class"=>"color","conditions"=>array("color_id"=>$category_id)));
		}
		 $msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		COlor deleted successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
	   redirect('admin/color');
	}


	function brand()
	{
	   $data['brand'] = $this->admin_model->KM_all(array(
            "class" => "brand"
        ));
        $this->load->view('admin/brands_list', $data);

	}
	function add_brand(){
		if($this->input->post('submit')){
			$brand_name = ucfirst($this->input->post('brand_name'));
			$system_array=array('brand_name'=>$brand_name
			);
			$this->admin_model->KM_save(array('class'=>'brand','insert'=>$system_array));
			$msg='<div class = "alert alert-success alert-dismissable">
			<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
			&times;
			</button>
			 Brand added successfully.
			</div>';
			$this->session->set_flashdata('cmsg',$msg);
			redirect('admin/brand');

		}
		$this->load->view('admin/add_brand');
	}
	public function edit_brand($brand_id=null)
	{
	if($this->input->post('submit'))
	{
		$brand_name = ucfirst($this->input->post('brand_name'));
		$brand_id=$this->input->post('brand_id');

		$system_array=array('brand_name'=>$brand_name
		);

	$this->admin_model->KM_update(array("class"=>"brand","update"=>$system_array),array('brand_id'=>$brand_id));
	$msg='<div class = "alert alert-success alert-dismissable">
	<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
	&times;
	</button>
	Brand Updated successfully.
	</div>';
	$this->session->set_flashdata('cmsg',$msg);
	redirect('admin/brand');
	}
   $data['color_edit']=$this->admin_model->KM_first(array("class"=>"brand","fields"=>array('*'),"conditions"=>array("brand_id"=>$brand_id)));
	$data["brand_id"]=$brand_id;


	$this->load->view("admin/edit_brand",$data);
	}

	public function delete_brand($category_id)
	{
		if(isset($category_id) && $category_id != "")
		{

		$this->admin_model->KM_delete(array("class"=>"brand","conditions"=>array("brand_id"=>$category_id)));
		}
		 $msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Brand deleted successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
	   redirect('admin/brand');
	}


	public function model()
	{
		$q=$this->db->query('SELECT c.brand_name,cc.model_name,cc.model_id FROM `brand` c inner join model cc on cc.`brand_id`=c.`brand_id`');
		$data['model'] = $q->result_array();
		$this->load->view('admin/model_list',$data);
	}
    	public function add_model()
	{
	   if($this->input->post('submit'))
		{

		$model_name = $this->input->post('model_name');
		$brand_id = $this->input->post('brand_id');

		$system_array=array('brand_id'=>$brand_id,
		                    'model_name'=>$model_name


		);
		$this->admin_model->KM_save(array('class'=>'model','insert'=>$system_array));
		$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		 Model added successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
		redirect('admin/model');
		}
		$data['brand'] = $this->admin_model->km_all(array(
		"class" => 'brand'
		));
		$this->load->view('admin/add_model',$data);
	}
	public function edit_model($model_id=null)
	{
	if($this->input->post('submit'))
	{
		$model_name = $this->input->post('model_name');
		$brand_id = $this->input->post('brand_id');
		$model_id = $this->input->post('model_id');

		$system_array=array('brand_id'=>$brand_id,
		'model_name'=>$model_name


		);
	$this->admin_model->KM_update(array("class"=>"model","update"=>$system_array),array('model_id'=>$model_id));
	$msg='<div class = "alert alert-success alert-dismissable">
	<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
	&times;
	</button>
	Model Updated successfully.
	</div>';
	$this->session->set_flashdata('cmsg',$msg);
	redirect('admin/model');
	}
   $data['category_edit']=$this->admin_model->KM_first(array("class"=>"model","fields"=>array('*'),"conditions"=>array("model_id"=>$model_id)));
	$data["model_id"]=$model_id;
		$data['brand'] = $this->admin_model->km_all(array(
		"class" => 'brand'
		));

	$this->load->view("admin/edit_model",$data);
	}

	public function delete_model($model_id)
	{
		if(isset($model_id) && $model_id != "")
		{

		$this->admin_model->KM_delete(array("class"=>"model","conditions"=>array("model_id"=>$model_id)));
		}
		 $msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Model deleted successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
	   redirect('admin/model');
	}

	function pincode()
	{
	   $data['pincode'] = $this->admin_model->KM_all(array(
            "class" => "pincode"
        ));
        $this->load->view('admin/pincode_list', $data);

	}
	function add_pincode(){
		if($this->input->post('submit')){
			$pin_code = ucfirst($this->input->post('pincode_name'));
			$system_array=array('pin_code'=>$pin_code
			);
			$this->admin_model->KM_save(array('class'=>'pincode','insert'=>$system_array));
			$msg='<div class = "alert alert-success alert-dismissable">
			<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
			&times;
			</button>
			 Pincode added successfully.
			</div>';
			$this->session->set_flashdata('cmsg',$msg);
			redirect('admin/pincode');

		}
		$this->load->view('admin/add_pincode');
	}
	public function edit_pincode($pin_id=null)
	{
	if($this->input->post('submit'))
	{
		$pin_code = ucfirst($this->input->post('pincode_name'));
		$pin_id=$this->input->post('pin_id');

		$system_array=array('pin_code'=>$pin_code
		);

	$this->admin_model->KM_update(array("class"=>"pincode","update"=>$system_array),array('pin_id'=>$pin_id));
	$msg='<div class = "alert alert-success alert-dismissable">
	<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
	&times;
	</button>
	Pincode Updated successfully.
	</div>';
	$this->session->set_flashdata('cmsg',$msg);
	redirect('admin/pincode');
	}
   $data['pin_edit']=$this->admin_model->KM_first(array("class"=>"pincode","fields"=>array('*'),"conditions"=>array("pin_id"=>$pin_id)));
	$data["pin_id"]=$pin_id;


	$this->load->view("admin/edit_pincode",$data);
	}

	public function delete_pincode($pin_id)
	{
		if(isset($pin_id) && $pin_id != "")
		{

		$this->admin_model->KM_delete(array("class"=>"pincode","conditions"=>array("pin_id"=>$pin_id)));
		}
		 $msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Pincode deleted successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
	   redirect('admin/pincode');
	}
   function change_sub_category()
   {
	$sub_cat_id=$this->input->post('sub_cat_id');
	$sort_order=$this->input->post('sort_order');
		$system_array=array('sort_order'=>$sort_order
		);
	$this->admin_model->KM_update(array("class"=>"sub_categories","update"=>$system_array),array('sub_cat_id'=>$sub_cat_id));
   }


   public function products_list()
	{
		ini_set('memory_limit', '8192M');
		$category=$this->uri->segment(2);

		$this->layout=false;
		$agent_no=$this->session->userdata('agent_no');
		$sql="SELECT * FROM products p ";


		$q=$this->db->query($sql);
		$data['count_posts']=$data['posts']=$q->result_array();

		$this->load->library('pagination');
		$query = $this->db->query($sql);
		$total_count = count($query->result_array());
		$config['base_url'] = base_url('admin/products_list/');

		$config['total_rows'] = $total_count;
		$config['use_page_numbers'] = TRUE;
		$config['per_page'] = 25;

		$config["uri_segment"] = 3;

		$choice = $config["total_rows"] / $config["per_page"];

		$config["num_links"] = 5;

		//config for bootstrap pagination class integration

		$config['full_tag_open'] = '<ul class="pagination">';

		$config['full_tag_close'] = '</ul>';

		$config['first_link'] = false;

		$config['last_link'] = false;

		$config['first_tag_open'] = '<li>';

		$config['first_tag_close'] = '</li>';

		$config['prev_link'] = '&laquo';

		$config['prev_tag_open'] = '<li class="prev">';

		$config['prev_tag_close'] = '</li>';

		$config['next_link'] = '&raquo';

		$config['next_tag_open'] = '<li>';

		$config['next_tag_close'] = '</li>';

		$config['last_tag_open'] = '<li>';

		$config['last_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="#">';

		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';

		$config['num_tag_close'] = '</li>';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
		$this->pagination->initialize($config);

		$data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

		//call the model function to get the department data

		$page=$data['page'];

		$per_page=$config['per_page'];
		if($page==0)
		$p=0;
		else
		$p=($page-1)*$config['per_page'];
		$this->db->query("set character_set_results='utf8'");
		$sql="SELECT * from  products p";

        $sql.= " order by p.product_id desc limit $p,$per_page";
		$query = $this->db->query($sql);
		$data['products'] = $query->result_array();
		$data['pagination'] = $this->pagination->create_links();

		 $this->load->view('admin/products_list',$data);
		//$this->load->view('admin/posts_list',$data);
	}
	public function search_products()
	{
		$search_key=$this->input->post('search_key');
		$sql="SELECT * FROM products p where product_name like '%$search_key%' limit 1000";
		    //echo $this->db->last_query();exit;

		$q=$this->db->query($sql);
		$data['products']=$q->result_array();
		 $this->load->view('admin/search_products',$data);

	}
	public function order_view($order_id)
	{

	$data['view_product']=$this->admin_model->KM_first(array("class"=>"orders","fields"=>array('*'),"conditions"=>array("order_id"=>$order_id)));
	$q=$this->db->query("SELECT * FROM `order_products` c inner join products cc on c.`id`=cc.`product_id` left join color ccc on c.`p_color`=ccc.`color_id` where c.order_id='$order_id'");
		$data['order_products'] = $q->result_array();
	//$data['order_products']=$this->admin_model->KM_all(array("class"=>"order_products","fields"=>array('*'),"conditions"=>array("order_id"=>$order_id)));
	$this->load->view("admin/view_order",$data);

	}


	public function save_pdf($order_id)
	{
		$this->load->library('m_pdf');

		$data['view_product']=$this->admin_model->KM_first(array("class"=>"orders","fields"=>array('*'),"conditions"=>array("order_id"=>$order_id)));
		$q=$this->db->query("SELECT * FROM `order_products` c inner join products cc on c.`id`=cc.`product_id` left join color ccc on c.`p_color`=ccc.`color_id` where c.order_id='$order_id'");
		$data['order_products'] = $q->result_array();

		$html=$this->load->view("admin/pdf_view",$data, true);

		//$this->load->view("admin/view_order",$data);
		$pdfFilePath =$order_id."-invoice-download.pdf";

		$pdf = $this->m_pdf->load();

		//generate the PDF!
		$pdf->WriteHTML($html,2);
		//offer it to user via browser download! (The PDF won't be saved on your server HDD)
		$pdf->Output($pdfFilePath, "D");
		$pdf->Output('uploads/invoices/'.$pdfFilePath, "F");
		$q=$this->db->query("update orders set invoice='$pdfFilePath' where order_id=$order_id");
	}

	public function update_profile(){
		$admin_id=$this->session->userdata('admin_id');

		$name=$this->input->post('admin_name');
		$last_name=$this->input->post('last_name');
		$q=$this->db->query("update admins set admin_name='$name',last_name='$last_name' where admin_id='$admin_id'");
		if($q){
			$msg='<div class = "alert alert-success alert-dismissable">
			<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
			&times;
			</button>
			 Profile Updated successfully.
			</div>';
			$this->session->set_flashdata('fmsg',$msg);
		}
		redirect('admin/edit_profile');
	}

	public function update_password()
	{

	   $admin_id=$this->session->userdata('admin_id');
	   if($this->input->post('submit'))
		{
		$q=$this->db->query("select admin_password,admin_real_password from admins where admin_id='$admin_id'");
		//echo $this->db->last_query();exit;
		$res=$q->row_array();

		$old_password=$res['admin_password'];

		$password1 = $this->input->post('password1');
		$password2=$this->input->post('password2');
		if($old_password == md5($this->input->post('password'))){

			if($password1==$password2)
			{
				//echo $old_password;exit;
			$system_array=array('admin_password'=>md5($password1),
								'admin_real_password'=>$password1


			);

			 $q=$this->admin_model->KM_update(array("class"=>"admins","update"=>$system_array),array('admin_id'=>$admin_id));

				if($q){
				$msg='<div class = "alert alert-success alert-dismissable">
				<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
				&times;
				</button>
				 Password Updated successfully.
				</div>';
				$this->session->set_flashdata('fmsg',$msg);
				}
			}else
				{
				$msg='<div class = "alert alert-danger alert-dismissable">
				<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
				&times;
				</button>
				 Password Not Match
				</div>';
				$this->session->set_flashdata('fmsg',$msg);
				}
			}else{
				$msg='<div class = "alert alert-danger alert-dismissable">
				<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
				&times;
				</button>
				 Incorrect Old Password
				</div>';
				$this->session->set_flashdata('fmsg',$msg);
			}

			redirect('admin/update_password');
		}

		//$admin_id=$this->session->userdata('admin_id');
		$data['admin_data']=$this->admin_model->KM_first(array("class"=>"admins","fields"=>array('*'),"conditions"=>array("admin_id"=>$admin_id)));

		$this->load->view('admin/update_password',$data);
	}


	    public function innovation_works_list()
    {
	  //  $this->check_session();
        $data['all_banners'] = $this->admin_model->KM_all(array(
            "class" => "innovation_works"
        ));
        $this->load->view('admin/innovation_works_list', $data);
    }
    public function delete_innovation_works($gallery_id)
    {
        if (isset($gallery_id) && $gallery_id != "") {
            $this->admin_model->KM_delete(array(
                "class" => "innovation_works",
                "conditions" => array(
                    "gallery_id" => $gallery_id
                )
            ));
        }
		$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Image Deleted successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
        redirect(base_url().'admin/innovation_works_list');
    }
    public function add_innovation_works()
    {
	 //  $this->check_session();
        if ($this->input->post('submit')) {

            $banner_title = $this->input->post('banner_title');
        	$date=date('Ymdhis');
			$configVideo['upload_path'] = 'uploads/banners';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$class_image1=$banner_image = $date.$_FILES['banner_image']['name'];
			$configVideo['file_name'] = $banner_image;
			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('banner_image'))
			{
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();
			$banner_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array("source_image"    => $videoDetails1['full_path'],
			"new_image" => 'uploads/banners/thumbnails/'.$banner_image,
			"maintain_ratio"  => FALSE,
			"width"	 =>  230,
			"height"  =>  140	);
			$config['image_library'] = 'gd2';
			$this->load->library('image_lib');		// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation
			$this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}
			}


            $banners_array = array(

                'gallery_title' => $banner_title,
                'gallery_image' => $banner_image,
				'sort_order'=> $this->input->post('sort_order')


            );
            $this->admin_model->KM_save(array(
                'class' => 'innovation_works',
                'insert' => $banners_array
            ));
				$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Image Added successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
            redirect(base_url().'admin/innovation_works_list');
        }

        $this->load->view('admin/add_innovation_works');
    }

	    public function edit_innovation_works($gallery_id='')
    {

        if ($this->input->post('submit')) {
			 $gallery_id   = $this->input->post('gallery_id');

            $banner_title = $this->input->post('banner_title');


           $banners_array = array(

                'gallery_title' => $banner_title,
				'sort_order'=> $this->input->post('sort_order')


            );
			$date=date('Ymdhis');
			if($_FILES['banner_image']['name']!='')
			{
			$configVideo['upload_path'] = 'uploads/banners';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$class_image1=$banner_image = $date.$_FILES['banner_image']['name'];
			$configVideo['file_name'] = $banner_image;
			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('banner_image'))
			{
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();
			$banner_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array("source_image"    => $videoDetails1['full_path'],
			"new_image" => 'uploads/banners/thumbnails/'.$banner_image,
			"maintain_ratio"  => FALSE,
			"width"	 => 230,
			"height"  => 140	);
			$config['image_library'] = 'gd2';
			$this->load->library('image_lib');		// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation
			$this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}
            $banners_array['gallery_image']= $banner_image;
			}
			}else
			{



			}

            $this->admin_model->KM_update(array(
                "class" => "innovation_works",
                "update" => $banners_array
            ), array(
                'gallery_id' => $gallery_id
            ));
			$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Image Updated successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
           redirect(base_url().'admin/innovation_works_list');
        }
       $data['edit_banner']=$this->admin_model->KM_first(array("class"=>"innovation_works","fields"=>array('*'),"conditions"=>array("gallery_id"=>$gallery_id)));

        $this->load->view('admin/edit_innovation_works', $data);
    }


		    public function sponcers_list()
    {
	  //  $this->check_session();
        $data['all_banners'] = $this->admin_model->KM_all(array(
            "class" => "sponcers"
        ));
        $this->load->view('admin/sponcers_list', $data);
    }
    public function delete_sponcers($gallery_id)
    {
        if (isset($gallery_id) && $gallery_id != "") {
            $this->admin_model->KM_delete(array(
                "class" => "sponcers",
                "conditions" => array(
                    "gallery_id" => $gallery_id
                )
            ));
        }
		$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Image Deleted successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
        redirect(base_url().'admin/sponcers_list');
    }
    public function add_sponcers()
    {
	 //  $this->check_session();
        if ($this->input->post('submit')) {

            $banner_title = $this->input->post('banner_title');
        	$date=date('Ymdhis');
			$configVideo['upload_path'] = 'uploads/banners';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$class_image1=$banner_image = $date.$_FILES['banner_image']['name'];
			$configVideo['file_name'] = $banner_image;
			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('banner_image'))
			{
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();
			$banner_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array("source_image"    => $videoDetails1['full_path'],
			"new_image" => 'uploads/banners/thumbnails/'.$banner_image,
			"maintain_ratio"  => FALSE,
			"width"	 =>  230,
			"height"  =>  140	);
			$config['image_library'] = 'gd2';
			$this->load->library('image_lib');		// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation
			$this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}
			}


            $banners_array = array(

                'gallery_title' => $banner_title,
                'gallery_image' => $banner_image,
				'sort_order'=> $this->input->post('sort_order')


            );
            $this->admin_model->KM_save(array(
                'class' => 'sponcers',
                'insert' => $banners_array
            ));
				$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Image Added successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
            redirect(base_url().'admin/sponcers_list');
        }

        $this->load->view('admin/add_sponcers');
    }

	    public function edit_sponcers($gallery_id='')
    {

        if ($this->input->post('submit')) {
			 $gallery_id   = $this->input->post('gallery_id');

            $banner_title = $this->input->post('banner_title');


           $banners_array = array(

                'gallery_title' => $banner_title,
				'sort_order'=> $this->input->post('sort_order')


            );
			$date=date('Ymdhis');
			if($_FILES['banner_image']['name']!='')
			{
			$configVideo['upload_path'] = 'uploads/banners';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$class_image1=$banner_image = $date.$_FILES['banner_image']['name'];
			$configVideo['file_name'] = $banner_image;
			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('banner_image'))
			{
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();
			$banner_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array("source_image"    => $videoDetails1['full_path'],
			"new_image" => 'uploads/banners/thumbnails/'.$banner_image,
			"maintain_ratio"  => FALSE,
			"width"	 => 230,
			"height"  => 140	);
			$config['image_library'] = 'gd2';
			$this->load->library('image_lib');		// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation
			$this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}
            $banners_array['gallery_image']= $banner_image;
			}
			}else
			{



			}

            $this->admin_model->KM_update(array(
                "class" => "sponcers",
                "update" => $banners_array
            ), array(
                'gallery_id' => $gallery_id
            ));
			$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Image Updated successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
           redirect(base_url().'admin/sponcers_list');
        }
       $data['edit_banner']=$this->admin_model->KM_first(array("class"=>"sponcers","fields"=>array('*'),"conditions"=>array("gallery_id"=>$gallery_id)));

        $this->load->view('admin/edit_sponcers', $data);
    }


			    public function appreciations_list()
    {
	  //  $this->check_session();
        $data['all_banners'] = $this->admin_model->KM_all(array(
            "class" => "appreciations"
        ));
        $this->load->view('admin/appreciations_list', $data);
    }
    public function delete_appreciations($gallery_id)
    {
        if (isset($gallery_id) && $gallery_id != "") {
            $this->admin_model->KM_delete(array(
                "class" => "appreciations",
                "conditions" => array(
                    "gallery_id" => $gallery_id
                )
            ));
        }
		$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Image Deleted successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
        redirect(base_url().'admin/appreciations_list');
    }
    public function add_appreciations()
    {
	 //  $this->check_session();
        if ($this->input->post('submit')) {

            $banner_title = $this->input->post('banner_title');
        	$date=date('Ymdhis');
			$configVideo['upload_path'] = 'uploads/banners';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$class_image1=$banner_image = $date.$_FILES['banner_image']['name'];
			$configVideo['file_name'] = $banner_image;
			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('banner_image'))
			{
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();
			$banner_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array("source_image"    => $videoDetails1['full_path'],
			"new_image" => 'uploads/banners/thumbnails/'.$banner_image,
			"maintain_ratio"  => FALSE,
			"width"	 =>  230,
			"height"  =>  140	);
			$config['image_library'] = 'gd2';
			$this->load->library('image_lib');		// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation
			$this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}
			}


            $banners_array = array(

                'gallery_title' => $banner_title,
                'gallery_image' => $banner_image,
				'sort_order'=> $this->input->post('sort_order')


            );
            $this->admin_model->KM_save(array(
                'class' => 'appreciations',
                'insert' => $banners_array
            ));
				$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Image Added successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
            redirect(base_url().'admin/appreciations_list');
        }

        $this->load->view('admin/add_appreciations');
    }

	    public function edit_appreciations($gallery_id='')
    {

        if ($this->input->post('submit')) {
			 $gallery_id   = $this->input->post('gallery_id');

            $banner_title = $this->input->post('banner_title');


           $banners_array = array(

                'gallery_title' => $banner_title,
				'sort_order'=> $this->input->post('sort_order')


            );
			$date=date('Ymdhis');
			if($_FILES['banner_image']['name']!='')
			{
			$configVideo['upload_path'] = 'uploads/banners';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$class_image1=$banner_image = $date.$_FILES['banner_image']['name'];
			$configVideo['file_name'] = $banner_image;
			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('banner_image'))
			{
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();
			$banner_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array("source_image"    => $videoDetails1['full_path'],
			"new_image" => 'uploads/banners/thumbnails/'.$banner_image,
			"maintain_ratio"  => FALSE,
			"width"	 => 230,
			"height"  => 140	);
			$config['image_library'] = 'gd2';
			$this->load->library('image_lib');		// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation
			$this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}
            $banners_array['gallery_image']= $banner_image;
			}
			}else
			{



			}

            $this->admin_model->KM_update(array(
                "class" => "appreciations",
                "update" => $banners_array
            ), array(
                'gallery_id' => $gallery_id
            ));
			$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Image Updated successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
           redirect(base_url().'admin/appreciations_list');
        }
       $data['edit_banner']=$this->admin_model->KM_first(array("class"=>"appreciations","fields"=>array('*'),"conditions"=>array("gallery_id"=>$gallery_id)));

        $this->load->view('admin/edit_appreciations', $data);
    }

				    public function clients_list()
    {
	  //  $this->check_session();
        $data['all_banners'] = $this->admin_model->KM_all(array(
            "class" => "clients"
        ));
        $this->load->view('admin/clients_list', $data);
    }
    public function delete_clients($gallery_id)
    {
        if (isset($gallery_id) && $gallery_id != "") {
            $this->admin_model->KM_delete(array(
                "class" => "clients",
                "conditions" => array(
                    "gallery_id" => $gallery_id
                )
            ));
        }
		$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Image Deleted successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
        redirect(base_url().'admin/clients_list');
    }
    public function add_clients()
    {
	 //  $this->check_session();
        if ($this->input->post('submit')) {



		if(isset($_FILES['files'])){
		$errors= array();
		foreach($_FILES['files']['tmp_name'] as $key => $tmp_name ){
		$file_name = $key.$_FILES['files']['name'][$key];
		$file_size =$_FILES['files']['size'][$key];
		$file_tmp =$_FILES['files']['tmp_name'][$key];
		$file_type=$_FILES['files']['type'][$key];

		$banners_array = array(
		'gallery_image' => $file_name
		);
		$this->admin_model->KM_save(array(
		'class' => 'clients',
		'insert' => $banners_array
		));
		move_uploaded_file($file_tmp,"uploads/banners/".$file_name);

		}

		}

				$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Image Added successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
            redirect(base_url().'admin/clients_list');
        }

        $this->load->view('admin/add_clients');
    }

	    public function edit_clients($gallery_id='')
    {

        if ($this->input->post('submit')) {
			 $gallery_id   = $this->input->post('gallery_id');


			$date=date('Ymdhis');
			if($_FILES['banner_image']['name']!='')
			{
			$configVideo['upload_path'] = 'uploads/banners';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$class_image1=$banner_image = $date.$_FILES['banner_image']['name'];
			$configVideo['file_name'] = $banner_image;
			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('banner_image'))
			{
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();
			$banner_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array("source_image"    => $videoDetails1['full_path'],
			"new_image" => 'uploads/banners/thumbnails/'.$banner_image,
			"maintain_ratio"  => FALSE,
			"width"	 => 230,
			"height"  => 140	);
			$config['image_library'] = 'gd2';
			$this->load->library('image_lib');		// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation
			$this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}
            $banners_array['gallery_image']= $banner_image;
			}
			}else
			{



			}

            $this->admin_model->KM_update(array(
                "class" => "clients",
                "update" => $banners_array
            ), array(
                'gallery_id' => $gallery_id
            ));
			$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Image Updated successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
           redirect(base_url().'admin/clients_list');
        }
       $data['edit_banner']=$this->admin_model->KM_first(array("class"=>"clients","fields"=>array('*'),"conditions"=>array("gallery_id"=>$gallery_id)));

        $this->load->view('admin/edit_clients', $data);
    }


		    public function sister_compannies_list()
    {
	  //  $this->check_session();
        $data['all_banners'] = $this->admin_model->KM_all(array(
            "class" => "sister_compannies"
        ));
        $this->load->view('admin/sister_compannies_list', $data);
    }
    public function delete_sister_compannies($gallery_id)
    {
        if (isset($gallery_id) && $gallery_id != "") {
            $this->admin_model->KM_delete(array(
                "class" => "sister_compannies",
                "conditions" => array(
                    "cmp_id" => $gallery_id
                )
            ));
        }
		$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		 Product Deleted successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
        redirect(base_url().'admin/sister_compannies_list');
    }
    public function add_siter_company()
    {
	 //  $this->check_session();
        if ($this->input->post('submit')) {

            $banner_title = $this->input->post('banner_title');
        	$date=date('Ymdhis');
			$configVideo['upload_path'] = 'uploads/banners';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$class_image1=$banner_image = $date.$_FILES['banner_image']['name'];
			$configVideo['file_name'] = $banner_image;
			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('banner_image'))
			{
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();
			$banner_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array("source_image"    => $videoDetails1['full_path'],
			"new_image" => 'uploads/banners/thumbnails/'.$banner_image,
			"maintain_ratio"  => FALSE,
			"width"	 =>  230,
			"height"  =>  140	);
			$config['image_library'] = 'gd2';
			$this->load->library('image_lib');		// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation
			$this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}
			}

	       $slug= $this->seo_friendly_url($banner_title);
            $banners_array = array(

                'gallery_title' => $banner_title,

                'gallery_image' => $banner_image,
				'sort_order'=> $this->input->post('sort_order'),
				'slug'=>$slug,
				'email'=> $this->input->post('email'),
				'phone'=> $this->input->post('phone'),
				'business_office'=> $this->input->post('business_office'),
				'reg_office'=> $this->input->post('reg_office'),


            );
            $this->admin_model->KM_save(array(
                'class' => 'sister_compannies',
                'insert' => $banners_array
            ));
				$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		 Product Added successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
            redirect(base_url().'admin/sister_compannies_list');
        }

        $this->load->view('admin/add_siter_company');
    }

	    public function edit_sister_compannies($gallery_id='')
    {

        if ($this->input->post('submit')) {
			 $gallery_id   = $this->input->post('gallery_id');

            $banner_title = $this->input->post('banner_title');


           $banners_array = array(

                'gallery_title' => $banner_title,

				'sort_order'=> $this->input->post('sort_order'),
					'email'=> $this->input->post('email'),
				'phone'=> $this->input->post('phone'),
				'business_office'=> $this->input->post('business_office'),
				'reg_office'=> $this->input->post('reg_office'),

            );
			$date=date('Ymdhis');
			if($_FILES['banner_image']['name']!='')
			{
			$configVideo['upload_path'] = 'uploads/banners';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$class_image1=$banner_image = $date.$_FILES['banner_image']['name'];
			$configVideo['file_name'] = $banner_image;
			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('banner_image'))
			{
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();
			$banner_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array("source_image"    => $videoDetails1['full_path'],
			"new_image" => 'uploads/banners/thumbnails/'.$banner_image,
			"maintain_ratio"  => FALSE,
			"width"	 => 230,
			"height"  => 140	);
			$config['image_library'] = 'gd2';
			$this->load->library('image_lib');		// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation
			$this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}
            $banners_array['gallery_image']= $banner_image;
			}
			}else
			{



			}

            $this->admin_model->KM_update(array(
                "class" => "sister_compannies",
                "update" => $banners_array
            ), array(
                'cmp_id' => $gallery_id
            ));
			$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		 Product Updated successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
           redirect(base_url().'admin/sister_compannies_list');
        }
       $data['edit_banner']=$this->admin_model->KM_first(array("class"=>"sister_compannies","fields"=>array('*'),"conditions"=>array("cmp_id"=>$gallery_id)));

        $this->load->view('admin/edit_sister_compannies', $data);
    }

	 public function add_img($cmp_id='')
    {
	 //  $this->check_session();
        if ($this->input->post('submit')) {

            $banner_title = $this->input->post('banner_title');
        	$date=date('Ymdhis');
			$configVideo['upload_path'] = 'uploads/banners';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$class_image1=$banner_image = $date.$_FILES['banner_image']['name'];
			$configVideo['file_name'] = $banner_image;
			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('banner_image'))
			{
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();
			$banner_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array("source_image"    => $videoDetails1['full_path'],
			"new_image" => 'uploads/banners/thumbnails/'.$banner_image,
			"maintain_ratio"  => FALSE,
			"width"	 =>  230,
			"height"  =>  140	);
			$config['image_library'] = 'gd2';
			$this->load->library('image_lib');		// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation
			$this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}
			}
		$cmp_id=$this->input->post('cmp_id');
	       $slug= $this->seo_friendly_url($banner_title);
            $banners_array = array(

                'gallery_title' => $banner_title,
                'gallery_image' => $banner_image,
				'sort_order'=> $this->input->post('sort_order'),
				'cmp_id'=> $this->input->post('cmp_id')


            );
            $this->admin_model->KM_save(array(
                'class' => 'sister_compannies_images',
                'insert' => $banners_array
            ));
				$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		 Image Added successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
            redirect(base_url().'admin/img_list/'.$cmp_id);
        }
        $data['cmp_id']=$cmp_id;
        $this->load->view('admin/add_img',$data);
    }
		    public function edit_img($gallery_id='')
    {

        if ($this->input->post('submit')) {
			 $gallery_id   = $this->input->post('gallery_id');

            $banner_title = $this->input->post('banner_title');

         $cmp_id = $this->input->post('cmp_id');
           $banners_array = array(

                'gallery_title' => $banner_title,
				'sort_order'=> $this->input->post('sort_order')

            );
			$date=date('Ymdhis');
			if($_FILES['banner_image']['name']!='')
			{
			$configVideo['upload_path'] = 'uploads/banners';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$class_image1=$banner_image = $date.$_FILES['banner_image']['name'];
			$configVideo['file_name'] = $banner_image;
			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('banner_image'))
			{
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();
			$banner_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array("source_image"    => $videoDetails1['full_path'],
			"new_image" => 'uploads/banners/thumbnails/'.$banner_image,
			"maintain_ratio"  => FALSE,
			"width"	 => 100,
			"height"  => 100	);
			$config['image_library'] = 'gd2';
			$this->load->library('image_lib');		// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation
			$this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}
            $banners_array['gallery_image']= $banner_image;
			}
			}else
			{



			}

            $this->admin_model->KM_update(array(
                "class" => "sister_compannies_images",
                "update" => $banners_array
            ), array(
                'gallery_id' => $gallery_id
            ));
			$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Image Updated successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
          redirect(base_url().'admin/img_list/'.$cmp_id);
        }
       $data['edit_banner']=$this->admin_model->KM_first(array("class"=>"sister_compannies_images","fields"=>array('*'),"conditions"=>array("gallery_id"=>$gallery_id)));

        $this->load->view('admin/edit_img', $data);
    }
		    public function img_list($cmp_id)
    {
 $data['cmp_id']=$cmp_id;
		  $data['all_banners']=$this->admin_model->KM_all(array("class"=>"sister_compannies_images","fields"=>array('*'),"conditions"=>array("cmp_id"=>$cmp_id)));
        $this->load->view('admin/img_list', $data);
    }
	   public function delete_img($gallery_id,$cmp_id)
    {
        if (isset($gallery_id) && $gallery_id != "") {
            $this->admin_model->KM_delete(array(
                "class" => "sister_compannies_images",
                "conditions" => array(
                    "gallery_id" => $gallery_id
                )
            ));
        }
		$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Image Deleted successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
        redirect(base_url().'admin/img_list/'.$cmp_id);
    }

		 public function add_v($cmp_id='')
    {
	 //  $this->check_session();
        if ($this->input->post('submit')) {

            $video_url = $this->input->post('video_url');

		$cmp_id=$this->input->post('cmp_id');

            $banners_array = array(

                'video_url' => $video_url,
    			'cmp_id'=> $this->input->post('cmp_id')


            );
            $this->admin_model->KM_save(array(
                'class' => ' sister_compannies_videos',
                'insert' => $banners_array
            ));
				$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		 Video Added successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
            redirect(base_url().'admin/v_list/'.$cmp_id);
        }
        $data['cmp_id']=$cmp_id;
        $this->load->view('admin/add_v',$data);
    }
		    public function v_list($cmp_id)
    {
 $data['cmp_id']=$cmp_id;
		  $data['all_banners']=$this->admin_model->KM_all(array("class"=>"sister_compannies_videos","fields"=>array('*'),"conditions"=>array("cmp_id"=>$cmp_id)));
        $this->load->view('admin/v_list', $data);
    }
	   public function delete_v($gallery_id,$cmp_id)
    {
        if (isset($gallery_id) && $gallery_id != "") {
            $this->admin_model->KM_delete(array(
                "class" => "sister_compannies_videos",
                "conditions" => array(
                    "video_id" => $gallery_id
                )
            ));
        }
		$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		Video Deleted successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
        redirect(base_url().'admin/v_list/'.$cmp_id);
    }

		    public function edit_v($video_id='')
    {

        if ($this->input->post('submit')) {
			 $video_id   = $this->input->post('video_id');
           $video_url   = $this->input->post('video_url');
		    $cmp_id   = $this->input->post('cmp_id');
            $banners_array = array(

                'video_url' => $video_url



            );


            $this->admin_model->KM_update(array(
                "class" => "sister_compannies_videos",
                "update" => $banners_array
            ), array(
                'video_id' => $video_id
            ));
			$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		video Updated successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
            redirect(base_url().'admin/v_list/'.$cmp_id);
        }
       $data['edit_banner']=$this->admin_model->KM_first(array("class"=>"sister_compannies_videos","fields"=>array('*'),"conditions"=>array("video_id"=>$video_id)));

        $this->load->view('admin/edit_v', $data);
    }

	 public function services_list_list()
    {
	  //  $this->check_session();
        $data['all_banners'] = $this->admin_model->KM_all(array(
            "class" => "services_list"
        ));
        $this->load->view('admin/services_list', $data);
    }
    public function delete_service_list($gallery_id)
    {
        if (isset($gallery_id) && $gallery_id != "") {
            $this->admin_model->KM_delete(array(
                "class" => "services_list",
                "conditions" => array(
                    "cmp_id" => $gallery_id
                )
            ));
        }
		$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		 Service Deleted successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
        redirect(base_url().'admin/services_list_list');
    }
    public function add_services_list()
    {
	 //  $this->check_session();
        if ($this->input->post('submit')) {

            $banner_title = $this->input->post('banner_title');
        	$date=date('Ymdhis');
			$configVideo['upload_path'] = 'uploads/banners';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$class_image1=$banner_image = $date.$_FILES['banner_image']['name'];
			$configVideo['file_name'] = $banner_image;
			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('banner_image'))
			{
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();
			$banner_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array("source_image"    => $videoDetails1['full_path'],
			"new_image" => 'uploads/banners/thumbnails/'.$banner_image,
			"maintain_ratio"  => FALSE,
			"width"	 =>  230,
			"height"  =>  140	);
			$config['image_library'] = 'gd2';
			$this->load->library('image_lib');		// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation
			$this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}
			}

	       $slug= $this->seo_friendly_url($banner_title);
            $banners_array = array(

                'gallery_title' => $banner_title,

                'gallery_image' => $banner_image,
				'sort_order'=> $this->input->post('sort_order'),
				'slug'=>$slug,
				'email'=> $this->input->post('email'),
				'phone'=> $this->input->post('phone'),
				'business_office'=> $this->input->post('business_office'),
				'reg_office'=> $this->input->post('reg_office'),


            );
            $this->admin_model->KM_save(array(
                'class' => 'services_list',
                'insert' => $banners_array
            ));
				$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		 Service Added successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
            redirect(base_url().'admin/add_service');
        }

        $this->load->view('admin/add_service_list');
    }

	    public function edit_service_list($gallery_id='')
    {

        if ($this->input->post('submit')) {
			 $gallery_id   = $this->input->post('gallery_id');

            $banner_title = $this->input->post('banner_title');


           $banners_array = array(

                'gallery_title' => $banner_title,

				'sort_order'=> $this->input->post('sort_order'),
					'email'=> $this->input->post('email'),
				'phone'=> $this->input->post('phone'),
				'business_office'=> $this->input->post('business_office'),
				'reg_office'=> $this->input->post('reg_office'),

            );
			$date=date('Ymdhis');
			if($_FILES['banner_image']['name']!='')
			{
			$configVideo['upload_path'] = 'uploads/banners';
			$configVideo['allowed_types'] = '*';
			$configVideo['overwrite'] = FALSE;
			$configVideo['remove_spaces'] = TRUE;
			$class_image1=$banner_image = $date.$_FILES['banner_image']['name'];
			$configVideo['file_name'] = $banner_image;
			$this->load->library('upload', $configVideo);
			$this->upload->initialize($configVideo);
			if (!$this->upload->do_upload('banner_image'))
			{
			echo $this->upload->display_errors();
			} else {
			$videoDetails1 = $this->upload->data();
			$banner_image = $tumbnail=$videoDetails1['file_name'];
			$helthy['thumbnail'] = $tumbnail;
			$config = array("source_image"    => $videoDetails1['full_path'],
			"new_image" => 'uploads/banners/thumbnails/'.$banner_image,
			"maintain_ratio"  => FALSE,
			"width"	 => 230,
			"height"  => 140	);
			$config['image_library'] = 'gd2';
			$this->load->library('image_lib');		// Set your config up
			$this->image_lib->initialize($config);
			// Do your manipulation
			$this->image_lib->resize();
			if ( ! $this->image_lib->resize())
			{
			echo $this->image_lib->display_errors();
			}
            $banners_array['gallery_image']= $banner_image;
			}
			}else
			{



			}

            $this->admin_model->KM_update(array(
                "class" => "services_list",
                "update" => $banners_array
            ), array(
                'cmp_id' => $gallery_id
            ));
			$msg='<div class = "alert alert-success alert-dismissable">
		<button type = "button" class = "close" data-dismiss = "alert" aria-hidden = "true">
		&times;
		</button>
		 Service Updated successfully.
		</div>';
		$this->session->set_flashdata('cmsg',$msg);
           redirect(base_url().'admin/services_list_list');
        }
       $data['edit_banner']=$this->admin_model->KM_first(array("class"=>"services_list","fields"=>array('*'),"conditions"=>array("cmp_id"=>$gallery_id)));

        $this->load->view('admin/edit_service', $data);
    }
}
