<?php
class Admin_model extends MY_Model { 
	 function __construct()
	 {
	 	parent::__construct(); 
	 }
	  public function cities_list()
	 {
	  $sql="select * from cities c inner join countries cs  on cs.id=c.country_id";
	  $res = $this->db->query($sql);	
		return $res->result_array(); 
	 
	 } 
	  public function faq_list($course_id)
	 {
	  $sql="select * from faq_list f inner join courses cs  on f.course_id=cs.course_id";
	  
	  if($course_id!='')
	  {
	  $sql.= " where f.course_id='$course_id'";
	  }
	  $res = $this->db->query($sql);	
		return $res->result_array(); 
	 
	 } 
	 
	 
	 public function all_users()
	 {
	  $sql="select * from users where user_type!='admin' order by user_id desc";
	  $res = $this->db->query($sql);	
		return $res->result_array(); 
	 
	 }
	  public function users_report()
	 {
	  $sql="select * from users where user_type!='admin' order by user_id desc";
	  $res = $this->db->query($sql);	
		return $res->result_array(); 
	 
	 }
	
	
	
	  public function categories_list()
	 {
	  $sql="select * from packages order by package_id desc";
	  $res = $this->db->query($sql);	
		return $res->result_array(); 
	 
	 } 


	  public function sub_categories_list($cat_id)
	 {
	  $sql="select * from sub_categories where cat_id='$cat_id'";
	  $res = $this->db->query($sql);	
		return $res->result_array(); 
	 
	 } 


	  public function products_list($product_id)
	 {
	  $sql="select * from products";
	  if($this->session->userdata('vendor_type')!='admin')
	  $sql.=" where vendor_id='".$this->session->userdata('admin_id')."'";
	  
	  $res = $this->db->query($sql);	
		return $res->result_array(); 
	 
	 } 
	  public function edit_category($id)
	  {
		$sql="select * from packages  where package_id='".$id."' ";
		$res = $this->db->query($sql);	
		return $res->result_array(); 
	  }

	   public function edit_sub_category($id)
	  {
		$sql="select * from sub_categories  where sub_cat_id='".$id."' ";
		$res = $this->db->query($sql);	
		return $res->result_array(); 
	  }

	  public function product_list($cat_id)
	 {
	  $sql="select * from products";
	  if($cat_id!=null)
      $sql.= " where cat_id='$cat_id'";
	  $sql.= " order by product_name asc";
	  $res = $this->db->query($sql);	
	  return $res->result_array(); 
	 
	 } 
	  public function product_list_by_cat($cat_slug)
	 {
	  $sql="select * from product_categories pc inner join products p on  p.cat_id=pc.cat_id";
	  if($cat_slug!=null)
      $sql.= " where pc.category_slug='$cat_slug'";
	  $sql.= " order by p.product_name asc";
	  $res = $this->db->query($sql);	
	  return $res->result_array(); 
	 
	 } 
	 	  public function my_orders($user_id)
	 {
	 
	      $sql="SELECT o.order_id,u.user_name,u.phone,o.paid_amount,  group_concat( op1.name ) AS products,o.order_date,o.payment_status
FROM orders o
INNER JOIN users u ON u.user_id = o.user_id
LEFT JOIN order_products op1 ON op1.order_id = o.order_id
where o.user_id='$user_id'
GROUP BY ,op1.order_id
";
		 $res = $this->db->query($sql);
	  
		return $res->result_array();
	 
	 }
	 
	   public function edit_product($id)
	  {
		$sql="select * from products  where product_id='".$id."' ";
		$res = $this->db->query($sql);	
		return $res->result_array(); 
	  }
	
	 

	  public function all_users_count()
	 {
	  $sql="select count(user_id) as count_users from users";
	  $res = $this->db->query($sql);	
		return $res->result_array(); 
	 
	 }

	 
	 public function all_categories()
	 {
	  $sql="select * from categories";
	  $res = $this->db->query($sql);	
		return $res->result_array(); 
	 
	 }
     public function all_news()
	  {
		$sql="select * from news";
		$res = $this->db->query($sql);	
		return $res->result_array(); 
	  }
	  public function edit_news($news_id)
	  {
	  $sql="select * from news  where news_id='".$news_id."' ";
		$res = $this->db->query($sql);	
		return $res->result_array(); 
	  
	  }
	 }