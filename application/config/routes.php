<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
// $route['(:any)'] = "home/view_journal_details/$1";

$route['journals'] = "home/journals";
$route['(:any)'] = "custom/about_journal/$1";

$route['(:any)/aims-and-scope'] = "custom/aims_and_scope/$1";
$route['(:any)/about-journal'] = "custom/about_journal/$1";
$route['(:any)/editorial-board'] = "custom/editorial_board/$1";
$route['(:any)/articles'] = "custom/articles/$1";
$route['(:any)/articles/(:any)'] = "custom/articles_list/$1/$2";

$route['(:any)/articles-in-press'] = "custom/articles_in_press/$1";
// $route['(:any)/articles-in-press/(:any)'] = "custom/articles_in_press_list/$1/$2";

$route['(:any)/special-issues'] = "custom/special_issues/$1";
// $route['(:any)/special-issues/(:any)'] = "custom/special_issues_list/$1/$2";

$route['(:any)/for-author'] = "custom/for_author/$1";
$route['(:any)/publication-charges'] = "custom/publication_charges/$1";
$route['(:any)/indexing'] = "custom/indexing/$1";

$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
